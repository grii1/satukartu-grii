# SatuKartu v1.0

Preinstalled with plugins:
SES - Advanced Article Plugin 4.10.5
SES - Advanced Events - Events Tickets Selling & Booking System 4.10.5
SES - Advanced Events Plugin 4.10.5
SES - Advanced Photos & Albums Plugin 4.10.3p7
SES - Advanced Videos & Channels Plugin 4.10.3p7
SES - Credits & Activity / Reward Points Plugin 4.10.5
SES - Crowdfunding / Charity / Fundraising / Donations Plugin 4.10.5
SES - Group Communities Plugin 4.10.5
*SES - Group Polls Extension 4.10.5
SES - Group Team Showcase Extension 4.10.5
*SES - Group Videos Extension 4.10.5
*SES - JS & CSS Minify Plugin 4.10.5
SES - Membership Cards Plugin 4.10.5
SES - Native Android Mobile App Plugin 4.10.5
SES - News / RSS Importer & Aggregator Plugin 4.10.5
SES - Professional Activity & Nested Comments Plugin 4.10.5
SES - Progressive Web App (PWA) Plugin - Interactive Mobile & Tablet Interface 4.10.5
SES - SocialEngine REST APIs Plugin 4.10.5
SES - SocialEngineSolutions Basic Required Plugin 4.10.5

*not working yet