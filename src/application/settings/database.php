<?php defined('_ENGINE') or die('Access Denied'); return array (
  'adapter' => 'mysqli',
  'params' => 
  array (
    'host' => $_ENV['MYSQL_HOST'],
    'username' => $_ENV['MYSQL_USER'],
    'password' => $_ENV['MYSQL_USER_PW'],
    'dbname' => $_ENV['MYSQL_DB'],
    'charset' => 'UTF8',
    'adapterNamespace' => 'Zend_Db_Adapter',
    'port' => NULL,
  ),
  'isDefaultTableAdapter' => true,
  'tablePrefix' => 'engine4_',
  'tableAdapterClass' => 'Engine_Db_Table',
); ?>
