<?php return array (
  'package' => 
  array (
    'type' => 'theme',
    'name' => 'insignia-purple',
    'version' => '4.10.5',
    'revision' => '$Revision: 10113 $',
    'path' => 'application/themes/insignia-purple',
    'repository' => 'socialengine.com',
    'title' => 'Insignia Purple',
    'thumb' => 'theme.jpg',
    'author' => 'Webligo Developments',
    'actions' => 
    array (
      0 => 'install',
      1 => 'upgrade',
      2 => 'refresh',
      3 => 'remove',
    ),
    'callback' => 
    array (
      'class' => 'Engine_Package_Installer_Theme',
    ),
    'directories' => 
    array (
      0 => 'application/themes/insignia-purple',
    ),
    'description' => 'Insignia',
  ),
  'files' => 
  array (
    0 => 'theme.css',
    1 => 'constants.css',
  ),
  'colorVariants' => 
  array (
  ),
); ?>