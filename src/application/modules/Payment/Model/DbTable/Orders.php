<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Payment
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Orders.php 9747 2012-07-26 02:08:08Z john $
 * @author     John Boehr <j@webligo.com>
 */

/**
 * @category   Application_Core
 * @package    Payment
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class Payment_Model_DbTable_Orders extends Engine_Db_Table
{
  protected $_rowClass = 'Payment_Model_Order';

  public function getHistoryTransaction($param = array())
  {
    $tableName = $this->info('name');
    $select = $this->select()->from($tableName);
    $select->where('user_id =?', $param['user_id']);
    // $select->where('state =?', $param['state']);
    // $select->where('state =?', 'complete');

    $select->order('creation_date DESC');

    $result = $this->fetchAll($select);

    return $result;
  }
}
