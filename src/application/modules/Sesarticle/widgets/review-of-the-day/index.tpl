<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesarticle
 * @package    Sesarticle
 * @copyright  Copyright 2015-2016 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: index.tpl 2016-07-23 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

?>
<ul class="sesarticle_side_block sesbasic_bxs sesbasic_clearfix">
  <?php include APPLICATION_PATH . '/application/modules/Sesarticle/views/scripts/_reviewsidebarWidgetData.tpl'; ?>
</ul>