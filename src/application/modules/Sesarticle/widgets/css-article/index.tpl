<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesarticle
 * @package    Sesarticle
 * @copyright  Copyright 2015-2016 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: index.tpl 2016-07-23 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

?>
<style type="text/css">


.layout_sesarticle_view_article{
	margin-bottom:0px !important;
	margin-top:0px !important;
	border:none;
	padding-top:0px !important;
	padding-bottom:0px !important
}
.layout_core_container_tabs{
	margin-bottom:0px !important;
	border:none;
	padding-top:0px !important;
	padding-bottom:0px !important;
	overflow:hidden;
}
.layout_core_comments{
	border:none !important;
	margin-top:0 !important;
	padding-top:15px !important;
}
.layout_sesarticle_css_article{
	padding:0 !important; 
	border:none !important;
	margin:0px !important;
	box-shadow:none !important;
}
</style>