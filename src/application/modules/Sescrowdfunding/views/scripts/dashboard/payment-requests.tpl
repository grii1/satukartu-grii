<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sescrowdfunding
 * @package    Sescrowdfunding
 * @copyright  Copyright 2019-2020 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: payment-requests.tpl  2019-01-08 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

 ?>
<?php
if(!$this->is_ajax){
	echo $this->partial('dashboard/left-bar.tpl', 'sescrowdfunding', array('crowdfunding' => $this->crowdfunding));?>
<div class="sescrowdfunding_dashboard_content sesbm sesbasic_clearfix sesbasic_bxs">
<?php } ?>
<?php $defaultCurrency = Engine_Api::_()->sescrowdfunding()->defaultCurrency(); ?>
<div class="sescrowdfunding_dashboard_content_header sesbasic_clearfix">
  <h3><?php echo $this->translate("Withdrawal reports"); ?></h3>
  <?php echo $this->translate('create withdrawal request.'); ?>
  <a href="<?php echo $this->url(array('crowdfunding_id' => $this->crowdfunding->custom_url,'action'=>'payment-request'), 'sescrowdfunding_dashboard', false) ?>">
    <button name="button" id="submit" type="button">Create request</button>
  </a>

</div>
<!-- <?php if(!count($this->userGateway)){ ?>
	<div class="tip">
  <span>
    <?php echo $this->translate('Payment details not submitted yet %1$sClick Here%2$s to submit.', '<a href="'.$this->url(array('crowdfunding_id' => $this->crowdfunding->custom_url,'action'=>'account-details'), 'sescrowdfunding_dashboard', false).'">', '</a>'); ?>
  </span>
</div>
<?php } ?> -->

<div class="sescf_payment_request_table sesbasic_dashboard_table sesbasic_bxs">
  <form method="post" >
    <table>
      <thead>
        <tr>
          <th class="centerT"><?php echo $this->translate("No"); ?></th>
          <th><?php echo $this->translate("Requester's name") ?></th>
          <th><?php echo $this->translate("Requested Amount") ?></th>
          <th><?php echo $this->translate("Date") ?></th>
          <th><?php echo $this->translate("Transfer Account") ?></th>
          <th><?php echo $this->translate("In the name of") ?></th>
          <th><?php echo $this->translate("Bank") ?></th>
          <th><?php echo $this->translate("Status") ?></th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1; foreach($this->withdrawRequest as $item): ?>
          <?php $user = Engine_Api::_()->getDbtable('users', 'user')->getUser(array('user_id' => $item->user_id)); ?>
          <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $user->displayname ?></td>
            <td><?php echo $item->requested_amount ?></td>
            <td><?php echo $item->creation_date ?></td>
            <td><?php echo $item->account_number ?></td>
            <td><?php echo $item->account_name ?></td>
            <td><?php echo $item->bank_name ?></td>
            <td><?php echo $item->state ?></td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
   </form>
</div>


<!-- <div class="sescf_payment_request_stats sescf_donations_stats_container sesbasic_bxs sesbasic_clearfix">
	<div class="sescf_donations_stats">
  	<div>
      <span><?php echo $this->translate("Total Orders"); ?></span>
      <span><?php echo $orderDetails['totalOrder'];?></span>
    </div>
  </div>
	<div class="sescf_donations_stats">
		<div>
      <span><?php echo $this->translate("Total Amount"); ?></span>
      <span><?php echo Engine_Api::_()->sescrowdfunding()->getCurrencyPrice($orderDetails['totalAmountSale'],$defaultCurrency); ?></span>
  	</div>
  </div>
	<div class="sescf_donations_stats">
		<div>
      <span><?php echo $this->translate("Commission Amount"); ?></span>
      <span><?php echo Engine_Api::_()->sescrowdfunding()->getCurrencyPrice($orderDetails['commission_amount'],$defaultCurrency); ?></span>
  	</div>
  </div>
	<div class="sescf_donations_stats">
		<div>
      <span><?php echo $this->translate("Total Amount After Commission"); ?></span>
      <?php $totalRemainingAmount = $orderDetails['totalAmountSale'] - $orderDetails['commission_amount']; ?>
      <span><?php echo Engine_Api::_()->sescrowdfunding()->getCurrencyPrice($totalRemainingAmount,$defaultCurrency); ?></span>
  	</div>
  </div>
  <div class="sescf_donations_stats">
		<div>
      <span><?php echo $this->translate("Total Remaining Amount"); ?></span>
      <?php $totalRemainingAmountafterPayreceived = $this->remainingAmount - $orderDetails['commission_amount']; ?>
      <span><?php echo Engine_Api::_()->sescrowdfunding()->getCurrencyPrice($totalRemainingAmountafterPayreceived,$defaultCurrency); ?></span>
		</div>
	</div>
</div> -->

</div>
</div>
</div>
