<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sescrowdfunding
 * @package    Sescrowdfunding
 * @copyright  Copyright 2019-2020 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: index.tpl  2019-01-08 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

 ?>
<?php $baseURL = $this->layout()->staticBaseUrl; ?>
<style>
  p {
    color : #000;
  }

  table tr * {
    color : #000;
  }
</style>
<script type="text/javascript">
  var currentOrder = '<?php echo $this->order ?>';
  var currentOrderDirection = '<?php echo $this->order_direction ?>';
  var changeOrder = function(order, default_direction){
    // Just change direction
    if( order == currentOrder ) {
      $('order_direction').value = ( currentOrderDirection == 'ASC' ? 'DESC' : 'ASC' );
    } else {
      $('order').value = order;
      $('order_direction').value = default_direction;
    }
    $('filter_form').submit();
  }
  function multiDelete() {
    return confirm("<?php echo $this->translate('Are you sure you want to delete the selected tickets?');?>");
  }
  function selectAll() {
    var i;
    var multidelete_form = $('multidelete_form');
    var inputs = multidelete_form.elements;
    for (i = 1; i < inputs.length; i++) {
      if (!inputs[i].disabled) {
        inputs[i].checked = inputs[0].checked;
      }
    }
  }
</script>
<?php include APPLICATION_PATH .  '/application/modules/Sescrowdfunding/views/scripts/dismiss_message.tpl';?>


<h3><?php echo $this->translate("All transaction of crowdfundings") ?></h3>
<!-- <p><?php echo $this->translate('This page lists all of the payments made to the crowdfunding owners on your website. You can use this page to monitor these payments made. Entering criteria into the filter fields will help you find specific payment detail. Leaving the filter fields blank will show all the payments made to crowdfunding owners on your social network.'); ?></p> -->
<br />
<div class='admin_search sesbasic_search_form'>
  <?php echo $this->formFilter->render($this) ?>
</div>
<br />
<?php $counter = $this->paginator->getTotalItemCount(); ?>
<?php if( count($this->paginator) ): ?>
  <div class="sesbasic_search_reasult">
    <?php echo $this->translate(array('%s entry found.', '%s entries found.', $counter), $this->locale()->toNumber($counter)) ?>
    <br><br>
    <?php echo $this->htmlLink(array('route' => 'default', 'module' => 'sescrowdfunding', 'controller' => 'admin-paymentmade', 'action' => 'exportexcel'), $this->translate("Export to Excel")) ?>
  </div>
  <form id='multidelete_form' method="post" action="<?php echo $this->url();?>" onSubmit="return multiDelete()">
    <div class="clear" style="overflow:auto;">
    <table class='admin_table'>
      <thead>
        <tr>
          <th class='admin_table_short'><a style="color:#000;" href="javascript:void(0);" onclick="javascript:changeOrder('userpayrequest_id', 'DESC');"><?php echo $this->translate("ID") ?></a></th>
          <th><?php echo $this->translate("Crowdfunding Title") ?></th>
          <th><?php echo $this->translate("Owner") ?></th>
          <th><?php echo $this->translate("VA Number") ?></th>
          <th><?php echo $this->translate("Current Amount") ?></th>
          <th><?php echo $this->translate("Withdrawal Amount") ?></th>
          <th><?php echo $this->translate("Latest Withdrawal"); ?></th>
          <th><?php echo $this->translate("Total Amount") ?></th>
          <th><?php echo $this->translate("Status"); ?></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($this->groups as $group): ?>
          <tr style="background:#d2d2d2;">
            <td></td>
            <td colspan="8"><a style="color:#000;" href="<?php echo $group->getHref(); ?>" target="_blank"><?php echo $group->getTitle(); ?></a></td>
          </tr>
          <?php $no=1; foreach ($this->paginator as $item): ?>
            <?php if($group->group_id == $item->group_id): ?>
              <?php $crowdfunding = Engine_Api::_()->getItem('crowdfunding',$item->crowdfunding_id);
                if(!$crowdfunding)
                  continue;

              ?>
              <tr>
                <td><?php echo $no++ ?></td>
                <td><a style="color:#000;" href="<?php echo $crowdfunding->getHref(); ?>" target="_blank"><?php echo $crowdfunding->getTitle(); ?></a></td>
                <td><?php echo $crowdfunding->getOwner(); ?></td>
                <td><?php echo $item->va_number ?></td>
                <td><?php echo round($item->total_amount,2); ?></td>
                <td>0</td>
                <td><?php echo $item->creation_date; ?></td>
                <td><?php echo round($item->total_amount,2); ?></td>
                <td><?php echo ($item->crowdfunding_status == '1' ? 'Active' : 'Non-Active'); ?></td>
              </tr>
            <?php endif; ?>
          <?php endforeach; ?>
        <?php endforeach; ?>
      </tbody>
    </table>
    </div>
  </form>
  <br/>
  <div>
    <?php echo $this->paginationControl($this->paginator); ?>
  </div>
<?php else:?>
  <div class="tip">
    <span>
      <?php echo $this->translate("No payments have been made yet.") ?>
    </span>
  </div>
<?php endif; ?>
<br>
<h3><?php echo $this->translate("All withdrawal request of crowdfundings") ?></h3>
<br>
<div class="multidelete_form">
  <div class="clear" style="overflow:auto;">
    <table class="admin_table">
      <thead>
        <tr>
          <th class="admin_table_short"><?php echo $this->translate("No"); ?></th>
          <th><?php echo $this->translate("Requester's name") ?></th>
          <th><?php echo $this->translate("Requested Amount") ?></th>
          <th><?php echo $this->translate("Date") ?></th>
          <th><?php echo $this->translate("Transfer Account") ?></th>
          <th><?php echo $this->translate("In the name of") ?></th>
          <th><?php echo $this->translate("Bank") ?></th>
          <th><?php echo $this->translate("Status") ?></th>
          <th><?php echo $this->translate("Action") ?></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($this->groups as $group): ?>
          <tr>
            <td></td>
            <td colspan="8"><a style="color:#000;" href="<?php echo $group->getHref(); ?>" target="_blank"><?php echo $group->getTitle(); ?></a></td>
          </tr>
          <?php foreach($this->crowdfundings as $crowdfunding): ?>
            <?php if($group->group_id == $crowdfunding->group_id): ?>
              <tr style="background:#d2d2d2;">
                <td></td>
                <td colspan="8"><a style="color:#000;" href="<?php echo $crowdfunding->getHref(); ?>" target="_blank"><?php echo $crowdfunding->getTitle(); ?></a></td>
              </tr>
              <?php $no=1; foreach($this->withdrawals as $withdrawal): ?>
                <?php if($crowdfunding->crowdfunding_id == $withdrawal->crowdfunding_id): ?>
                  <?php $user = Engine_Api::_()->getDbtable('users', 'user')->getUser(array('user_id' => $withdrawal->user_id)); ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $user->displayname; ?></td>
                    <td><?php echo $withdrawal->requested_amount; ?></td>
                    <td><?php echo $withdrawal->creation_date; ?></td>
                    <td><?php echo $withdrawal->account_number; ?></td>
                    <td><?php echo $withdrawal->account_name; ?></td>
                    <td><?php echo $withdrawal->bank_name; ?></td>
                    <td><?php echo $withdrawal->state; ?></td>
                    <td>
                      <a class='smoothbox' href='<?php echo $this->url(array('action' => 'withdraw-approve', 'id' => $withdrawal->withdrawrequests_id));?>' style="color:#5ba1cd">
                        <?php echo $this->translate("Approve") ?>
                      </a>
                       | 
                      <a class='smoothbox' href='<?php echo $this->url(array('action' => 'withdraw-reject', 'id' => $withdrawal->withdrawrequests_id));?>' style="color:#5ba1cd">
                        <?php echo $this->translate("Reject") ?>
                      </a>
                    </td>
                  </tr>
                <?php endif; ?>
              <?php endforeach; ?>
            <?php endif; ?>
          <?php endforeach; ?>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>
