<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sescrowdfunding
 * @package    Sescrowdfunding
 * @copyright  Copyright 2019-2020 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: dismiss_message.tpl  2019-01-08 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

 ?>
<h2><?php echo $this->translate("Crowdfunding / Charity / Fundraising / Donations Plugin") ?></h2>
<?php include APPLICATION_PATH .  '/application/modules/Sesbasic/views/scripts/_mapKeyTip.tpl';?>
<?php if( count($this->navigation) ): ?>
  <div class='sesbasic-admin-navgation'>
    <?php
      $viewer = Engine_Api::_()->user()->getViewer();
      if($viewer->level_id == 1){
        echo $this->navigation()->menu()->setContainer($this->navigation)->render();
      }
    ?>
  </div>
<?php endif; ?>
