<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sescrowdfunding
 * @package    Sescrowdfunding
 * @copyright  Copyright 2019-2020 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Userpayrequests.php  2019-01-08 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
class Sescrowdfunding_Model_DbTable_Withdrawrequests extends Engine_Db_Table {

  protected $_name = 'sescrowdfunding_withdrawrequests';
  protected $_rowClass = "Sescrowdfunding_Model_Withdrawrequests";

  public function getTotalWithdrawalAmount($params = array()){
    $tableName = $this->info('name');
    $select = $this->select()->from($tableName,array('sum(requested_amount) as total_amount'));
    if ($params['crowdfunding_id'] && !empty($params['crowdfunding_id'])) {
      $select->where('crowdfunding_id =?', $params['crowdfunding_id']);
    }

    $result = $this->fetchRow($select);

    if ($result->total_amount == null) {
      return 0;
    }else {
      return $result->total_amount;
    }

  }

  public function getWithdrawalRequests($params = array()) {
    $tabeleName = $this->info('name');
    $select = $this->select()->from($tabeleName);

    if (isset($params['crowdfunding_id']))
      $select->where('crowdfunding_id =?', $params['crowdfunding_id']);

    if (isset($params['state']))
      $select->where('state =?', $params['state']);

    if (isset($params['withdrawrequests_id']))
      $select->where('withdrawrequests_id =?', $params['withdrawrequests_id']);

    if (isset($params['fetchRow']))
      return $this->fetchRow($select);


		$select->order('withdrawrequests_id DESC');
    return $this->fetchAll($select);
  }

  public function checkApproveCount($withdrawrequest_id,$state){
    $tabeleName = $this->info('name');
    $select = $this->select()->from($tabeleName);
    $select->where('withdrawrequests_id =?', $withdrawrequest_id);

    $result = $this->fetchRow($select);

    if ($state == 'approve') {
      return $result->approve_count;
    }else {
      return $result->reject_count;
    }
  }

  public function approve($withdrawrequest_id){
    $tabeleName = $this->info('name');
    $select = $this->select()->from($tabeleName);

    $checkApproveCount = $this->checkApproveCount($withdrawrequest_id,'approve');

    if ($checkApproveCount >= 1) {
      $this->update(array('approve_count' => $checkApproveCount + 1 ,'state' => 'processed','approved_date' =>new Zend_Db_Expr('NOW()') ),
         array('withdrawrequests_id = ?' => $withdrawrequest_id));
    }else {
      $this->update(array('approve_count' => $checkApproveCount + 1 ),
         array('withdrawrequests_id = ?' => $withdrawrequest_id));
    }
  }

  public function reject($withdrawrequest_id){
    $tabeleName = $this->info('name');
    $select = $this->select()->from($tabeleName);

    $checkApproveCount = $this->checkApproveCount($withdrawrequest_id,'reject');

    if ($checkApproveCount >= 1) {
      $this->update(array('reject_count' => $checkApproveCount + 1 ,'state' => 'rejected','approved_date' =>new Zend_Db_Expr('NOW()') ),
         array('withdrawrequests_id = ?' => $withdrawrequest_id));
    }else {
      $this->update(array('reject_count' => $checkApproveCount + 1 ),
         array('withdrawrequests_id = ?' => $withdrawrequest_id));
    }
  }

  public function upload($withdrawrequest_id,$file_id){
    $this->update(array('file_id' => $file_id, 'state' => 'complete','approved_date' =>new Zend_Db_Expr('NOW()') ),
       array('withdrawrequests_id = ?' => $withdrawrequest_id));
  }

}
