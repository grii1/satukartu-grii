<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sescrowdfunding
 * @package    Sescrowdfunding
 * @copyright  Copyright 2019-2020 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Userpayrequests.php  2019-01-08 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
class Sescrowdfunding_Model_DbTable_Userwithdrawapprove extends Engine_Db_Table {

  protected $_name = 'sescrowdfunding_userwithdrawapprove';
  protected $_rowClass = "Sescrowdfunding_Model_Userwithdrawapprove";

  public function approvalCount($params = array()){
    $tableName = $this->info('name');
    $select = $this->select()->from($tableName,array('count(1) as counter'));
    $select->where('withdrawrequests_id =?', $params['withdrawrequest_id']);

    if ($params['state'] && !empty($params['state'])) {
      $select->where('state =?', $params['state']);
    }

    return $this->fetchRow($select)->counter;
  }

  public function checkUserApprove($params = array()){
    $tableName = $this->info('name');
    $select = $this->select()->from($tableName);
    $select->where('withdrawrequests_id =?', $params['withdrawrequest_id']);
    $select->where('user_id =?', $params['user_id']);

    return $this->fetchRow($select);
  }

  public function saveApproveHistory($params = array()){
    $this->insert(array(
        'withdrawrequests_id' => $params['withdrawrequests_id'],
        'user_id' => $params['user_id'],
        'state' => $params['state'],
        'creation_date' => new Zend_Db_Expr('NOW()'),
    ));
  }
}
