<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sescrowdfunding
 * @package    Sescrowdfunding
 * @copyright  Copyright 2019-2020 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Donate.php  2019-01-08 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

class Sescrowdfunding_Form_GenerateVA extends Engine_Form {
  protected $amount,$va;

  public function setVaNumber($vaNumber){
    $this->va = $vaNumber;
  }

  public function setDonationAmount($donationAmount){
    $this->amount = $donationAmount;
  }


  public function init() {

    $crowdfunding_id = Zend_Controller_Front::getInstance()->getRequest()->getParam('crowdfunding_id', null);
    $gateway_id = Zend_Controller_Front::getInstance()->getRequest()->getParam('gateway_id', null);
    $paymentGateways = Engine_Api::_()->sescrowdfunding()->checkPaymentGatewayEnable();
    $this->setTitle('Crowdfunding Donation')
          ->setDescription('Transfer the amount of your transfer to the virtual account that appears in the column.')
          ->setAttrib('name', 'sescrowdfundings_donate');

    $this->addElement('Text', 'donation_amount', array(
        'label' => "Virtual Account",
        'allowEmpty' => true,
        'required' => true,
        'value' => $this->va,
    ));

    $this->addElement('Text', 'va_number', array(
        'label' => "Transfer Amount",
        'allowEmpty' => true,
        'required' => true,
        'value' => $this->amount,
    ));

    $this->addElement('Cancel', 'Exit the page', array(
        'label' => 'Exit the page',
        'link' => true,
        'prependText' => ' or ',
        'onclick' => 'parent.Smoothbox.close();',
        'decorators' => array(
            'ViewHelper'
        )
    ));

  }
}
