<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sescrowdfunding
 * @package    Sescrowdfunding
 * @copyright  Copyright 2019-2020 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Paymentrequest.php  2019-01-08 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
class Sescrowdfunding_Form_Dashboard_Paymentrequest extends Engine_Form {

  public function init() {

    //get current logged in user
    $user = Engine_Api::_()->user()->getViewer();
    $this->setTitle('Withdrawal Request')
			->setDescription('Enter the information in below form and click on "Submit" button to request the withdrawal.')
            ->setAttrib('id','sescrowdfunding_ppayment_request')
            ->setMethod("POST");

		// $this->addElement('Text', 'total_amount', array(
    //       'label' => 'Total Amount',
		// 			'readonly'=>'readonly',
    // ));

    // $this->addElement('Text','requester_name',array(
    //   'label' => "Requester's Name",
    //   'required' => true
    // ));

    $this->addElement('Text', 'requested_amount', array(
          'label' => 'Request Amount',
					'allowEmpty' => false,
					'required' => true,
					'validators' => array(
								array('GreaterThan', true, array(0)),
						)
    ));

    $this->addElement('Text','account_number',array(
      'label' => "Account number",
      'required' => true
    ));

    $this->addElement('Text','account_name',array(
      'label' => "Account name",
      'required' => true
    ));

    // $this->addElement('Text','bank_name',array(
    //   'label' => "Bank",
    //   'required' => true
    // ));

    $listOfBank = array(
      'Bank BCA' => 'Bank BCA',
      'Bank Mandiri' => 'Bank Mandiri',
      'Bank BNI' => 'Bank BNI',
      'Bank BNI Syariah' => 'Bank BNI Syariah',
      'Bank BRI' => 'Bank BRI',
      'Bank Syariah Mandiri' => 'Bank Syariah Mandiri',
      'Bank CIMB Niaga' => 'Bank CIMB Niaga',
      'Bank CIMB Niaga Syariah' => 'Bank CIMB Niaga Syariah',
      'JENIUS' => 'JENIUS',
      'Bank BRI Syariah' => 'Bank BRI Syariah',
      'Permata Bank' => 'Permata Bank',
      'Bank Danamon' => 'Bank Danamon',
      'Bank BII Maybank' => 'Bank BII Maybank',
      'Bank Mega' => 'Bank Mega',
      'Bank BCA Syariah' => 'Bank BCA Syariah',
      'Bank Bukopin' => 'Bank Bukopin',
      'Bank BCA Syariah' => 'Bank BCA Syariah',
      'Bank Lippo' => 'Bank Lippo',
      'Citibank' => 'Citibank',
      'Others' => 'Others',
    );

    $this->addElement('Select', 'bank_name', array(
        'label' => 'Bank',
        'multiOptions' => $listOfBank,
        'required' => true,
    ));


// 		$this->addElement('Text', 'total_tax_amount', array(
//           'label' => 'Total Tax Amount',
// 					'readonly'=>'readonly',
//     ));
//
// 		$this->addElement('Text', 'total_commission_amount', array(
//           'label' => 'Total Commission Amount',
// 					'readonly'=>'readonly',
//     ));

		// $this->addElement('Text', 'remaining_amount', array(
    //       'label' => 'Total Remaining Amount',
		// 			'readonly'=>'readonly',
    // ));
    //
    //
		// $this->addElement('Textarea', 'user_message', array(
    //       'label' => 'Message',
    // ));

		$this->addElement('Button', 'submit', array(
      'label' => 'Submit',
      'type' => 'submit',
      'ignore' => true,
      'decorators' => array(
          'ViewHelper',
      ),
    ));

    $this->addElement('Cancel', 'cancel', array(
      'label' => 'cancel',
      'link' => true,
      'onclick'=>'parent.Smoothbox.close();',
      'prependText' => ' or ',
      'decorators' => array(
          'ViewHelper',
      ),
    ));

    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons', array(
      'decorators' => array(
          'FormElements',
          'DivDivDivWrapper',
      ),
    ));
  }
}
