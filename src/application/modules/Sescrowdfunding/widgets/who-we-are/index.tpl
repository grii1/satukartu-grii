<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sescrowdfunding
 * @package    Sescrowdfunding
 * @copyright  Copyright 2019-2020 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: index.tpl  2019-01-08 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
 
 ?>

<div class="sescf_whoweare sesbasic_bxs sesbasic_clearfix">
  <div class="sescf_whoweare_inner">
  	<h3><?php echo $this->translate("Who We Are"); ?></h3>
  	<div class="_lowertext"><?php echo $this->translate("An Organization set up to provide help and raise money for those in need."); ?></div>
  	<p><?php echo $this->translate("Our mission is to help people in need get help from our volunteers around the world and raise funds for their crowdfundings. Various campaigns running here require urgent help, so we appeal or donors to help as much as they can."); ?></p>
    <div class="block_head">
    	<div class="whoweare_blk">
    		<div class="icon">
    			<img src="application/modules/Sescrowdfunding/externals/images/project.png"/>
    		</div>
    		<div class="_desc">
    			<h4><?php echo $this->translate("Create Crowdfundings"); ?></h4>
    			<p><?php echo $this->translate("Get your crowdfundings created and get donations from people."); ?></p>
    		</div>
    	</div>
    	<div class="whoweare_blk">
    		<div class="icon">
    			<img src="application/modules/Sescrowdfunding/externals/images/donate.png"/>
    		</div>
    		<div class="_desc">
    			<h4><?php echo $this->translate("Donate Money"); ?></h4>
    			<p><?php echo $this->translate("Your donation will help people accomplish their mission."); ?></p>
    		</div>
    	</div>
    	<div class="whoweare_blk">
    		<div class="icon">
    			<img src="application/modules/Sescrowdfunding/externals/images/share.png"/>
    		</div>
    		<div class="_desc">
    			<h4><?php echo $this->translate("Easy Sharing"); ?></h4>
    			<p><?php echo $this->translate("Easy options to share crowdfundings & get more donations for them."); ?></p>
    		</div>
    	</div>
    	<div class="whoweare_blk">
    		<div class="icon">
    			<img src="application/modules/Sescrowdfunding/externals/images/support.png"/>
    		</div>
    		<div class="_desc">
    			<h4><?php echo $this->translate("Get Support"); ?></h4>
    			<p><?php echo $this->translate("Get support from our volunteers on your query, doubts, and suggestions."); ?></p>
    		</div>
    	</div>
    </div>
  </div>
</div>
