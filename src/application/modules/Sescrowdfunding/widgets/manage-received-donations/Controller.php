<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sescrowdfunding
 * @package    Sescrowdfunding
 * @copyright  Copyright 2019-2020 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Controller.php  2019-01-08 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

 use PhpOffice\PhpSpreadsheet\Spreadsheet;
 use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
 use PhpOffice\PhpSpreadsheet\IOFactory;
 use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class Sescrowdfunding_Widget_ManageReceivedDonationsController extends Engine_Content_Widget_Abstract {

  public function indexAction() {

    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
    if(empty($viewer->getIdentity()))
      return $this->setNoRender();

    if (isset($viewer->level_id) && !in_array($viewer->level_id,array(1,15,13))) {
      return $this->setNoRender();
    }

    if (isset($viewer->level_id) && in_array($viewer->level_id,array(15))) {
      $membershipTable = Engine_Api::_()->getDbtable('membership', 'sesgroup');
      $select = $membershipTable->select()->from($membershipTable->info('name'))->where('user_id =?',$viewer->user_id);
      $membership = $membershipTable->fetchRow($select);

      $values['group_id'] = $membership->resource_id;
    }else {
      if($_SERVER['REQUEST_METHOD'] === 'POST'){
        if ($_POST['group_id'] && $_POST['group_id'] != 'all')
          $this->view->group_id = $values['group_id'] = $group_id = $_POST['group_id'];

        if($_POST['crowdfunding_id'])
          $this->view->crowdfunding_id = $values['crowdfunding_id'] = $crowdfunding_id = $_POST['crowdfundings'];

        if ($_POST['category_id'] && !in_array($_POST['category_id'],array('all','reminder')) )
          $this->view->category_id = $values['category_id'] = $category_id = $_POST['category_id'];
      }
    }


    $groupTable = Engine_Api::_()->getDbtable('groups', 'sesgroup');

    $values['fetchAll'] = 1;
    $this->view->params = $values;
    $this->view->groups = $groups = $groupTable->getGroups($values);
    $this->view->crowdfundings = $crowdfundings = Engine_Api::_()->getItemTable('crowdfunding')->getSescrowdfundingsSelect($values);


    if ($_POST['type'] != 'Export')
      return;

    $category_id = $_POST['category_id'];

    if ($category_id == '53') {
      $this->exportAllPersepuluhan($groups,$crowdfundings);
      return;
    }

    if ($category_id == 'reminder') {
      $this->exportAllReminder($groups,$crowdfundings);
      return;
    }

    if ($_POST['crowdfunding_id']){
      $this->exportCrowdfunding($_POST['crowdfunding_id']);
      return;
    }

    $this->exportAllPersembahan($groups,$crowdfundings,$category_id);
    // $this->exportTest();
  }

  public function exportAllPersembahan($groups,$crowdfundings,$category_id = ''){
    $table = '';

    if ($category_id == '' || $category_id == 'all') {
      $title = 'Crowdfunding';
    }else {
      $category = Engine_Api::_()->getDbtable('categories','sescrowdfunding')->getCategoryDetail($category_id);
      $title = $category->category_name;
    }

    if (count($groups) >= 2) {
      $table .= '<tr><td colspan="8" style="border:0.1pt solid #ccc; text-align:center;">Amount report of all '.$title.' in all Church</td></tr>';
    }else {
      $table .= '<tr><td colspan="8" style="border:0.1pt solid #ccc; text-align:center;">Amount report of all '.$title.' in '.$groups[0]->getTitle().'</td></tr>';
    }


    $table .= '<tr><td colspan="8" style="border:0;">&nbsp;</td></tr>';
    foreach ($groups as $group) {
      $table .= '<tr><td colspan="8" style="border:0;">&nbsp;</td></tr>';
      $table .= '<tr><td colspan="8" style="border:0.1pt solid #ccc;">Amount report of all '.$title.' '.$group->getTitle().' </td></tr>';
      $table .= '<tr><td colspan="8" style="border:0;">&nbsp;</td></tr>';
      $table .= "</tr>
                    <th>No</th>
                    <th>Crowdfunding name</th>
                    <th>VA Code</th>
                    <th>Current Amount</th>
                    <th>Total Withdrawal</th>
                    <th>Latest Withdrawal</th>
                    <th>Total Amount</th>
                    <th>Status Crowdfunding</th>
                 </tr>";
      foreach ($crowdfundings as $crowdfunding) {
        if ($group->group_id == $crowdfunding->group_id) {
          $no = 1;
          $totalAmount = Engine_Api::_()->getDbtable('orders', 'sescrowdfunding')->getTotalOrderAmount(array('crowdfunding_id' => $crowdfunding->getIdentity()));
          $totalWithdrawalAmount = Engine_Api::_()->getDbtable('withdrawrequests', 'sescrowdfunding')->getTotalWithdrawalAmount(array('crowdfunding_id' => $crowdfunding->getIdentity()));
          $totalAvailableAmount = $totalAmount - $totalWithdrawalAmount;
            $table .= "<tr>";
            $table .= "<td>".$no++."</td>";
            $table .= "<td>".$crowdfunding->getTitle()."</td>";
            $table .= "<td>".$crowdfunding->va_number."</td>";
            $table .= "<td>".$totalAvailableAmount."</td>";
            $table .= "<td>".$totalWithdrawalAmount."</td>";
            $table .= "<td></td>";
            $table .= "<td>".$totalAmount."</td>";
            $table .= "<td>".($crowdfunding->approved == '1' ? 'Active' : 'Non-Active')."</td>";
            $table .= "</tr>";
        }
      }
    }
    // $html = '<script language="javascript">sesJqueryObject("html").empty();</script>';
    $html = "<body style='border:0.1pt solid #ccc'><table>$table</table></body>";

    $clean_html = str_replace("\r\n", "",$html) ;
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header("Content-Disposition: attachment;filename=Crowdfunding-report-" . time() . ".xls");
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: no-cache');

    echo $clean_html;
    exit;
  }

  public function exportAllReminder($groups,$crowdfundings){
    $table = '';

    if (count($groups) >= 2) {
      $table .= '<tr><td colspan="8" style="border:0.1pt solid #ccc; text-align:center;">Amount report of all Janji iman in all Church</td></tr>';
    }else {
      $table .= '<tr><td colspan="8" style="border:0.1pt solid #ccc; text-align:center;">Amount report of all Janji iman in '.$groups[0]->getTitle().'</td></tr>';
    }

    $table .= '<tr><td colspan="8" style="border:0;">&nbsp;</td></tr>';
    foreach ($groups as $group) {
      $table .= '<tr><td colspan="8" style="border:0;">&nbsp;</td></tr>';
      $table .= '<tr><td colspan="8" style="border:0.1pt solid #ccc;">Amount report of all Janji iman '.$group->getTitle().' </td></tr>';
      $table .= '<tr><td colspan="8" style="border:0;">&nbsp;</td></tr>';
      $table .= "</tr>
                    <th>No</th>
                    <th>Crowdfunding name</th>
                    <th>VA Code</th>
                    <th>Progress</th>
                    <th>Total Janji iman</th>
                    <th>Status Crowdfunding</th>
                 </tr>";
      foreach ($crowdfundings as $crowdfunding) {
        if ($group->group_id == $crowdfunding->group_id) {
          $no = 1;
          $value = array(
            'crowdfunding_id' => $crowdfunding->getIdentity(),
            'state' => 'complete',
            'reminder' => 1
          );
          $progressAmount = Engine_Api::_()->getDbtable('orders', 'sescrowdfunding')->getTotalOrderAmount($value);
          unset($value['state']);
          $totalAmount = Engine_Api::_()->getDbtable('orders', 'sescrowdfunding')->getTotalOrderAmount($value);
            $table .= "<tr>";
            $table .= "<td>".$no++."</td>";
            $table .= "<td>".$crowdfunding->getTitle()."</td>";
            $table .= "<td>".$crowdfunding->va_number."</td>";
            $table .= "<td>".number_format($progressAmount, 0, '', '.')."</td>";
            $table .= "<td>".number_format($totalAmount, 0, '', '.')."</td>";
            $table .= "<td>".($crowdfunding->approved == '1' ? 'Active' : 'Non-Active')."</td>";
            $table .= "</tr>";
        }
      }
    }
    $html = "<body style='border:0.1pt solid #ccc'><table>$table</table></body>";

    $clean_html = str_replace("\r\n", "",$html) ;
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header("Content-Disposition: attachment;filename=JanjiIman-report-" . time() . ".xls");
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: no-cache');

    echo $clean_html;
    exit;
  }

  public function exportAllPersepuluhan($groups,$crowdfundings){
    $table = '';
    if (count($groups) >= 2) {
      $table .= '<tr><td colspan="8" style="border:0.1pt solid #ccc; text-align:center;">Amount report of all Persepuluhan in all Church</td></tr>';
    }else {
      $table .= '<tr><td colspan="8" style="border:0.1pt solid #ccc; text-align:center;">Amount report of Persepuluhan in '.$groups[0]->getTitle().'</td></tr>';
    }

    $table .= '<tr><td colspan="8" style="border:0;">&nbsp;</td></tr>';
    $table .= '<tr><td colspan="8" style="border:0;">&nbsp;</td></tr>';
    $table .= "</tr>
                  <th>No</th>
                  <th>Church name</th>
                  <th>VA Code</th>
                  <th>Current Amount</th>
                  <th>Total Withdrawal</th>
                  <th>Latest Withdrawal</th>
                  <th>Total Amount</th>
                  <th>Status Crowdfunding</th>
               </tr>";
    foreach ($groups as $group) {
      foreach ($crowdfundings as $crowdfunding) {
        if ($group->group_id == $crowdfunding->group_id) {
          $no = 1;
          $totalAmount = Engine_Api::_()->getDbtable('orders', 'sescrowdfunding')->getTotalOrderAmount(array('crowdfunding_id' => $crowdfunding->getIdentity()));
          $totalWithdrawalAmount = Engine_Api::_()->getDbtable('withdrawrequests', 'sescrowdfunding')->getTotalWithdrawalAmount(array('crowdfunding_id' => $crowdfunding->getIdentity()));
          $totalAvailableAmount = $totalAmount - $totalWithdrawalAmount;
            $table .= "<tr>";
            $table .= "<td>".$no++."</td>";
            $table .= "<td>".$group->getTitle()."</td>";
            $table .= "<td>".$crowdfunding->va_number."</td>";
            $table .= "<td>".$totalAvailableAmount."</td>";
            $table .= "<td>".$totalWithdrawalAmount."</td>";
            $table .= "<td></td>";
            $table .= "<td>".$totalAmount."</td>";
            $table .= "<td>".($crowdfunding->approved == '1' ? 'Active' : 'Non-Active')."</td>";
            $table .= "</tr>";
        }
      }
    }
    // $html = '<script language="javascript">sesJqueryObject("html").empty();</script>';
    $html = "<body style='border:0.1pt solid #ccc'><table>$table</table></body>";

    $clean_html = str_replace("\r\n", "",$html) ;
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header("Content-Disposition: attachment;filename=Persepuluhan-report-" . time() . ".xls");
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: no-cache');

    echo $clean_html;
    exit;
  }

  public function exportCrowdfunding($crowdfunding_id){
    $crowdfunding = Engine_Api::_()->getDbtable('crowdfundings', 'sescrowdfunding')->getCrowdfunding($crowdfunding_id);
    $group = Engine_Api::_()->getDbTable('groups','sesgroup')->getGroups(array('group_id' => $crowdfunding->group_id,'fetchRow' => true));
    $totalAmount = Engine_Api::_()->getDbtable('orders', 'sescrowdfunding')->getTotalOrderAmount(array('state' => 'complete','crowdfunding_id' => $crowdfunding->getIdentity()));
    $totalWithdrawalAmount = Engine_Api::_()->getDbtable('withdrawrequests', 'sescrowdfunding')->getTotalWithdrawalAmount(array('crowdfunding_id' => $crowdfunding->getIdentity()));
    $totalAvailableAmount = $totalAmount - $totalWithdrawalAmount;


    $spreadsheet = new Spreadsheet();

    $spreadsheet->setActiveSheetIndex(0)->setCellValue('A1', 'Withdrawal report of '.$crowdfunding->getTitle().' in '.$group->getTitle());
    $spreadsheet->getActiveSheet()->mergeCells('A1:E1');
    $spreadsheet->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal('center');

    $spreadsheet->getActiveSheet()->setCellValue('A3', 'Withdrawal report of '.$crowdfunding->getTitle())->mergeCells('A3:C3');
    $spreadsheet->getActiveSheet()->setCellValue('A4', $crowdfunding->va_number)->mergeCells('A4:C4');
    $spreadsheet->getActiveSheet()->setCellValue('A5', 'Status '.(($crowdfunding->approved == 1) ? 'Active' : 'Non-Active'))->mergeCells('A5:C5');

    $spreadsheet->getActiveSheet()->setCellValue('D3','Current amount');
    $spreadsheet->getActiveSheet()->setCellValue('D4','Total withdrawal');
    $spreadsheet->getActiveSheet()->setCellValue('D5','Total Ammount');
    $spreadsheet->getActiveSheet()->setCellValue('E3',$totalAvailableAmount);
    $spreadsheet->getActiveSheet()->setCellValue('E4',$totalWithdrawalAmount);
    $spreadsheet->getActiveSheet()->setCellValue('E5',$totalAmount);

    $spreadsheet->getActiveSheet()->setCellValue('A8','No');
    $spreadsheet->getActiveSheet()->setCellValue('B8','Requester name');
    $spreadsheet->getActiveSheet()->setCellValue('C8','NPA');
    $spreadsheet->getActiveSheet()->setCellValue('D8','Amount');
    $spreadsheet->getActiveSheet()->setCellValue('E8','Transaction date');
    $spreadsheet->getActiveSheet()->setCellValue('F8','Account number');
    $spreadsheet->getActiveSheet()->setCellValue('G8','Account name');
    $spreadsheet->getActiveSheet()->setCellValue('H8','Bank');

    $value = array('crowdfunding_id' => $crowdfunding_id);
    $data = Engine_Api::_()->getDbtable('withdrawrequests','sescrowdfunding')->getWithdrawalRequests($value);
    $no = 1;
    $cellNumber = 8;
    foreach ($data as $item) {
      $cellNumber ++;
      $user = Engine_Api::_()->getDbtable('users', 'user')->getUser(array('user_id' => $item->user_id));
      $spreadsheet->getActiveSheet()->setCellValue('A'.$cellNumber,$no++);
      $spreadsheet->getActiveSheet()->setCellValue('B'.$cellNumber,$user->displayname);
      $spreadsheet->getActiveSheet()->setCellValue('C'.$cellNumber,'');
      $spreadsheet->getActiveSheet()->setCellValue('D'.$cellNumber,$item->requested_amount);
      $spreadsheet->getActiveSheet()->setCellValue('E'.$cellNumber,$item->creation_date);
      $spreadsheet->getActiveSheet()->setCellValue('F'.$cellNumber,$item->account_number);
      $spreadsheet->getActiveSheet()->setCellValue('G'.$cellNumber,$item->account_name);
      $spreadsheet->getActiveSheet()->setCellValue('H'.$cellNumber,$item->bank_name);
    }

    $spreadsheet->getActiveSheet()->setTitle('Withdrawals');

    $newspreadsheet = new Spreadsheet();

    $newspreadsheet->setActiveSheetIndex(0)->setCellValue('A1', 'Donors report of '.$crowdfunding->getTitle().' in  '.$group->getTitle());
    $newspreadsheet->getActiveSheet()->mergeCells('A1:E1');
    $newspreadsheet->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal('center');

    $newspreadsheet->getActiveSheet()->setCellValue('A3', 'Donors report of '.$crowdfunding->getTitle())->mergeCells('A3:C3');
    $newspreadsheet->getActiveSheet()->setCellValue('A4', $crowdfunding->va_number)->mergeCells('A4:C4');
    $newspreadsheet->getActiveSheet()->setCellValue('A5', 'Status '.(($crowdfunding->approved == 1) ? 'Active' : 'Non-Active'))->mergeCells('A5:C5');

    $newspreadsheet->getActiveSheet()->setCellValue('D3','Current amount');
    $newspreadsheet->getActiveSheet()->setCellValue('D4','Total withdrawal');
    $newspreadsheet->getActiveSheet()->setCellValue('D5','Total Ammount');
    $newspreadsheet->getActiveSheet()->setCellValue('E3',$totalAvailableAmount);
    $newspreadsheet->getActiveSheet()->setCellValue('E4',$totalWithdrawalAmount);
    $newspreadsheet->getActiveSheet()->setCellValue('E5',$totalAmount);

    $newspreadsheet->getActiveSheet()->setCellValue('A8','No');
    $newspreadsheet->getActiveSheet()->setCellValue('B8','Donor name');
    $newspreadsheet->getActiveSheet()->setCellValue('C8','NPA');
    $newspreadsheet->getActiveSheet()->setCellValue('D8','Amount');
    $newspreadsheet->getActiveSheet()->setCellValue('E8','Transaction date');

    $data = Engine_Api::_()->getDbtable('orders', 'sescrowdfunding')->getReportData($value);
    $no = 1;
    $cellNumber = 8;
    foreach ($data as $item) {
      $cellNumber ++;
      $user = Engine_Api::_()->getDbtable('users', 'user')->getUser(array('user_id' => $item->user_id));
      $newspreadsheet->getActiveSheet()->setCellValue('A'.$cellNumber,$no++);
      $newspreadsheet->getActiveSheet()->setCellValue('B'.$cellNumber,$user->displayname);
      $newspreadsheet->getActiveSheet()->setCellValue('C'.$cellNumber,'');
      $newspreadsheet->getActiveSheet()->setCellValue('D'.$cellNumber,$item->total_useramount);
      $newspreadsheet->getActiveSheet()->setCellValue('E'.$cellNumber,$item->creation_date);
    }

    $newspreadsheet->getActiveSheet()->setTitle('Donors');
    $spreadsheet->addSheet($newspreadsheet->getActiveSheet());

    $newspreadsheet = new Spreadsheet();

    $newspreadsheet->setActiveSheetIndex(0)->setCellValue('A1', 'Janji iman report of '.$crowdfunding->getTitle().' in  '.$group->getTitle());
    $newspreadsheet->getActiveSheet()->mergeCells('A1:E1');
    $newspreadsheet->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal('center');

    $newspreadsheet->getActiveSheet()->setCellValue('A3', 'Janji iman report of '.$crowdfunding->getTitle())->mergeCells('A3:C3');
    $newspreadsheet->getActiveSheet()->setCellValue('A4', $crowdfunding->va_number)->mergeCells('A4:C4');
    $newspreadsheet->getActiveSheet()->setCellValue('A5', 'Status '.(($crowdfunding->approved == 1) ? 'Active' : 'Non-Active'))->mergeCells('A5:C5');

    $newspreadsheet->getActiveSheet()->setCellValue('D3','Current amount');
    $newspreadsheet->getActiveSheet()->setCellValue('D4','Total withdrawal');
    $newspreadsheet->getActiveSheet()->setCellValue('D5','Total Ammount');
    $newspreadsheet->getActiveSheet()->setCellValue('E3',$totalAvailableAmount);
    $newspreadsheet->getActiveSheet()->setCellValue('E4',$totalWithdrawalAmount);
    $newspreadsheet->getActiveSheet()->setCellValue('E5',$totalAmount);

    $newspreadsheet->getActiveSheet()->setCellValue('A8','No');
    $newspreadsheet->getActiveSheet()->setCellValue('B8','Donor name');
    $newspreadsheet->getActiveSheet()->setCellValue('C8','NPA');
    $newspreadsheet->getActiveSheet()->setCellValue('D8','Progress');
    $newspreadsheet->getActiveSheet()->setCellValue('E8','Total Janji iman');
    $newspreadsheet->getActiveSheet()->setCellValue('F8','Transaction date');

    $value['reminder'] = 1;
    $data = Engine_Api::_()->getDbtable('orders', 'sescrowdfunding')->getDonors($value);
    $no = 1;
    $cellNumber = 8;
    foreach ($data as $item) {
      $cellNumber ++;
      $param = array(
        'crowdfunding_id' => $crowdfunding->getIdentity(),
        'state' => 'complete',
        'reminder' => 1,
        'user_id'=> $item->user_id
      );
      $progressAmount = Engine_Api::_()->getDbtable('orders', 'sescrowdfunding')->getTotalOrderAmount($param);
      unset($param['state']);
      $totalAmount = Engine_Api::_()->getDbtable('orders', 'sescrowdfunding')->getTotalOrderAmount($param);

      $user = Engine_Api::_()->getDbtable('users', 'user')->getUser(array('user_id' => $item->user_id));
      $newspreadsheet->getActiveSheet()->setCellValue('A'.$cellNumber,$no++);
      $newspreadsheet->getActiveSheet()->setCellValue('B'.$cellNumber,$user->displayname);
      $newspreadsheet->getActiveSheet()->setCellValue('C'.$cellNumber,'');
      $newspreadsheet->getActiveSheet()->setCellValue('D'.$cellNumber,$progressAmount);
      $newspreadsheet->getActiveSheet()->setCellValue('E'.$cellNumber,$totalAmount);
      $newspreadsheet->getActiveSheet()->setCellValue('F'.$cellNumber,'');
    }

    $newspreadsheet->getActiveSheet()->setTitle('Janji Iman');
    $spreadsheet->addSheet($newspreadsheet->getActiveSheet());

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="01simple.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer->save('php://output');
    exit;
  }

  public function exportTest(){
    // Create new Spreadsheet object
    $spreadsheet = new Spreadsheet();

    $spreadsheet->setActiveSheetIndex(0)->setCellValue('A1', 'Withdrawal report of Persembahan Karawaci in GRII Karawaci');
    $spreadsheet->getActiveSheet()->mergeCells('A1:E1');
    $spreadsheet->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal('center');

    $spreadsheet->getActiveSheet()->setCellValue('A3','No');
    $spreadsheet->getActiveSheet()->setCellValue('B3','Requester name');
    $spreadsheet->getActiveSheet()->setCellValue('C3','NPA');
    $spreadsheet->getActiveSheet()->setCellValue('D3','Amount');
    $spreadsheet->getActiveSheet()->setCellValue('E3','Transaction date');
    $spreadsheet->getActiveSheet()->setCellValue('F3','Account number');
    $spreadsheet->getActiveSheet()->setCellValue('G3','Account name');
    $spreadsheet->getActiveSheet()->setCellValue('H3','Bank');

    // Rename worksheet
    $spreadsheet->getActiveSheet()->setTitle('Simple');

    // Clone worksheet
    $clonedSheet = new Spreadsheet();
    $clonedSheet->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Goodbye')
        ->setCellValue('A2', 'cruel')
        ->setCellValue('C1', 'Goodbye')
        ->setCellValue('C2', 'cruel');

    // Rename cloned worksheet
    $clonedSheet->getActiveSheet()->setTitle('Simple Clone');

    $spreadsheet->addSheet($clonedSheet->getActiveSheet());
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="01simple.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer->save('php://output');
    exit;
  }

}
