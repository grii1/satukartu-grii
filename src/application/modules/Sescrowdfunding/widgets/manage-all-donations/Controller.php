<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sescrowdfunding
 * @package    Sescrowdfunding
 * @copyright  Copyright 2019-2020 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Controller.php  2019-01-08 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

class Sescrowdfunding_Widget_ManageAlldonationsController extends Engine_Content_Widget_Abstract {

  public function indexAction() {

    $this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
    if(empty($viewer->getIdentity()))
      return $this->setNoRender();

    if (isset($viewer->level_id) && !in_array($viewer->level_id,array(1,15,13))) {
      return $this->setNoRender();
    }

    if (isset($viewer->level_id) && in_array($viewer->level_id,array(15))) {
      $membershipTable = Engine_Api::_()->getDbtable('membership', 'sesgroup');
      $select = $membershipTable->select()->from($membershipTable->info('name'))->where('user_id =?',$viewer->user_id);
      $membership = $membershipTable->fetchRow($select);

      $values['group_id'] = $membership->resource_id;
    }
    $groupTable = Engine_Api::_()->getDbtable('groups', 'sesgroup');

    $values['fetchAll'] = 1;
    $this->view->groups = $groupTable->getGroups($values);
    $this->view->crowdfundings = Engine_Api::_()->getItemTable('crowdfunding')->getSescrowdfundingsSelect($values);

    $withdrawTable = Engine_Api::_()->getDbtable('withdrawrequests', 'sescrowdfunding');
    $withdrawTableName = $withdrawTable->info('name');

    $crowdfundingTable = Engine_Api::_()->getDbtable('crowdfundings', 'sescrowdfunding');
    $crowdfundingTableName = $crowdfundingTable->info('name');

    $groupTable = Engine_Api::_()->getDbtable('groups', 'sesgroup');
    $groupTableName = $groupTable->info('name');

    $select = $withdrawTable->select()
                            ->from($withdrawTableName,array('*'))
                            ->setIntegrityCheck(false)
                            ->joinLeft($crowdfundingTableName, "$withdrawTableName.crowdfunding_id = $crowdfundingTableName.crowdfunding_id", array('crowdfunding_id','title as crowdfunding_name','va_number','approved as crowdfunding_status'))
                            ->joinLeft($groupTableName, "$groupTableName.group_id = $crowdfundingTableName.group_id", array('group_id','title as group_name'));

    if (isset($values['group_id'])) {
      $select->where($groupTableName.'.group_id = ?',$values['group_id']);
    }

    $this->view->groups = $groupTable->getGroups($values);
    $this->view->crowdfundings = Engine_Api::_()->getItemTable('crowdfunding')->getSescrowdfundingsSelect($values);
    $this->view->withdrawals = $withdrawTable->fetchAll($select);

  }
}
