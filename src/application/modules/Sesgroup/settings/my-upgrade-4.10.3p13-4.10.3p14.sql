UPDATE `engine4_activity_notificationtypes` SET `type` = 'sesgroup_waitingadminapproval' WHERE `engine4_activity_notificationtypes`.`type` = 'sesgroup_group_waitingadminapproval';

UPDATE `engine4_activity_notificationtypes` SET `type` = 'sesgroup_gpsijoinedjoin' WHERE `engine4_activity_notificationtypes`.`type` = 'sesgroup_group_groupsijoinedjoin';

UPDATE `engine4_activity_notificationtypes` SET `type` = 'sesgroup_waitingapproval' WHERE `engine4_activity_notificationtypes`.`type` = 'sesgroup_group_waitingapproval';

UPDATE `engine4_activity_notificationtypes` SET `type` = 'sesgroup_gpsijoinedlike' WHERE `engine4_activity_notificationtypes`.`type` = 'sesgroup_group_groupsijoinedlike';

UPDATE `engine4_activity_notificationtypes` SET `type` = 'sesgroup_gpsijoinedalbum' WHERE `engine4_activity_notificationtypes`.`type` = 'sesgroup_group_groupsijoinedalbum';

UPDATE `engine4_activity_notificationtypes` SET `type` = 'sesgroup_gpsijoinedphoto' WHERE `engine4_activity_notificationtypes`.`type` = 'sesgroup_group_groupsijoinedphoto';

UPDATE `engine4_activity_notificationtypes` SET `type` = 'sesgroup_gpjoinednamechange' WHERE `engine4_activity_notificationtypes`.`type` = 'sesgroup_group_groupsijoinedgroupnamechanged';

UPDATE `engine4_activity_notificationtypes` SET `type` = 'sesgroup_gpsifollowedlike' WHERE `engine4_activity_notificationtypes`.`type` = 'sesgroup_group_groupsifollowedlike';

UPDATE `engine4_activity_notificationtypes` SET `type` = 'sesgroup_gpsifollowedjoin' WHERE `engine4_activity_notificationtypes`.`type` = 'sesgroup_group_groupsifollowedjoin';

UPDATE `engine4_activity_notificationtypes` SET `type` = 'sesgroup_gpsifollowedalbum' WHERE `engine4_activity_notificationtypes`.`type` = 'sesgroup_group_groupsifollowedalbum';

UPDATE `engine4_activity_notificationtypes` SET `type` = 'sesgroup_gpsifollowedphoto' WHERE `engine4_activity_notificationtypes`.`type` = 'sesgroup_group_groupsifollowedphoto';

UPDATE `engine4_activity_notificationtypes` SET `type` = 'sesgroup_gpfollowednamechange' WHERE `engine4_activity_notificationtypes`.`type` = 'sesgroup_group_groupsifollowedgroupnamechanged';  

UPDATE `engine4_activity_notificationtypes` SET `type` = 'sesgroup_gpfavouritednamechange' WHERE `engine4_activity_notificationtypes`.`type` = 'sesgroup_group_groupsifavousitegroupnamechanged'; 
