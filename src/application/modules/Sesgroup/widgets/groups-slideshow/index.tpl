<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesgroup
 * @package    Sesgroup
 * @copyright  Copyright 2017-2018 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: index.tpl  2018-04-23 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
 
?>

<?php $identity = $this->identity;?>
<?php $this->headLink()->appendStylesheet($this->layout()->staticBaseUrl . 'application/modules/Sesgroup/externals/styles/styles.css'); ?>
<?php if($this->params['enableSlideshow']){ ?>
<?php 
  $baseURL = $this->layout()->staticBaseUrl;
  $this->headScript()->appendFile($baseURL . 'application/modules/Sesgroup/externals/scripts/jquery.js');
  $this->headScript()->appendFile($baseURL . 'application/modules/Sesgroup/externals/scripts/owl.carousel.js'); 
?>
 <?php } ?>
<?php $viewer = Engine_Api::_()->user()->getViewer();?>
<?php $viewerId = $viewer->getIdentity();?>
<div class="sesgroup_groups_slideshow_wrapper sesbasic_clearfix sesbasic_bxs">
  <div class="sesgroup_groups_slideshow_container sesbasic_clearfix sesgroup_fg_color">

    <div class="_right_col <?php if(empty($this->params['leftGroup'])) {?>_noleftblock<?php } ?>" <?php if(empty($this->params['enableSlideshow'])) { ?> style="display:none;" <?php } ?>>
      <div class="sesgroup_groups_slideshow sesgroup_groups_slideshow_<?php echo $this->identity;?>" style="height:<?php echo $this->params['height'] ?>px;">
        <?php $leftData = array();?>
        <?php foreach($this->groups as $data):?>
          <?php if($data['type'] == 'left'):?>
             <?php $leftData[] = $data['group_id']; ?>
             <?php continue;?>
          <?php endif;?>
          <?php $group = Engine_Api::_()->getItem('sesgroup_group', $data['group_id']);?>
          <div class="sesgroup_groups_slideshow_item sesbasic_clearfix">
            <div class="_thumb sesgroup_thumb" style="height:<?php echo $this->params['height'] ?>px;"> 
              <a href="<?php echo $group->getHref(); ?>" class="sesgroup_thumb_img">
                <span style="background-image:url(<?php echo $group->getPhotoUrl('thumb.profile'); ?>);"></span>
              </a>
              <?php if(isset($this->featuredLabelActive) || isset($this->sponsoredLabelActive) || isset($this->hotLabel)):?>
                <div class="sesgroup_list_labels sesbasic_animation">
                  <?php include APPLICATION_PATH .  '/application/modules/Sesgroup/views/scripts/_dataLabel.tpl';?>
                </div>
              <?php endif;?>
              <div class="_btns sesbasic_animation"> 
                <?php include APPLICATION_PATH .  '/application/modules/Sesgroup/views/scripts/_dataButtons.tpl';?>
              </div>
            </div>
            <div class="sesgroup_groups_slideshow_cont_wrap">   
              <?php if(isset($this->titleActive)):?>
                <?php if(strlen($group->getTitle()) > $this->params['title_truncation']):?>
                  <?php $title = mb_substr($group->getTitle(),0,$this->params['title_truncation']).'...';?>
                <?php else: ?>
                  <?php $title = $group->getTitle();?>
                <?php endif; ?>
                <div class="_title">
                  <a href="<?php echo $group->getHref();?>"><?php echo $title;?></a><?php if(isset($this->verifiedLabelActive)&& $group->verified):?><i class="sesgroup_label_verified sesbasic_verified_icon" title='<?php echo $this->translate("Verified");?>'></i><?php endif;?>
                </div>
              <?php endif;?>
              <div class="sesgroup_groups_slideshow_cont">
              	<div class="_left">  
                	<div class="_stats">
                    <?php if(SESGROUPSHOWUSERDETAIL == 1 && isset($this->byActive)):?>
                      <?php $owner = $group->getOwner();?>
                      <span>
                        <i class="fa fa-user"></i>	
                        <span><?php echo $this->translate('by');?>&nbsp;<?php echo $this->htmlLink($owner->getHref(), $owner->getTitle());?></span>
                      </span>
                    <?php endif;?>
                  </div>
                  <div class="_stats">
                    <span>
                    	<i class="fa fa-bar-chart"></i>	
                      <span><?php include APPLICATION_PATH .  '/application/modules/Sesgroup/views/scripts/_dataStatics.tpl';?></span>
                  	</span>
                  </div>  
                  <div class="_stats">
                  	<?php if(isset($this->categoryActive)):?>
                      <?php if (!empty($group->category_id)):?>
                      <?php $category = Engine_Api::_ ()->getDbtable('categories', 'sesgroup')->find($group->category_id)->current();?>
                      <?php endif;?>
                      <span>
                        <i class="fa fa-folder-open"></i>
                        <span><a href="<?php echo $group->getHref();?>"><?php echo $category->category_name;?></a></span>
                      </span>
                  	<?php endif;?>
                    <?php if(isset($this->locationActive) && $group->location && Engine_Api::_()->getApi('settings', 'core')->getSetting('sesgroup_enable_location', 1)):?>
                      <span class="_location">
                        <i class="fa fa-map-marker" title="<?php echo $this->translate('Location');?>"></i>
                        <span title="<?php echo $group->location;?>"><?php if(Engine_Api::_()->getApi('settings','core')->getSetting('sesgroup.enable.map.integration', 1)):?><a href="<?php echo $this->url(array('resource_id' => $group->group_id,'resource_type'=>'sesgroup_group','action'=>'get-direction'), 'sesbasic_get_direction', true);?>" class="openSmoothbox"><?php echo $group->location;?></a><?php else:?><?php echo $group->location;?><?php endif;?></span>
                      </span>
                    <?php endif;?>
                  </div>
                  <?php if(isset($this->descriptionActive)):?>
                    <div class="_des">
                      <p><?php echo $this->string()->truncate($this->string()->stripTags($group->description), $this->params['description_truncation']) ?></p>
                    </div>
                  <?php endif;?>
                  <?php if(isset($this->contactDetailActive) && (isset($group->group_contact_phone) || isset($group->group_contact_email) || isset($group->group_contact_website))):?>
                    <div class="_contactlinks sesbasic_clearfix sesbasic_animation">
                      <?php if($group->group_contact_phone):?>
                        <span>
                          <?php if(SESGROUPSHOWCONTACTDETAIL == 1):?>
                            <a href="javascript:void(0);" onclick="sessmoothboxDialoge('<?php echo $group->group_contact_phone ;?>');"><?php echo $this->translate("View Phone No")?></a>
                          <?php else:?>
                            <a href="<?php echo $this->url(array('action' => 'show-login-page'),'sesgroup_general',true);?>" class="smoothbox"><?php echo $this->translate("View Phone No")?></a>
                          <?php endif;?>
                        </span>
                      <?php endif;?>
                      <?php if($group->group_contact_email):?>
                        <span>
                          <?php if(SESGROUPSHOWCONTACTDETAIL == 1):?>
                            <a href='mailto:<?php echo $group->group_contact_email ?>'><?php echo $this->translate("Send Email")?></a>
                          <?php else:?>
                            <a href="<?php echo $this->url(array('action' => 'show-login-page'),'sesgroup_general',true);?>" class="smoothbox"><?php echo $this->translate("Send Email")?></a>
                          <?php endif;?>
                        </span>
                      <?php endif;?>
                      <?php if($group->group_contact_website):?>
                        <span>
                          <?php if(SESGROUPSHOWCONTACTDETAIL == 1):?>
                            <a href="<?php echo parse_url($group->group_contact_website, PHP_URL_SCHEME) === null ? 'http://' . $group->group_contact_website : $group->group_contact_website; ?>" target="_blank" ><?php echo $this->translate("Visit Website")?></a>
                          <?php else:?>
                            <a href="<?php echo $this->url(array('action' => 'show-login-page'),'sesgroup_general',true);?>" class="smoothbox"><?php echo $this->translate("Visit Website")?></a>
                          <?php endif;?>
                        </span>
                      <?php endif;?>
                    </div>
                  <?php endif;?>
                </div>
                <div class="_buttons">
                	<?php include APPLICATION_PATH .  '/application/modules/Sesgroup/views/scripts/_dataSharing.tpl';?>
              	</div>
              </div>
            </div>
          </div>	
        <?php endforeach;?>
      </div>
    </div>
    <?php if($this->params['leftGroup']) { 
      $height = ($this->params['height'] -20) / 3;
    ?>
      <div class="_left_col <?php if(empty($this->params['enableSlideshow'])) {?>_norightblock<?php } ?>">
        <?php for($i=0;$i<count($leftData);$i++):?>
          <?php $group = Engine_Api::_()->getItem('sesgroup_group', $leftData[$i]);?>
          <div class="sesgroup_groups_slideshow_left_item">
            <div class="_thumb" style="height:<?php echo $height ?>px;">
              <a href="<?php echo $group->getHref(); ?>" class="_thumbimg">
                <span style="background-image:url(<?php echo $group->getPhotoUrl('thumb.profile'); ?>);"></span>
              </a>
            </div>
            <div class="_cont">    
              <?php if(isset($this->titleActive)):?>
                <div class="_title">
                  <a href="<?php echo $group->getHref();?>"><?php echo $group->getTitle();?></a><?php if(isset($this->verifiedLabelActive)&& $group->verified):?><i class="sesgroup_label_verified sesbasic_verified_icon" title='<?php echo $this->translate("Verified");?>'></i><?php endif;?>
                </div>
              <?php endif;?>
              <?php if(isset($this->categoryActive)):?>
                <div class="_stats sesbasic_clearfix">
                  <?php if (!empty($group->category_id)):?>
                  <?php $category = Engine_Api::_ ()->getDbtable('categories', 'sesgroup')->find($group->category_id)->current();?>
                  <?php endif;?>
                  <i class="fa fa-folder-open"></i>
                  <span><a href="<?php echo $group->getHref();?>"><?php echo $category->category_name;?></a></span>
                </div>
              <?php endif;?>
            </div>  
          </div>
        <?php endfor;?>
      </div>
    <?php } ?>
  </div>
</div>
<?php if($this->enableSlideshow): ?>
  <style>
  .sesgroup_groups_slideshow .owl-prev,
	.sesgroup_groups_slideshow .owl-next {
    display:block !important;
  }
  </style>
<?php endif; ?>
<script type="text/javascript">
//Slideshow widget
sesgroupJqueryObject(document).ready(function() {
  var sesgroupElement = sesJqueryObject('.sesgroup_groups_slideshow_<?php echo $this->identity;?>');
	if(sesgroupElement.length > 0) {
    var sesgroupElements = sesgroupJqueryObject('.sesgroup_groups_slideshow_<?php echo $this->identity;?>');
    sesgroupElements.each(function(){
      sesgroupJqueryObject(this).owlCarousel({
        loop:true,
        items:1,
        margin:0,
        autoHeight:true,
        autoplay:<?php echo $this->params['autoplay'] ?>,
        autoplayTimeout:<?php echo $this->params['speed'] ?>,
        autoplayHoverPause:true
      });
      sesgroupJqueryObject(".owl-prev").html('<i class="fa fa-angle-left"></i>');
      sesgroupJqueryObject(".owl-next").html('<i class="fa fa-angle-right"></i>');
    });
	}
});
</script>
<style type="text/css">
  <?php if($this->params['navigation'] == 2){?>
    .sesgroup_groups_slideshow_<?php echo $this->identity;?> .owl-dots{
      display:none;
    }
    .sesgroup_groups_slideshow_<?php echo $this->identity;?> .owl-nav > div{
      display:block !important;
    }
  <?php } else{ ?>
    .sesgroup_groups_slideshow_<?php echo $this->identity;?> .owl-nav{
       display:none;
    }
  <?php } ?>
	.sespage_groups_slideshow_<?php echo $this->identity;?> .owl-stage-outer{
		height:<?php echo $this->params['height'] ?>px !important;
	}
</style>