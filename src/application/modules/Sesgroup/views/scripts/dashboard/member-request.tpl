<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesgroup
 * @package    Sesgroup
 * @copyright  Copyright 2017-2018 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: reports.tpl  2018-04-23 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

?>

<style>
.sescf_payment_request_table table,.sescf_group_reports_table table,.sescf_paymentmade_table table{
  width:100%;
  box-shadow:0 2px #fbfbfb;
}
.sescf_payment_request_table thead,.sescf_group_reports_table thead,.sescf_paymentmade_table thead{
  background:url(../images/transprant-bg.png);
}
.sescf_payment_request_table tr th,.sescf_payment_request_table tr td,.sescf_group_reports_table tr th,.sescf_group_reports_table tr td,.sescf_paymentmade_table tr th,.sescf_paymentmade_table tr td{
  text-align:center;
  padding:8px 0;
}
.sescf_payment_request_table .table_options a,.sescf_group_reports_table .table_options a{
  margin:0 2px;
}
</style>

<?php $this->headLink()->appendStylesheet($this->layout()->staticBaseUrl . 'application/modules/Sesgroup/externals/styles/jquery.timepicker.css'); ?>
<?php $this->headLink()->appendStylesheet($this->layout()->staticBaseUrl . 'application/modules/Sesgroup/externals/styles/bootstrap-datepicker.css'); ?>
<?php $this->headScript()->appendFile($this->layout()->staticBaseUrl . 'application/modules/Sesbasic/externals/scripts/jquery1.11.js'); ?>
<?php $this->headScript()->appendFile($this->layout()->staticBaseUrl . 'application/modules/Sesgroup/externals/scripts/jquery.timepicker.js'); ?>
<?php $this->headScript()->appendFile($this->layout()->staticBaseUrl . 'application/modules/Sesgroup/externals/scripts/bootstrap-datepicker.js'); ?>
<?php ?>
<?php if(!$this->is_ajax){
echo $this->partial('dashboard/left-bar.tpl', 'sesgroup', array(
	'group' => $this->group,
      ));
?>
<div class="sesgroup_dashboard_content sesbm sesbasic_clearfix sesbasic_bxs">
  <div class="sesgroup_dashboard_content_header sesbasic_clearfix">
    <h3><?php echo $this->translate('Member Request'); ?></h3>
    <p><?php echo $this->translate(''); ?></p>
  </div>
<?php }	?>
<div class="sescf_group_reports_table sesbasic_dashboard_table sesbasic_bxs" style="overflow-x:auto;">
  <table class="table">
    <thead style="background:#e4e4e4;">
      <th>No</th>
      <th>Name</th>
      <th>Email</th>
      <th>Signup Date</th>
      <th>Options</th>
    </thead>
    <tbody>
      <?php foreach($this->members as $index => $member): ?>
      <tr>
          <td><?php echo $index + 1 ?></td>
          <td><?php echo $member['name'] ?></td>
          <td><?php echo $member['email']?></td>
          <td><?php echo $member['signup_date']?></td>
          <!-- <td><button onclick="window.location.href='test'">Approve</button></td> -->
          <td><a class="smoothbox" href="<?php echo $this->url(array('module'=>'sesgroup','controller'=>'dashboard', 'action' => 'member-approve', 'user_id' => $member['id'] )) ?>">Approve</a> </td>
      </tr>
    <?php endforeach; ?>
    </tbody>
  </table>
</div>


<?php if(!$this->is_ajax){ ?>
</div>
</div>
</div>
<?php  } ?>
