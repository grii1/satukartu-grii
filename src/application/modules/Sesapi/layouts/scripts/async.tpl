<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesapi
 * @package    Sesapi
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: async.tpl  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
 
 ?>
<html>
  <head>
    <script type="text/javascript">
      parent.en4.core.dloader.handleLoad(<?php echo Zend_Json::encode($this->layout()->content) ?>);
    </script>
  </head>
  <body>

  </body>
</html>
