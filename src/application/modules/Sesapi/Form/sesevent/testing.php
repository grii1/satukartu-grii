<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesapi
 * @package    Sesapi
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: testing.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
class Sesapi_Form_sesevent_testing extends Engine_Form
{
    public function init(){
         $this->setMethod('post');
         
          $this->addElement('File', 'image', array(
        'label' => 'Upload Icon',
        'description' => 'Upload an icon. (The Recommended dimensions of the icon: 40px * 40px.]'
    ));
           $this->addElement('Button', 'submit', array(
        'label' => 'upload',
        'type' => 'submit',
        'ignore' => true,
        
    ));
           
           
    }
 
}
