<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesapi
 * @package    Sesapi
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: ReportController.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
class Sesapi_ReportController extends Sesapi_Controller_Action_Standard
{
  public function init()
  {  
    $this->_helper->requireUser();
    $this->_helper->requireSubject();
  }
  
  public function createAction()
  {
    $this->view->subject = $subject = Engine_Api::_()->core()->getSubject();
    $this->view->form = $form = new Sesapi_Form_Report();
    if($this->_getParam('getForm')){
      $formFields = Engine_Api::_()->getApi('FormFields','sesapi')->generateFormFields($form);
      $this->generateFormFields($formFields);
    }
    if( !$form->isValid($this->getRequest()->getPost()) )
      Engine_Api::_()->getApi('response','sesapi')->sendResponse(array('error'=>'1','error_message'=>'validation_error','result'=>array()));

    // Process
    $table = Engine_Api::_()->getItemTable('core_report');
    $db = $table->getAdapter();
    $db->beginTransaction();
    try
    {
      $viewer = Engine_Api::_()->user()->getViewer();
      $report = $table->createRow();
      $report->setFromArray(array_merge($form->getValues(), array(
        'description'=>$_POST['des'],
        'subject_type' => $subject->getType(),
        'subject_id' => $subject->getIdentity(),
        'user_id' => $viewer->getIdentity(),
      )));
      $report->save();
      // Increment report count
      Engine_Api::_()->getDbtable('statistics', 'core')->increment('core.reports');
      $db->commit();
      Engine_Api::_()->getApi('response','sesapi')->sendResponse(array('error'=>'0','error_message'=>'','result'=>"Your report has been submitted."));
    }
    catch( Exception $e )
    {
      $db->rollBack();
      Engine_Api::_()->getApi('response','sesapi')->sendResponse(array('error'=>'1','error_message'=>$e->getMessage(),'result'=>array()));
    }
    
    
  }
}