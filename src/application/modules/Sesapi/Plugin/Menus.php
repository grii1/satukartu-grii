<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesapi
 * @package    Sesapi
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Menus.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

class Sesapi_Plugin_Menus
{
	public function enableIosModule(){
		// Must be logged in
    if(Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sesiosapp')){
      return true; 
    }
    return false;
		
	}
	public function enableAndroidModule(){
		// Must be logged in
    if(Engine_Api::_()->getDbtable('modules', 'core')->isModuleEnabled('sesandroidapp')){
      return true; 
    }
    return false;
		
	}
  
}