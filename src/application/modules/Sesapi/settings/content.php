<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesapi
 * @package    Sesapi
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: content.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

return array(
 /* array(
    'title' => 'SES - REST APIs',
    'description' => '',
    'category' => 'SES - REST APIs Tip message',
    'type' => 'widget',
    'name' => 'sesapi.tip',
    'autoEdit' => false,
  )*/
);