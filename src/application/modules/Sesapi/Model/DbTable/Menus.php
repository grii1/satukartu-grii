<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesapi
 * @package    Sesapi
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Menus.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
class Sesapi_Model_DbTable_Menus extends Engine_Db_Table {

  protected $_rowClass = 'Sesapi_Model_Menu';
	
  public function getMenus($param = array()) {
    $select = $this->select()
                   ->from($this->info('name'));
    if(!empty($param['status']))
      $select->where('status =?',$param['status']);
    $select->where('device =?',$param['device']);
    $select->order("order ASC");
    return $this->fetchAll($select);
  }
}