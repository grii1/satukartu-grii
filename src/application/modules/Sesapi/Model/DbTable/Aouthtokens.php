<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesapi
 * @package    Sesapi
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Aouthtokens.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

class Sesapi_Model_DbTable_Aouthtokens extends Engine_Db_Table {
	public function check($code,$params = array()){
    $select = $this->select()
								->from($this->info('name'),'*')
                ->where('platform =?',_SESAPI_PLATFORM_SERVICE)
                ->limit(1);
   // if(!empty($code))
      $select = $select->where('token =?',$code);
		return $this->fetchRow($select);	
	}
  
}