<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesapi
 * @package    Sesapi
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: ItemParent.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

class Sesapi_Model_Helper_ItemParent extends Sesapi_Model_Helper_Item
{
  public function direct($item, $type = null, $text = null, $href = null)
  {
    $item = $this->_getItem($item, false);

    // Check to make sure we have an item
    if( !($item instanceof Core_Model_Item_Abstract) )
    {
      return false;
    }
    
    $item = $item->getParent($type);
    return parent::direct($item, $text, $href);
  }
}