<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesapi
 * @package    Sesapi
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Translate.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
class Sesapi_Model_Helper_Translate extends Sesapi_Model_Helper_Abstract
{
  /**
   *
   * @param string $value
   * @return string
   */
  public function direct($value, $noTranslate = false)
  {
    $translate = Zend_Registry::get('Zend_Translate');
    if( !$noTranslate && $translate instanceof Zend_Translate ) {
      $tmp = $translate->translate($value);
      return $tmp;
    } else {
      return $value;
    }
  }
}