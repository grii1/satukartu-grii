<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesapi
 * @package    Sesapi
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Menus.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
class Sesapi_Api_Menus extends Core_Api_Abstract
{
  public function getMenus($params = array()) {
    return Engine_Api::_()
      ->getApi('menus', 'core')
      ->getNavigation($params['menu']);
  }
  
  public function getIconsMenu($menuName) {

    $coreMenuItemsTable = Engine_Api::_()->getDbTable('menuitems', 'core');
    $coreMenuItemsTableName = $coreMenuItemsTable->info('name');
    return $coreMenuItemsTable->select()
                    ->from($coreMenuItemsTableName, 'file_id')
                    ->where('name =?', $menuName)
                    ->query()
                    ->fetchColumn();
  }
}
