<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesapi
 * @package    Sesapi
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Core.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
class Sesapi_Api_Core extends Core_Api_Abstract {
  function checkCodeexists(){
    $path = APPLICATION_PATH.DIRECTORY_SEPARATOR.'index.php';
    if(file_exists($path)){
      $checkString = "restApi";
      $content = file_get_contents($path);
      if(strpos($content,$checkString) !== false)
        return true;
    }
    return false;  
  }
/**
   * @var Core_Model_Item_Abstract|mixed The object that represents the subject of the page
   */
  protected $_subject;
  function getMentionTags($content){
    $contentMention = $content;
    $mentions = array();
    preg_match_all('/(^|\s)(@\w+)/', $contentMention, $result);
    $counter = 0;
    
    foreach($result[2] as $value){
        $user_id = str_replace('@_user_','',$value);
        if(intval($user_id)>0) {
            $user = Engine_Api::_()->getItem('user', $user_id);
            if (!$user)
                continue;
        }else{
            $itemArray = explode('_',$user_id);
            $resource_id = $itemArray[count($itemArray) - 1];
            unset($itemArray[count($itemArray) - 1]);
            $resource_type = implode('_',$itemArray);
            try {
                $user = Engine_Api::_()->getItem($resource_type, $resource_id);
            }catch (Exception $e){
                continue;
            }
            if(!$user || !$user->getIdentity())
                continue;
        }
        $mentions[$counter]['word'] = $value;
        $mentions[$counter]['title'] = $user->getTitle();
        $mentions[$counter]['module'] = 'user';
        $mentions[$counter]['href'] = $this->getBaseUrl(false).$user->getHref();
        $mentions[$counter]['user_id'] = $user->getIdentity();
        $counter++;
    }    
    return $mentions;
  }
  function gethashtags($content)
  {
    $hashTagWords = array();
    preg_match_all("/#([\p{Pc}\p{N}\p{L}\p{Mn}]+)/u", $content, $matches);
    $searchword = $replaceWord = array();
    foreach($matches[0] as $value){
       $hashTagWords[]=$value;
    }
    return $hashTagWords;
  }
  function privacyOptions(){
    $arrayPrivacy = array('everyone'=>'Everyone','networks'=>"Friends & Networks",'friends'=>"Friends Only",'onlyme'=>"Only Me");
    $privacyArray = array();
    $conter = 0;
    foreach($arrayPrivacy as $key=>$pri){
      $privacyArray[$conter]['name'] = $key;
      $privacyArray[$conter]['value'] = $pri;
      $conter++;
    } 
    return $privacyArray;   
  }
  
  public function contentLike($subject)
  {
      $viewer = Engine_Api::_()->user()->getViewer();
      //return if non logged in user or content empty
      if (empty($subject) || empty($viewer))
          return;
      if ($viewer->getIdentity())
          $like = Engine_Api::_()->getDbTable("likes", "core")->isLike($subject, $viewer);
      return !empty($like) ? true : false;
  }
  public function getContentLikeCount($subject) {
      $getLikeCount = $subject->likes()->getLikePaginator()->getTotalItemCount();
      return (int) $getLikeCount;
  }
  public function getContentCommentCount($subject) {
      $getCommentCount = $subject->comments()->getCommentPaginator()->getTotalItemCount();
      return (int) $getCommentCount;
  }
  public function contentFollow($subject = null,$tableName = "",$modulename = "",$resource_type = "",$column_name = "user_id"){
    $viewer = Engine_Api::_()->user()->getViewer();
    //return if non logged in user or content empty
    if (empty($subject) || empty($viewer))
        return;
    if ($viewer->getIdentity())
    {
          $select =  Engine_Api::_()->getDbTable($tableName, $modulename)->select();
          $select->where('resource_id =?',$viewer->getIdentity())->where($column_name.' =?',$subject->getIdentity());
          if($resource_type)
            $select->where('resource_type =?',$resource_type);

          $follow = (int) Zend_Paginator::factory($select)->getTotalItemCount();
    }
    return !empty($follow) ? true : false;
  }
  
  public function getContentFollowCount($subject,$tableName = "",$modulename = "",$resources_type = "",$column_name = "user_id"){
      $viewer = Engine_Api::_()->user()->getViewer();
      if(!$tableName || !$modulename)
        return 0;
      $select =  Engine_Api::_()->getDbTable($tableName, $modulename)->select();
      $select->where($column_name.' =?',$subject->getIdentity());
      if($resources_type)
            $select->where('resource_type =?',$resources_type);
      return (int) Zend_Paginator::factory($select)->getTotalItemCount();
  }
  
  public function contentFavoutites($subject,$tableName = "",$modulename = "",$resources_type = "",$column_name = 'user_id'){
    $viewer = Engine_Api::_()->user()->getViewer();
    //return if non logged in user or content empty
    if (empty($subject) || empty($viewer))
        return;
    if ($viewer->getIdentity())
    {
      $select =  Engine_Api::_()->getDbTable($tableName, $modulename)->select();
      $select->where($column_name.' =?',$viewer->getIdentity())->where('resource_id =?',$subject->getIdentity());
      if($resources_type)
        $select->where('resource_type =?',$resources_type);
      $fav = (int) Zend_Paginator::factory($select)->getTotalItemCount();
    }
    return !empty($fav) ? true : false;
  }
  function parseCSSFile($file)
  {
    $css = file_get_contents($file);
    preg_match_all( '/(?ims)([a-z0-9\s\.\:#_\-@,]+)\{([^\}]*)\}/', $css, $arr);
    $result = array();
    foreach ($arr[0] as $i => $x){
        $selector = trim($arr[1][$i]);
        $rules = explode(';', trim($arr[2][$i]));
        $rules_arr = array();
        foreach ($rules as $strRule){
            if (!empty($strRule)){
                $rule = explode(":", $strRule);
                $rules_arr[trim($rule[0])] = trim($rule[1]);
            }
        }
        $selectors = explode(',', trim($selector));
        foreach ($selectors as $strSel){
            $result[trim($strSel)] = $rules_arr;
        }
    }
    return $result;
}
  public function getContentFavouriteCount($subject,$tableName = "",$modulename = "")
  {
      $viewer = Engine_Api::_()->user()->getViewer();
      if(!$tableName || !$modulename)
        return 0;
      $select =  Engine_Api::_()->getDbTable($tableName, $modulename)->select();
      $select->where('resource_id =?',$subject->getIdentity());
      return (int) Zend_Paginator::factory($select)->getTotalItemCount();
  }
  public function getBaseUrl($staticBaseUrl = true,$url = ""){

//      if(strpos($url,'http') !== false)
//          return $url;
//      $http = 'http://';
//      if(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off'){
//          $http = 'https://';
//      }
//      if(strpos($url,'http') !== false || strpos($url,'https') !== false){
//          return $url;
//      }
//      $baseUrl =  $_SERVER['HTTP_HOST'].'/';
//
//      //if($staticBaseUrl){
//      $baseUrl = $baseUrl;
//      //}
//      return $http.str_replace('//','/',$baseUrl.$url);

    if(strpos($url,'http') !== false)
      return $url;
    $http = 'http://';
    if(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off'){
       $http = 'https://';
    }
    $baseUrl =  $_SERVER['HTTP_HOST'];
    if(Zend_Registry::get('StaticBaseUrl') != "/")
    $url = str_replace(Zend_Registry::get('StaticBaseUrl'),'',$url);
    //if($staticBaseUrl){
      $baseUrl = $baseUrl.Zend_Registry::get('StaticBaseUrl') ;
    //}
    return $http.str_replace('//','/',$baseUrl.$url);
  }
    public function getCoverPhotoUrls($resource , $type,$resource_type,$getImageSize = false){

        try
        {
            if(is_int($resource)  || intval($resource) > 0)
            {
                $resource_id = $resource;

                if(!$resource_id)
                    return  array();
            }
            else if(isset($resource->cover) && $resource->getType() != "album_photo")
                $resource_id = $resource->cover;
            else if(isset($resource->thumbnail_id))
                $resource_id  = $resource->thumbnail_id;
            else
                $resource_id = $resource->file_id;

            if($resource_id)
            {
                $table = Engine_Api::_()->getItemTable('storage_file');
                $select = $table->select()->from($table)->where('file_id =?',$resource_id);
                if($type)
                    $select->where('type =?',$type);
                if($resource_type)
                    $select->where('parent_type =?',$resource_type);

                $result = $table->fetchRow($select);
                $photos = array();
                if($result){
                    $counter = 0;
                    $photos["main"] = $this->getBaseUrl(true,$result->map());
                    if($getImageSize)
                        list($photos["main_width_height"]['width'],$photos["main_width_height"]['height']) = @getimagesize($photos['main']);
                    $select = $table->select()->from($table)->where('parent_file_id =?',$result->getIdentity());
                    $result = $table->fetchAll($select);
                    foreach($result as $photo){
                        $type = str_replace('thumb.','',$photo->type);
                        $photos[$type] = $this->getBaseUrl(true,$photo->map());
                        if($getImageSize)
                            list($photos[$type."_width_height"]['width'],$photos[$type."_width_height"]['height']) = @getimagesize($photos[$type]);
                        $counter++;
                    }
                }else if(!is_int($resource)){
                    $photos["main"] =  $this->getBaseUrl(false,$resource->getPhotoURL());
                    if($getImageSize)
                        list($photos["main_width_height"]['width'],$photos["main_width_height"]['height']) = @getimagesize($photos['main']);
                }
                return $photos;
            }else{
                if($resource->getCoverPhotoUrls()){
                    $photos["main"] =  $this->getBaseUrl(false,$resource->getCoverPhotoUrls());
                    //list($photos["main_width_height"]['width'],$photos["main_width_height"]['height']) = @getimagesize($photos['main']);
                    return $photos;
                }
            }
        }catch(Exception $e){
            return array();
        }
        return array();
    }
  public function getPhotoUrls($resource , $type,$resource_type,$getImageSize = false)
  {
    try
    {
     if(is_int($resource))
     {
       $resource_id = $resource;

        if(!$resource_id)
          return  array();
     }
      else if(isset($resource->photo_id) && $resource->getType() != "album_photo")
       $resource_id = $resource->photo_id;
     else if(isset($resource->thumbnail_id))
      $resource_id  = $resource->thumbnail_id;
     else
      $resource_id = $resource->file_id;
    
     if($resource_id)
     {
        $table = Engine_Api::_()->getItemTable('storage_file');
        $select = $table->select()->from($table)->where('file_id =?',$resource_id);
        if($type)
          $select->where('type =?',$type);
        if($resource_type)
          $select->where('parent_type =?',$resource_type);
          
        $result = $table->fetchRow($select);
        $photos = array();
        if($result){
          $counter = 0;
          $photos["main"] = $this->getBaseUrl(true,$result->map());
          if($getImageSize)
            list($photos["main_width_height"]['width'],$photos["main_width_height"]['height']) = @getimagesize($photos['main']);
          $select = $table->select()->from($table)->where('parent_file_id =?',$result->getIdentity());
          $result = $table->fetchAll($select);
          foreach($result as $photo){
            $type = str_replace('thumb.','',$photo->type);
            $photos[$type] = $this->getBaseUrl(true,$photo->map());
            if($getImageSize)
            list($photos[$type."_width_height"]['width'],$photos[$type."_width_height"]['height']) = @getimagesize($photos[$type]);
            $counter++;
          }
        }else if(!is_int($resource)){
          $photos["main"] =  $this->getBaseUrl(false,$resource->getPhotoURL());
          if($getImageSize)
            list($photos["main_width_height"]['width'],$photos["main_width_height"]['height']) = @getimagesize($photos['main']);
        }
        return $photos;
     }else{
       if($resource->getPhotoURL()){
        $photos["main"] =  $this->getBaseUrl(false,$resource->getPhotoURL());
        //list($photos["main_width_height"]['width'],$photos["main_width_height"]['height']) = @getimagesize($photos['main']);
        return $photos;
       }
     }
    }catch(Exception $e){
      return array();  
    }
     return array();
  }
  public function getCoordinates($address){
		$address = str_replace(" ", "+", $address); // replace all the white space with "+" sign to match with google search pattern	 
		$url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
		$response = file_get_contents($url);
		$json = json_decode($response,TRUE); //generate array object from the response from the web
		if(!empty($json['results'][0]['geometry']['location']['lat']))
			return array('lat'=>$json['results'][0]['geometry']['location']['lat'],'lng'=>$json['results'][0]['geometry']['location']['lng']);
		else 
			return '';
	}
  public function friendship($subject){
    $view = Zend_Registry::isRegistered('Zend_View') ? Zend_Registry::get('Zend_View') : null;
    $viewer = Engine_Api::_()->user()->getViewer();
    $subject = Engine_Api::_()->core()->getSubject();
    
    // Not logged in
    if( !$viewer->getIdentity() || $viewer->getGuid(false) === $subject->getGuid(false) ) {
      return false;
    }

    // No blocked
    if( $viewer->isBlockedBy($subject) ) {
      return false;
    }

    // Check if friendship is allowed in the network
    $eligible = (int) Engine_Api::_()->getApi('settings', 'core')->getSetting('user.friends.eligible', 2);
    if( !$eligible ) {
      return '';
    }

    // check admin level setting if you can befriend people in your network
    else if( $eligible == 1 ) {

      $networkMembershipTable = Engine_Api::_()->getDbtable('membership', 'network');
      $networkMembershipName = $networkMembershipTable->info('name');

      $select = new Zend_Db_Select($networkMembershipTable->getAdapter());
      $select
        ->from($networkMembershipName, 'user_id')
        ->join($networkMembershipName, "`{$networkMembershipName}`.`resource_id`=`{$networkMembershipName}_2`.resource_id", null)
        ->where("`{$networkMembershipName}`.user_id = ?", $viewer->getIdentity())
        ->where("`{$networkMembershipName}_2`.user_id = ?", $subject->getIdentity())
      ;

      $data = $select->query()->fetch();

      if( empty($data) ) {
        return '';
      }
    }

    // One-way mode
    $direction = (int) Engine_Api::_()->getApi('settings', 'core')->getSetting('user.friends.direction', 1);
    if( !$direction )
    {
      $viewerRow = $viewer->membership()->getRow($subject);
      $subjectRow = $subject->membership()->getRow($viewer);
      $params = array();

      // Viewer?
      if( null === $subjectRow ) {
        // Follow
        return array(
          'label' => $view->translate('Follow'),
          'name'=>'add',
          'params' => array(
              'user_id' => $subject->getIdentity()
            ),
        );
      } else if( $subjectRow->resource_approved == 0 ) {
        // Cancel follow request
       return array(
          'label' => $view->translate('Cancel Follow'),
          'name'=>'cancel',
          'params' => array(
              'user_id' => $subject->getIdentity()
            ),
        );
      } else {
        // Unfollow
        return array(
          'label' => $view->translate('Unfollow'),
          'name' => 'remove',
          'params' => array(
              'user_id' => $subject->getIdentity()
            ),
        );
      }
      // Subject?
      if( null === $viewerRow ) {
        // Do nothing
      } else if( $viewerRow->resource_approved == 0 ) {
        // Approve follow request
        return array(
          'label' => $view->translate('Approve Follow'),
          'name' => 'confirm',
          'params' => array(
              'user_id' => $subject->getIdentity()
            ),
        );
      } else {
        // Remove as follower?
        return array(
          'label' => $view->translate('Unfollow'),
          'name' => 'remove',
          'params' => array(
              'user_id' => $subject->getIdentity()
            ),
        );
      }
      if( count($params) == 1 ) {
        return $params[0];
      } else if( count($params) == 0 ) {
        return false;
      } else {
        return $params;
      }
    }

    // Two-way mode
    else {
      $row = $viewer->membership()->getRow($subject);
      if( null === $row ) {
        // Add
        return array(
          'label' => $view->translate('Add Friend'),
          'name' => 'add',
          'params' => array(
              'user_id' => $subject->getIdentity()
            ),
        );
      } else if( $row->user_approved == 0 ) {
        // Cancel request
        return array(
          'label' => $view->translate('Cancel Friend'),
          'name' => 'cancel',
          'params' => array(
              'user_id' => $subject->getIdentity()
            ),
        );
      } else if( $row->resource_approved == 0 ) {
        // Approve request
        return array(
          'label' => $view->translate('Approve Friend'),
          'name' => 'confirm',
          'params' => array(
              'user_id' => $subject->getIdentity()
            ),
        );
      } else {
        // Remove friend
        return array(
          'label' => $view->translate('Remove Friend'),
          'name' => 'remove',
          'params' => array(
              'user_id' => $subject->getIdentity()
            ),
        );
      }
    }
  
  }
  public function getIdentityWidget($name, $type, $corePages) {
    $widgetTable = Engine_Api::_()->getDbTable('content', 'core');
    $widgetPages = Engine_Api::_()->getDbTable('pages', 'core')->info('name');
    $identity = $widgetTable->select()
            ->setIntegrityCheck(false)
            ->from($widgetTable, '*')
            ->where($widgetTable->info('name') . '.type = ?', $type)
            ->where($widgetTable->info('name') . '.name = ?', $name)
            ->where($widgetPages . '.name = ?', $corePages)
            ->joinLeft($widgetPages, $widgetPages . '.page_id = ' . $widgetTable->info('name') . '.page_id',null);
       return     $widgetTable->fetchRow($identity);
  }
  /**
   * Set the object that represents the subject of the page
   *
   * @param Core_Model_Item_Abstract|mixed $subject
   * @return Core_Api_Core
   */
  public function setSubject($subject)
  {
    if( null !== $this->_subject ) {
      throw new Core_Model_Exception("The subject may not be set twice");
    }

    if( !($subject instanceof Core_Model_Item_Abstract) ) {
      throw new Core_Model_Exception("The subject must be an instance of Core_Model_Item_Abstract");
    }
    
    $this->_subject = $subject;
    return $this;
  }

  /**
   * Get the previously set subject of the page
   *
   * @return Core_Model_Item_Abstract|null
   */
  public function getSubject($type = null)
  {
    if( null === $this->_subject ) {
      throw new Core_Model_Exception("getSubject was called without first setting a subject.  Use hasSubject to check");
    } else if( is_string($type) && $type !== $this->_subject->getType() ) {
      throw new Core_Model_Exception("getSubject was given a type other than the set subject");
    } else if( is_array($type) && !in_array($this->_subject->getType(), $type) ) {
      throw new Core_Model_Exception("getSubject was given a type other than the set subject");
    }
    
    return $this->_subject;
  }

  /**
   * Checks if a subject has been set
   *
   * @return bool
   */
  public function hasSubject($type = null)
  {
    if( null === $this->_subject ) {
      return false;
    } else if( null === $type ) {
      return true;
    } else {
      return ( $type === $this->_subject->getType() );
    }
  }

  public function clearSubject()
  {
    $this->_subject = null;
    return $this;
  }
  
  // Get Count of item based on category
  public function getCategoryBasedItems($params = array()) {

    $table = Engine_Api::_()->getDbTable($params['table_name'], $params['module_name']);
    $tableName = $table->info('name');
    return $table->select()
                ->from($tableName, array('count(*) as albumCount'))
                ->where('category_id =?', $params['category_id'])
                ->query()
                ->fetchColumn();
      
  }
}