<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesapi
 * @package    Sesapi
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Pushnoti.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
// Server file
class Sesapi_Api_Pushnoti extends Core_Api_Abstract
{
  // Sends Push notification for Android users
	public function android($data, $reg_id,$userInfo = array()) {
      $tokens = array();
      foreach($reg_id as $token){
          $tokens[] = $token->device_uuid;
      }
      $url = 'https://fcm.googleapis.com/fcm/send';
      if(!empty($data['description']))
        $description = $data['description'];
      else
        $description = " ";
      $message = array(
          'title' => $data['title'],
          'message' => $description,
          'msgcnt' => 1,
          'vibrate' => 1,
          'sound'=>'default',
      );
      if(count($userInfo)>0){
          $message = array_merge($message,array('userInfo'=>$userInfo));
      }else{
          $message = array_merge($message,array('userInfo'=>"{}"));
      }
      $settings = Engine_Api::_()->getApi('settings', 'core');
      $API_ACCESS_KEY =  $settings->getSetting('sesandroidapp_server_key', 0);
      if(!$API_ACCESS_KEY){
        return false;
      }
      $headers = array(
        'Authorization: key=' .$API_ACCESS_KEY,
        'Content-Type: application/json'
      );
      
      foreach(array_chunk($tokens,50) as $array){
        $fields = array(
            'registration_ids' => $array,
            'data' => $message,
        );
        $this->useCurl($url, $headers, json_encode($fields));
      }
      return true;
 	}
  // Sends Push notification for iOS users
	public function iOS($data, $deviceToken,$userInfo = array()) {  
    $settings = Engine_Api::_()->getApi('settings', 'core');
    $API_ACCESS_KEY =  $settings->getSetting('sesiosapp_server_key', 0);
    $PASSWORD =  $settings->getSetting('sesiosapp_server_password', '');
    $MODE =  $settings->getSetting('sesiosapp_apn_mode', 1);
    if(!$API_ACCESS_KEY){
        return false;
    }
		$ctx = stream_context_create();
    $file = "";
    if(file_exists(APPLICATION_PATH . DIRECTORY_SEPARATOR.$API_ACCESS_KEY)){
      $file = APPLICATION_PATH . DIRECTORY_SEPARATOR.$API_ACCESS_KEY;  
    }else if(file_exists($API_ACCESS_KEY)){
      $file = $API_ACCESS_KEY;  
    }else{
      return false;  
    }
		stream_context_set_option($ctx, 'ssl', 'local_cert', $file);
		stream_context_set_option($ctx, 'ssl', 'passphrase', $PASSWORD);		
		// Create the payload body
    if(!empty($data['description']))
      $description = $data['description'];
    else
      $description = " ";
		$body['aps'] = array(
			'alert' => array(
			    'title' => $data['title'],
          'body' => $description,
          'userinfo'=>$userInfo,
			 ),
			'sound' => 'default'
		);
    if (version_compare(PHP_VERSION, '5.4.0') >= 0) {
        $payload = json_encode($body, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    } else {
        $payload = json_encode($body);
    }  
    if($MODE){
      $url = 'ssl://gateway.push.apple.com:2195';  
    }else{
      $url = "ssl://gateway.sandbox.push.apple.com:2195";  
    }
		// Encode the payload as JSON
    foreach($deviceToken as $token){
       if($token->device_id != 1)
        continue;
      $fp = stream_socket_client(
        $url, $err,
        $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
      $expire = time() + 3600;
      // Build the binary notification
      $msg = pack('CNNnH*', 1, time(), $expire, 32, str_replace(' ','',$token->device_uuid))
            . pack('n', strlen($payload))
            . $payload;
      // Send it to the server
      $result = fwrite($fp, $msg);
      // Close the connection to the server
      fclose($fp);
    }	
		if (!$result){
			return false;
    }
		else
			return true;
	}
	// Curl 
	private function useCurl($url, $headers, $fields = null) {
    // Open connection
    $ch = curl_init();
    if ($url) {
      // Set the url, number of POST vars, POST data
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      // Disabling SSL Certificate support temporarly
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      if ($fields)
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
      // Execute post
      $result = curl_exec($ch);
      if ($result === FALSE) {
        //die('Curl failed: ' . curl_error($ch));
      }
      // Close connection
      curl_close($ch);
      return $result;
    }
  }
}
?>