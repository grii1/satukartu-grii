<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesapi
 * @package    Sesapi
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: index.tpl  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

?>
<?php include APPLICATION_PATH .  '/application/modules/Sesapi/views/scripts/dismiss_message.tpl';?>
<h2>
  <?php echo $this->translate("SocialEngine REST APIs Plugin") ?>
</h2>
<div class="sesbasic_nav_btns">
  <a href="<?php echo $this->url(array('module' => 'sesapi', 'controller' => 'settings', 'action' => 'help'),'admin_default',true); ?>" target = "_blank" class="request-btn">Help Center</a>
</div>
<?php if( count($this->navigation) ): ?>
  <div class='sesbasic-admin-navgation'>
    <?php echo $this->navigation()->menu()->setContainer($this->navigation)->render() ?>
  </div>
<?php endif; ?>
<div class="settings sesbasic_admin_form">
  <div class='settings'>
    <?php echo $this->form->render($this); ?>
  </div>
</div>
<div class="sesbasic_waiting_msg_box" style="display:none;">
	<div class="sesbasic_waiting_msg_box_cont">
    <?php echo $this->translate("Please wait.. It might take some time to activate plugin."); ?>
    <i></i>
  </div>
</div>
<?php if(!Engine_Api::_()->getApi('settings', 'core')->getSetting('sesapi.pluginactivated',0)){ 
 $this->headScript()->appendFile($this->layout()->staticBaseUrl . 'application/modules/Sesbasic/externals/scripts/sesJquery.js');?>
	<script type="application/javascript">
  	sesJqueryObject('.global_form').submit(function(e){
			sesJqueryObject('.sesbasic_waiting_msg_box').show();
		});
  </script>
<?php } ?>

<script type="application/javascript">
function hideShow(showHide){console.log(showHide);
  if(document.getElementById('sesapi_tip_title-wrapper'))
    document.getElementById('sesapi_tip_title-wrapper').style.display = showHide;
  if(document.getElementById('sesapi_tip_description-wrapper'))
    document.getElementById('sesapi_tip_description-wrapper').style.display = showHide;
  if(document.getElementById('sesapi_tip_iosid-wrapper'))
    document.getElementById('sesapi_tip_iosid-wrapper').style.display = showHide;
  if(document.getElementById('sesapi_tip_buttoninstall-wrapper'))
    document.getElementById('sesapi_tip_buttoninstall-wrapper').style.display = showHide;
  if(document.getElementById('sesapi_tip_daysHidden-wrapper'))
    document.getElementById('sesapi_tip_daysHidden-wrapper').style.display = showHide;
  if(document.getElementById('sesapi_tip_daysReminder-wrapper'))
    document.getElementById('sesapi_tip_daysReminder-wrapper').style.display = showHide;
  if(document.getElementById('sesapi_tip_image-wrapper'))
    document.getElementById('sesapi_tip_image-wrapper').style.display = showHide;
  if(document.getElementById('sesapi_tip_androidid-wrapper'))
    document.getElementById('sesapi_tip_androidid-wrapper').style.display = showHide;
}
<?php 
  $settings = Engine_Api::_()->getApi('settings', 'core');
  if($settings->getSetting('sesapi.tip.enable', 1)){ ?>
  hideShow('block');
<?php }else{ ?>
  hideShow('none');
<?php } ?>
function tipMessage(value){
  if(value == 0){
      hideShow('none');
  }else
      hideShow('block');
}
function openURL(name){
  if(name == "manual"){
    Smoothbox.open('admin/sesapi/settings/manual');
    parent.Smoothbox.close;
    return false;
  }else{
    Smoothbox.open('admin/sesapi/settings/automatic');
    parent.Smoothbox.close;
    return false;
  }  
}
</script>
