<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesapi
 * @package    Sesapi
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Item.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

class Sesapi_View_Helper_Item extends Zend_View_Helper_Abstract
{
  public function item($type, $identity)
  {
    return Engine_Api::_()->getItem($type, $identity);
  }
}