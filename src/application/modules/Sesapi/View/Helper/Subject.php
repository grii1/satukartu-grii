<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesapi
 * @package    Sesapi
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Subject.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
class Sesapi_View_Helper_Subject extends Zend_View_Helper_Abstract
{
  public function subject()
  {
    if( Engine_Api::_()->sesapi()->hasSubject() )
    {
      return Engine_Api::_()->sesapi()->getSubject();
    }
  }
}