<?php
//folder name or directory name. 
$module_name = 'sesgrouppoll'; 
//product title and module title. 
$module_title = 'Group Polls Extension'; 

if (!$this->getRequest()->isPost()) { return; } 
if (!$form->isValid($this->getRequest()->getPost())) { return; } 

if ($this->getRequest()->isPost()) { 
    // $postdata = array(); 
    // //domain name 
    // $postdata['domain_name'] = $_SERVER['HTTP_HOST']; 
    // //license key 
    // $postdata['licenseKey'] = @base64_encode($_POST['sesgrouppoll_licensekey']); 
    // $postdata['module_title'] = @base64_encode($module_title); 
    // $ch = curl_init(); curl_setopt($ch, CURLOPT_URL, "https://socialnetworking.solutions/licensecheck.php"); 
    // curl_setopt($ch, CURLOPT_POST, 1); 
    // // in real life you should use something like: curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postdata)); 
    // // receive server response ... 
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
    // $server_output = curl_exec($ch); $error = 0; 
    // if (curl_error($ch)) { $error = 1; } curl_close($ch); 
    
    $server_output = "OK";
    $error = 0;
    //here we can set some variable for checking in plugin files. 
    if ($server_output == "OK" && $error != 1) { 
        if (!Engine_Api::_()->getApi('settings', 'core')->getSetting('sesgrouppoll.pluginactivated')) { 
            $db = Zend_Db_Table_Abstract::getDefaultAdapter(); 
            
            $db->query('DROP TABLE IF EXISTS `engine4_sesgrouppoll_polls`;'); 
            $db->query('CREATE TABLE IF NOT EXISTS `engine4_sesgrouppoll_polls` ( `poll_id` int(11) unsigned NOT NULL auto_increment, `user_id` int(11) unsigned NOT NULL, `group_id` int(11) unsigned NOT NULL, `is_closed` tinyint(1) NOT NULL default "0", `title` varchar(255) NOT NULL, `description` text NOT NULL, `creation_date` datetime NOT NULL, `view_count` int(11) unsigned NOT NULL default "0", `comment_count` int(11) unsigned NOT NULL default "0", `like_count` int(11) unsigned NOT NULL default "0", `vote_count` int(11) unsigned NOT NULL default "0", `favourite_count` int(11) unsigned NOT NULL default "0", `search` tinyint(1) NOT NULL default "1", `closed` tinyint(1) NOT NULL default "0", `view_privacy` VARCHAR(24) NOT NULL, PRIMARY KEY (`poll_id`), KEY `user_id` (`user_id`), KEY `is_closed` (`is_closed`), KEY `creation_date` (`creation_date`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci ;'); 
            $db->query('CREATE TABLE IF NOT EXISTS `engine4_sesgrouppoll_options` ( `poll_option_id` int(11) unsigned NOT NULL auto_increment, `poll_id` int(11) unsigned NOT NULL, `poll_option` text NOT NULL, `votes` smallint(4) unsigned NOT NULL, `file_id` int(11) NOT NULL DEFAULT "0", `image_type` tinyint(1) NOT NULL DEFAULT "0", PRIMARY KEY (`poll_option_id`), KEY `poll_id` (`poll_id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci ;'); 
            
            $db->query('DROP TABLE IF EXISTS `engine4_sesgrouppoll_votes`;'); 
            $db->query('CREATE TABLE IF NOT EXISTS `engine4_sesgrouppoll_votes` ( `poll_id` int(11) unsigned NOT NULL, `user_id` int(11) unsigned NOT NULL, `poll_option_id` int(11) unsigned NOT NULL, `creation_date` datetime NOT NULL, `modified_date` datetime NOT NULL, PRIMARY KEY (`poll_id`,`user_id`), KEY `poll_option_id` (`poll_option_id`), KEY `user_id` (`user_id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci ;'); 
            
            $db->query('DROP TABLE IF EXISTS `engine4_sesgrouppoll_favourites`;'); 
            $db->query('CREATE TABLE IF NOT EXISTS `engine4_sesgrouppoll_favourites` ( `favourite_id` int(11) UNSIGNED NOT NULL auto_increment, `user_id` int(11) UNSIGNED NOT NULL, `resource_type` varchar(128) COLLATE utf8_unicode_ci NOT NULL, `resource_id` int(11) NOT NULL, KEY `favourite_id` (`favourite_id`), PRIMARY KEY (`favourite_id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;'); 
            $db->query('INSERT IGNORE INTO `engine4_core_jobtypes` (`title`, `type`, `module`, `plugin`, `priority`) VALUES ("Rebuild Group Poll Privacy", "sesgrouppoll_maintenance_rebuild_privacy", "sesgrouppoll", "sesgrouppoll_Plugin_Job_Maintenance_RebuildPrivacy", 50);'); 
            $db->query('INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`,`enabled`, `submenu`, `order`) VALUES ("sesgroup_main_grouppollhome", "sesgrouppoll", "Polls", "", \'{"route":"sesgrouppoll_general","action":"home"}\',"sesgroup_main","1", "", 13), ("sesgroup_main_grouppollbrowse", "sesgrouppoll", "Browse Polls", "", \'{"route":"sesgrouppoll_general","action":"browse"}\',"sesgroup_main","0","", 14), ("sesgrouppoll_admin_main_manage", "sesgrouppoll", "Manage Polls", "", \'{"route":"admin_default","module":"sesgrouppoll","controller":"manage"}\', "sesgrouppoll_admin_main","1", "", 2), ("sesgrouppoll_admin_main_level", "sesgrouppoll", "Member Level Settings", "", \'{"route":"admin_default","module":"sesgrouppoll","controller":"settings","action":"level"}\', "sesgrouppoll_admin_main","1", "", 3);'); $table_exist = $db->query("SHOW TABLES LIKE 'engine4_sesgroup_managegroupapps'")->fetch(); if (!empty($table_exist)) { $sesgrouppoll = $db->query("SHOW COLUMNS FROM engine4_sesgroup_managegroupapps LIKE 'sesgrouppoll'")->fetch(); if (empty($sesgrouppoll)) { $db->query('ALTER TABLE `engine4_sesgroup_managegroupapps` ADD COLUMN `sesgrouppoll` TINYINT(1) NOT NULL DEFAULT 1;'); } } $db->query('INSERT IGNORE INTO `engine4_activity_actiontypes` (`type`, `module`, `body`, `enabled`, `displayable`, `attachable`, `commentable`, `shareable`, `is_generated`) VALUES ("sesgroup_group_createpoll", "sesgrouppoll", \'{item:$subject} created a new poll\', "1", "7", "1", "3", "1", 1), ("favourite_sesgrouppoll_poll", "sesgrouppoll", \'{item:$subject} marked a Poll as fovourite in {item:$object}.\',"1","7","1","3","3",1), ("vote_sesgrouppoll_poll", "sesgrouppoll", \'{item:$subject} has voted a Poll in {item:$object}.\',"1", "7","1","3","3",1), ("like_sesgrouppoll_poll", "sesgrouppoll", \'{item:$subject} has liked a Poll in {item:$object}.\',"1", "7","1","3","3",1);'); 
            $db->query('INSERT IGNORE INTO `engine4_activity_notificationtypes` (`type`, `module`, `body`, `is_request`, `handler`) VALUES ("sesgrouppoll_favourite_poll", "sesgrouppoll", \'{item:$subject} has marked your Poll in {item:$object} as favourite.\', 0, ""), ("sesgrouppoll_like_poll", "sesgrouppoll", \'{item:$subject} liked your Poll in {item:$object}.\', 0, ""), ("sesgrouppoll_vote_poll", "sesgrouppoll", \'{item:$subject} voted your Poll in {item:$object}.\', 0, "");'); 
            
            include_once APPLICATION_PATH . "/application/modules/Sesgrouppoll/controllers/defaultsettings.php"; 
            Engine_Api::_()->getApi('settings', 'core')->setSetting('sesgrouppoll.pluginactivated', 1); 
            Engine_Api::_()->getApi('settings', 'core')->setSetting('sesgrouppoll.licensekey', $_POST['sesgrouppoll_licensekey']); 
        } 
        $domain_name = @base64_encode(str_replace(array('http://','https://','www.'),array('','',''),$_SERVER['HTTP_HOST'])); $licensekey = Engine_Api::_()->getApi('settings', 'core')->getSetting('sesgrouppoll.licensekey'); 
        $licensekey = @base64_encode($licensekey);
             
        
        Engine_Api::_()->getApi('settings', 'core')->setSetting('sesgrouppoll.sesdomainauth', $domain_name); 
        Engine_Api::_()->getApi('settings', 'core')->setSetting('sesgrouppoll.seslkeyauth', $licensekey); $error = 1; 
    } 
    else { 
        $error = $this->view->translate('Please enter correct License key for this product.'); 
        $error = Zend_Registry::get('Zend_Translate')->_($error); 
        $form->getDecorator('errors')->setOption('escape', false); 
        $form->addError($error); $error = 0; 
                
        Engine_Api::_()->getApi('settings', 'core')->setSetting('sesgrouppoll.licensekey', $_POST['sesgrouppoll_licensekey']); return; 
        $this->_helper->redirector->gotoRoute(array()); 
    } 
}