<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesgrouppoll
 * @package    Sesgrouppoll
 * @copyright  Copyright 2017-2018 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: index.tpl  2018-04-23 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
 
?>
<div class="quicklinks">
  <a href="<?php echo $this->url(array('action' => 'browse'), 'sesgrouppoll_general', true); ?>"><?php echo $this->translate("Browse Polls"); ?></a>
</div>
