<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesgrouppoll
 * @package    Sesgrouppoll
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: gifcontent.tpl  2018-11-02 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
 
 ?>
<div class="gif_grouppoll_content ses_emoji_container sesbasic_bxs _gif_grouppoll_content notclose">
  <div class="ses_emoji_container_arrow"></div>
  <div class="ses_gif_container_inner sesbasic_clearfix">
    <div class="ses_gif_holder">
      <div class="sesbasic_loading_container empty_cnt" style="height:100%;"></div>
    </div>
  </div>
</div>