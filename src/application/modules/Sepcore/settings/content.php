<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Netlogtemplate
 * @copyright  Copyright 2010-2011 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     Vadim
 */

return array(
	array(
		'title' => 'SEP Main Menu (place in header)',
		'description' => 'Show main menu with "more" button. REPLACE standart "Main Menu" widget.',
		'category' => 'SocialEnginePro Core',
		'type' => 'widget',
		'name' => 'sepcore.main-menu',
	),
	array(
		'title' => 'SEP Languages Block (place in footer)',
		'description' => 'This widget displays available languages on your site. You can use this only in the footer.',
		'category' => 'SocialEnginePro Core',
		'type' => 'widget',
		'name' => 'sepcore.languages',
	),
	array(
		'title' => 'SEP Mobile promo (any place)',
		'description' => 'Widget with link to mobile site.',
		'category' => 'SocialEnginePro Core',
		'type' => 'widget',
		'name' => 'sepcore.mobile-version',
	),
	array(
		'title' => 'SEP AddThis (any place)',
		'description' => 'Widget with AddThis.com button',
		'category' => 'SocialEnginePro Core',
		'type' => 'widget',
		'name' => 'sepcore.addthis',
	),
);