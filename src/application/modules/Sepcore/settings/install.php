<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sepcore
 * @copyright  Copyright 2010-2012 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     altrego aka vadim ( provadim@gmail.com )
 */

if ( !class_exists('Sepcore_Installer') ) {
	require_once(APPLICATION_PATH . '/application/modules/Sepcore/settings/installer.php');
}

class Sepcore_Install extends Sepcore_Installer {

}