<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sepcore
 * @copyright  Copyright 2010-2012 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     altrego aka Vadim ( provadim@gmail.com )
 */

abstract class Sepcore_Installer extends Engine_Package_Installer_Module {

    protected $db;
    protected $translate;
    protected $content_ids;
    protected $operation;
    protected $module_name;
    protected $account_info;
    protected $manifest;

    final public function onPreInstall() {

        parent::onPreInstall();
        $this->_init();

	if ( !class_exists('Sepcore_Form_ClientAccount') ) {
		require_once(APPLICATION_PATH . '/application/modules/Sepcore/Form/ClientAccount.php');
	}

        $this->_insertModule();
        $this->_query();

        if ( isset($this->manifest['menu']) ) {
            $this->_insertMenu($this->manifest['menu']);
        }

        if ( isset($this->manifest['widgets']) ) {
            $this->_insertWidgets($this->manifest['widgets']);
        }

        $this->db->commit();

    }


    // sending request to our server
    final public function request($type, $params=array()) {
	$data = array();
	$data['domain'] = $_SERVER['HTTP_HOST'];
	$data['account'] = $this->account_info['hash'];

	switch ( $type ) {
		case 'check_account':
			$url = 'u';
			$data['account'] = md5( md5($params['client_email']) . md5($params['client_password']) );
		break;
		case 'check_license':
			$url = 'l';
			$data['product'] = $this->module_name;
			$data['checksum'] = $this->manifest['sha1'];
		break;
		case 'check_plugins':
			$url = 'p';
			$data['account'] = $params['account'];
		break;
	}

	$data_string = '';
	foreach ($data as $key=>$value) {
		$data_string .= $key.'='.urlencode($value).'&';
	}
	$data_string = substr($data_string,0,-1);
	$query = 'https://socialenginepro.com/license/' . $url . '.php?' . $data_string;

	$curl = curl_init($query);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($curl);
	$result = json_decode($result);
	curl_close($curl);

	if ( empty($result) ) {
		$result = json_decode(json_encode(array('result'=>0, 'message'=>'Can`t connect to socialenginepro.com server. Try again later.')));
	}
	return $result;
    }


    // initialize all needed info
    final public function _init() {
        $this->db = $this->getDb();
        $this->db->beginTransaction();
        $this->translate = Zend_Registry::get('Zend_Translate');
        $this->operation = $this->_databaseOperationType;
        $this->module_name = $this->getOperation()->getTargetPackage()->getName();

        $this->account_info = array();
        $select = $this->db->select()
            ->from('engine4_core_settings')
            ->where('name IN("sepcore.client.email", "sepcore.client.password")');
        $account_info = $this->db->fetchAll($select);

	if ($account_info) {
		foreach ($account_info as $value) {
			$this->account_info[$value['name']] = $value['value'];
		}
		$this->account_info['hash'] = md5( md5($this->account_info['sepcore.client.email']) . $this->account_info['sepcore.client.password'] );
	}

        $package = $this->getOperation()->getPrimaryPackage();
        $path = $package->getPath();
        if( $this instanceof Engine_Package_Installer_Module ) {
            $manifestPath = APPLICATION_PATH . DIRECTORY_SEPARATOR . $path . DIRECTORY_SEPARATOR . 'settings' . DIRECTORY_SEPARATOR . 'manifest.php';
        } else {
            $manifestPath = APPLICATION_PATH . DIRECTORY_SEPARATOR . $path . DIRECTORY_SEPARATOR . 'manifest.php';
        }
        ob_start();
        $this->manifest = include $manifestPath;
        $this->manifest['sha1'] = sha1_file($manifestPath);
        ob_end_clean();
    }


    // insert menu items
    public function _insertMenu($menu_array) {

        foreach($menu_array as $menu_name=>$menu) {
            $counter = 1;

            if ( !empty($menu_name) && !empty($menu['title']) ) {
                $this->db->delete('engine4_core_menus', 'name="'.$menu_name.'"');
                $this->db->insert('engine4_core_menus', array('name'=>$menu_name, 'type'=>'standart', 'title'=>$menu['title']));
            }

            foreach( $menu['menuitems'] as $menuitem ) {
                if ( empty($menuitem['name']) )
                    continue;
                $menuitem['module'] = $this->module_name;
                $menuitem['menu'] = $menu_name;
                $menuitem['enabled'] = 1;
                $menuitem['custom'] = 0;
                $menuitem['order'] = $counter;
                $this->db->delete('engine4_core_menuitems', 'name="'.$menuitem['name'].'"');
                $this->db->insert('engine4_core_menuitems', $menuitem);
                $counter++;
            }
        }
    }


    // insert widgets
    public function _insertWidgets($widgets_array) {

        foreach($widgets_array as $widget) {

            $page_id = $this->_getPageId($widget['page']);
            $parrent_content_id = $this->_getContainerId($widget['parent_content_name'], $page_id);

            if ( !$page_id || !$parrent_content_id || !$widget['name'] )
                continue;

            $this->db->delete('engine4_core_content', 'name="'.$widget['name'].'" AND page_id="'.$page_id.'"');
            $this->db->insert('engine4_core_content', array(
                'page_id' => $page_id,
                'type' => 'widget',
                'name' => $widget['name'],
                'parent_content_id' => $parrent_content_id,
                'order' => $widget['order'],
                'params' => $widget['params'],
            ));
        }
    }


    // insert menu items
    public function _insertModule() {
        $this->db->delete('engine4_core_modules', 'name="'.$this->manifest['package']['name'].'"');
        $this->db->insert('engine4_core_modules', array('name'=>$this->manifest['package']['name'], 'title'=>$this->manifest['package']['title'], 'description'=>$this->manifest['package']['description'], 'version'=>$this->manifest['package']['version'], 'enabled'=>1, 'type'=>'extra'));
    }


    // get page_id by page_name
    public function _getPageId($page_name) {
        $info = $this->db->select()
            ->from('engine4_core_pages', array('page_id'))
            ->where('name = ?', $page_name)
            ->query()->fetch();
        return $info['page_id'];
    }


    // return content_id if specified widget exists
    // $page_id: page_id or page_name on witch page widget must exists
    public function _getContainerId($container_name, $page_id=0) {

        $select = $this->db->select()
            ->from('engine4_core_content', array('content_id'))
            ->where('name = ?', $container_name);

        if ( $page_id!==0 ) {
            $fieldname = is_int($page_id) ? 'id' : 'name';
            if ( $fieldname=='name' )
                $page_id = $this->_getPageId($page_id);
            if ( !empty($page_id) ) {
                $select->where('page_id = ?', $page_id);
            } else {
                return false;
            }
        }

        $info = $select->query()->fetch();

        if ( !empty($info) ) {
            return $info['content_id'];
        } else {
            return false;
        }
    }


    public function _query() {
    }

}