<?php return array(
'package'=>array(
	'type' => 'module',
	'name' => 'sepcore',
	'version' => '4.9.0',
	'path' => 'application/modules/Sepcore',
	'title' => 'SocialEnginePro Core',
	'description' => '',
	'author' => 'SocialEnginePro',
	'actions' => array(
		'install',
		'upgrade',
		'refresh',
		'enable',
		'disable',
	),
	'callback' => array(
		'path' => 'application/modules/Sepcore/settings/install.php',
		'class' => 'Sepcore_Install',
	),
	'directories' => array( 0=>'application/modules/Sepcore' ),
	'files' => array( 0=>'application/languages/en/sepcore.csv' ),
),

	// Routes --------------------------------------------------------------------
'routes' => array(
	'sepcore_admin' => array(
		'route' => 'admin/sepcore/:action/*',
			'defaults' => array(
			'module' => 'sepcore',
			'controller' => 'admin-index',
			'action' => 'plugins',
		),
	),
),

	// Menus --------------------------------------------------------------------
'menu' => array(
	'sepcore_admin_main'=>array(
		'title'=>'',
		'menuitems'=>array(
			array(
				'name'=>'sepcore_admin_main_settings',
				'label'=>'Global Settings',
				'plugin'=>'',
				'params'=>'{"route":"admin_default","module":"sepcore","controller":"settings"}'
			),
			array(
				'name'=>'sepcore_admin_main_plugins',
				'label'=>'All Plugins',
				'plugin'=>'',
				'params'=>'{"route":"admin_default","module":"sepcore"}'
			),
			array(
				'name'=>'sepcore_admin_main_installed',
				'label'=>'Installed Plugins',
				'plugin'=>'',
				'params'=>'{"route":"admin_default","module":"sepcore","controller":"installed"}'
			),
			array(
				'name'=>'sepcore_admin_main_contact',
				'label'=>'Contact Us',
				'plugin'=>'',
				'params'=>'{"route":"admin_default","module":"sepcore","controller":"contact"}'
			),
		),
	),
	'core_admin_main_plugins'=>array(
		'title'=>'',
		'menuitems'=>array(
			array(
				'name'=>'core_admin_main_plugins_sepcore',
				'label'=>'SocialEnginePro Core',
				'plugin'=>'',
				'params'=>'{"route":"admin_default","module":"sepcore","controller":"plugins"}'
			),
		),
	),
),
);
?>