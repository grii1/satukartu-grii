<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sepcore
 * @copyright  Copyright 2010-2012 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     altrego aka Vadim ( provadim@gmail.com )
 */

class Sepcore_Plugin_Menus {

  public function onMenuInitialize_InitializeIcon($row) {
	$info = explode('|', $row->label);
	$icon = $info[0];
	$label = ( !empty($info[1]) ) ? $info[1] : $info[0];
	$params = $row->params;
	$params['label'] = $label;
	return $params;

	return $label;
  }

}