<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sepcore
 * @copyright  Copyright 2010-2012 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     altrego aka Vadim
 */

class Sepcore_View_Helper_SepMessage extends Engine_View_Helper_HtmlElement {

	// color:	array('red','blue','black','green','yellow');    (refer to SepCore main.css)
	// icon:	array('download','purchase','seicon','sepicon','friends','info');    (refer to SepCore main.css)

  public function SepMessage($message, $color='yellow', $icon='') {

	if ( $icon ) {
		return '
			<div class="sep_message sep_' . $color . '">
				<div class="sep_icon_' . $icon . '_16"></div>
				<p>' . $this->view->translate($message) . '</p>
			</div>';
	} else {
		return '
			<div class="sep_message sep_' . $color . '">
				<p>' . $this->view->translate($message) . '</p>
			</div>';
	}
	return '';

  }

}