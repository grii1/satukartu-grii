<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sepcore
 * @copyright  Copyright 2010-2012 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     altrego aka Vadim
 */

class Sepcore_View_Helper_SepButton extends Engine_View_Helper_HtmlElement {

	// color:	array('red','blue','black','green','yellow');				(refer to SepCore main.css)
	// icon:	array('download','purchase','seicon','sepicon','friends');	(refer to SepCore main.css)
	// params:	array('id','target','smoothbox');

  public function SepButton($label, $link=array(), $color='blue', $icon=false, $params=array()) {

	$id = ( $params['id'] ) ? ' id="' . $params['id'] . '"' : '';
	$target = ( $params['new'] ) ? '_blank' : '';
	$smoothbox = ( $params['smoothbox'] ) ? ' smoothbox' : '';
	if ( $icon ) {
		return $this->view->htmlLink($link,
			$this->view->translate($label) . '<div class="sep_icon_' . $icon . '_16"></div>',
			array('class'=>'sep_btn_icon sep_' . $color . $smoothbox, 'id'=>$id, 'target'=>$target)
		);
	} else {
		return $this->view->htmlLink($link,
			$this->view->translate($label),
			array('class'=>'sep_btn sep_' . $color . $smoothbox, 'id'=>$id, 'target'=>$target)
		);
	}

  }

}