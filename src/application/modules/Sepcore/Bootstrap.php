<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sepcore
 * @copyright  Copyright 2010-2012 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     Vadim
 */

class Sepcore_Bootstrap extends Engine_Application_Bootstrap_Abstract {

  public function __construct($application) {
	$this->initViewHelperPath();
	parent::__construct($application);
  }

}


	// CHECK GLOBAL DEBUG
if (isset($_GET['debug']) && $_GET['debug']!='off') {
	setcookie('debug', true);
	$_COOKIE['debug']=true;
}

if (isset($_GET['debug']) && $_GET['debug']=='off') {
	setcookie('debug', false);
	$_COOKIE['debug'] = false;
}

if (!function_exists('print_arr')) {

function print_arr( $var, $die = false, $title = '', $ignore_cookie = false, $return = false ) {

	//if ($_COOKIE['debug']) {

		sep_debug_declare_styles();

		$type = gettype( $var );
		
		$out = print_r( $var, true );
		$out = htmlspecialchars( $out );
		$out = str_replace('  ', '&nbsp; ', $out );
		$global_count = ++$GLOBALS['debug_global_count'];
		if( $type == 'boolean' ) {
			$content = $var ? 'true' : 'false';

			$out = '
			<div class="debug_main_frame">
				<span class="debug_var_type">('.$type.')</span> '.$content.' <span class="debug_var_title">'.$title.'</span>
			</div>';
		}
		else {

			$object_definition = '';

			if ($type=='object') {
				$class_name = get_class($var);
				if ( get_parent_class($var) ) { $object_parent_class='&nbsp;exted&nbsp;'.get_parent_class($var); } else { $object_parent_class=''; }
				$object_definition = '
				<div>
					<a href="javascript://" id="debug_object_definition_'.$global_count.'_lnk" onclick="debug_functions_expand(\'debug_object_definition_'.$global_count.'\');" class="lnkExpand">+</a>&nbsp;&nbsp;&nbsp;&nbsp;<span class="debug_var_type">Functions</span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="debug_var_title"><a href="javascript://" onClick="debug_expand(\'debug_variable_definition_'.$global_count.'\');" id="debug_variable_definition_'.$global_count.'_lnk">[-]</a>&nbsp;&nbsp;'.$title.'</span>
					<div class="debug_object_definition" id="debug_object_definition_'.$global_count.'">
						<ul id="debug_object_definition_vars">';
				foreach ( get_class_vars($class_name) as $class_var) {
					$object_definition .= '<li>' . $class_var . '</li>';
				}
				$object_definition .= '</ul>';

				$object_definition .= '<ul id="debug_object_definition_functions">';
				foreach ( get_class_methods($var) as $method) {
					$object_definition .= '<li>' . $method . '()</li>';
				}
				$object_definition .= '
						</ul>
					</div>
					<hr>
				</div>';

				$content = nl2br( $out );
				$out = '
				<div class="debug_main_frame" id="debug_variable_definition_'.$global_count.'">'.$object_definition.'
					<span class="debug_var_type">('.$type.$object_parent_class.')</span> '.$content.'
				</div>';

			} else {
				$content = nl2br( $out );
				if (!empty($title)) {
					$out = '
					<div class="debug_main_frame" id="debug_variable_definition_'.$global_count.'">
						<span class="debug_var_title"><a href="javascript://" onClick="debug_expand(\'debug_variable_definition_'.$global_count.'\');" id="debug_variable_definition_'.$global_count.'_lnk">[-]</a>&nbsp;&nbsp;'.$title.'</span>
						<span class="debug_var_type">('.$type.')</span> '.$content.'
					</div>';
				} else {
					$out = '
					<div class="debug_main_frame" id="debug_variable_definition_'.$global_count.'">
						<span class="debug_var_title"><a href="javascript://" onClick="debug_expand(\'debug_variable_definition_'.$global_count.'\');" id="debug_variable_definition_'.$global_count.'_lnk">[-]</a></span>
						<span class="debug_var_type">('.$type.')</span> '.$content.'
					</div>';
				}
			}
			
		}


		if( !$return ) { echo $out; } else { return $out; }
		if( $die && $die<=$GLOBALS['debug_global_count'] ) { die('...stopped...'); }
	//}
	//else 
	//return;
}

function sep_debug_declare_styles() {
	if ($GLOBALS['debug_styles_declared']) return;
	$GLOBALS['debug_styles_declared'] = true;
	$GLOBALS['debug_global_count']=0;
?>
<html>
<head>
	<script type="text/javascript">
		function debug_expand(id) {
			var $element = document.getElementById(id);
			var $expand_link = document.getElementById(id + '_lnk');
			if ($expand_link.innerHTML=='[-]') {
				$element.style.height='2px';
				$element.style.padding='16px 0 0';
				$expand_link.innerHTML='[+]';
			} else {
				$element.style.height='auto';
				$element.style.padding='16px';
				$expand_link.innerHTML='[-]';
			}
		}
	
		function debug_functions_expand(id) {
			var $element = document.getElementById(id);
			var $expand_link = document.getElementById(id + '_lnk');
			if ($element.style.display=='block') {
				$element.style.display='none';
				$expand_link.innerHTML='+';
			} else {
				$element.style.display='block';
				$expand_link.innerHTML='-';
			}
		}
	</script>

	<style type="text/css">
			/* SEP DEBUG */
		.debug_main_frame { position:relative; margin:20px; padding:16px; border:2px inset #666; font:11px Verdana; background:#000; color:#6F6; text-align:left; overflow:hidden; }
		.debug_main_frame hr { margin:8px 0; border:none; border-top:1px solid #930; height:1px; line-height:1px; }
		.debug_main_frame a:link { color:#900; text-decoration:none; }
		.debug_main_frame a:hover { color:#900; text-decoration:underline; }
		.debug_var_type { color:#f66; }
		.debug_var_title { position:absolute; left:6px; top:2px; color:#999; font-size:10px; }
		.debug_var_title a:link { color:#999; text-decoration:none; }
		.debug_object_definition { display:none; }
		.debug_object_definition ul { padding-left:24px; }
	</style>
</head>
<body>
<?php
}

}
