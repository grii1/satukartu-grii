<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sepcore
 * @copyright  Copyright 2010-2012 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     Vadim
 */

class Sepcore_Widget_MainmenuController extends Engine_Content_Widget_Abstract {

  public function indexAction() {

	$this->view->navigation = $navigation = Engine_Api::_()
		->getApi('menus', 'core')
		->getNavigation('core_main');

	$this->view->viewer = $viewer = Engine_Api::_()->user()->getViewer();
	$require_check = Engine_Api::_()->getApi('settings', 'core')->getSetting('core.general.browse', 1);
	if(!$require_check && !$viewer->getIdentity()){
		$navigation->removePage($navigation->findOneBy('route','user_general'));
	}

		// Don't render this if not authorized
	$viewer = Engine_Api::_()->user()->getViewer();

		// Don't render this if friendships are disabled
	if( !Engine_Api::_()->getApi('settings', 'core')->user_friends_eligible || !$viewer->getIdentity() || !Engine_Api::_()->getApi('settings', 'core')->getSetting('sepcore.friends.renderwidget', 1) )
		return;

	$this->view->renderFriends = true;

		// Multiple friend mode
	$select = $viewer->membership()->getMembersOfSelect();
	$this->view->friends = $friends = $paginator = Zend_Paginator::factory($select);  

		// Set item count per page and current page number
	$paginator->setItemCountPerPage($this->_getParam('itemCountPerPage', 40));
	$paginator->setCurrentPageNumber($this->_getParam('page', 1));

		// Get stuff
	$ids = array();
	foreach( $friends as $friend )
		$ids[] = $friend->resource_id;

		// Get the items
	$friendUsers = array();
	foreach( Engine_Api::_()->getItemTable('user')->find($ids) as $friendUser ) {
			// Get info about messages
		$conversation = $this->_haveUnread($viewer->getIdentity(), $friendUser->getIdentity());
		if ( $conversation ) {
			$messageStatus = array('status'=>'user1_unread', 'conversation'=>$conversation);
		}
		else {
			$conversation = $this->_haveUnread($friendUser->getIdentity(), $viewer->getIdentity());
			if ( $conversation!=0 )
				$messageStatus = array('status'=>'user2_unread', 'conversation'=>$conversation);
			else
				$messageStatus = array('status'=>'noUnreadMessages', 'conversation'=>false);
		}

		$profileStatus = strip_tags($friendUser->status()->getLastStatus($friendUser)->body);
		if ( mb_strlen($profileStatus)>75 )
			$profileStatus = mb_substr($profileStatus,0,75) . '...';

		$friendUsers[] = array(
			'user'=>$friendUser,
			'message'=>$messageStatus,
			'profile_status'=>$profileStatus
		);
	}

	$this->view->friendUsers = $friendUsers;

  }


  private function _haveUnread($user1_id, $user2_id) {

	$rName = Engine_Api::_()->getDbtable('recipients', 'messages')->info('name');
	$select = Engine_Api::_()->getDbtable('recipients', 'messages')->select()
		->setIntegrityCheck(false)
		->from(array('in'=>$rName), new Zend_Db_Expr('in.inbox_message_id AS id'))
		->joinLeft(array('out'=>$rName), 'in.conversation_id=out.conversation_id')
		->where('in.user_id = ?', $user1_id)
		->where('in.inbox_deleted = ?', 0)
		->where('in.inbox_read = ?', 0)
		->where('out.user_id = ?', $user2_id);
	$data = Engine_Api::_()->getDbtable('recipients', 'messages')->fetchRow($select);

	if ( !$data )
		return false;

	$conversation = Engine_Api::_()->getItem('messages_message', $data->id);

	return $conversation;

  }

}