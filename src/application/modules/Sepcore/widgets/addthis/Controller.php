<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sepcore
 * @copyright  Copyright 2010-2012 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     Vadim
 */

class Sepcore_Widget_AddthisController extends Engine_Content_Widget_Abstract {

  public function indexAction() {
	$settings = Engine_Api::_()->getApi('settings', 'core');
	$this->view->addthis = unserialize($settings->getSetting('sepcore.addthis',''));
	if ( !$this->view->addthis ) {
		$this->view->addthis = array('style'=>0, 'sharing'=>0);
	}
  }
}

