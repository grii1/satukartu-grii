<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sepcore
 * @copyright  Copyright 2010-2012 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     altrego aka Vadim ( provadim@gmail.com )
 */
?>

<?php $items_count = Engine_Api::_()->getApi('settings', 'core')->getSetting('sepcore.languages.count', 5); ?>
<?php $counter = 1; ?>

<style type="text/css">
.layout_page_footer { overflow:visible; }
.layout_page_footer .layout_main { height:40px; overflow:visible; }
.layout_page_footer #language { display:none; }
</style>

<form method="post" action="<?php print $this->url(array('controller' => 'utility', 'action' => 'locale'), 'default', true)?>" style="display:inline-block">

<?php $selectedLanguage = $this->translate()->getLocale() ?>

<ul class="sep_languages">
<?php foreach($this->languageNameList as $code=>$lang) { ?>
	<?php if ( $counter>$items_count ) { break; } ?>
	<li<?php if($selectedLanguage==$code) { print ' class="active"'; } ?>><a href="javascript://" onclick="$('sep-selected-lang').set('value','<?php echo $code; ?>'); $(this).getParent('form').submit();"><?php echo $lang; ?></a></li>
	<?php $counter++; ?>
<?php } ?>
	<?php if ( count($this->languageNameList)>$items_count ) print '<li><a href="javascript://" onclick="$(\'sep_languages_dropdown\').setStyle(\'display\',\'block\');">' . $this->translate('More...') . '</a></li>'; ?>
</ul>

<div id="sep_languages_dropdown">
	<ul>
<?php
	if ( count($this->languageNameList)>$items_count ) {
		foreach($this->languageNameList as $code=>$lang) {
			if( $selectedLanguage==$code ) { $active = ' class="active"'; } else { $active = ''; }
			print '<li' . $active . '><a href="javascript://" onclick="$(\'sep-selected-lang\').set(\'value\',\'' . $code . '\'); $(this).getParent(\'form\').submit();">'.$lang.'</a></li>';
		}
	}
?>
	</ul>
	<a href="javascript://" class="btnClose" onclick="$('sep_languages_dropdown').setStyle('display','none');">X</a>
</div>

<?php echo $this->formHidden('language', $selectedLanguage, array('id'=>'sep-selected-lang')) ?>
<?php echo $this->formHidden('return', $this->url()) ?>
</form>
