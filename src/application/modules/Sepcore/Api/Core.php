<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sepcore
 * @copyright  Copyright 2010-2012 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     Vadim
 */

class Sepcore_Api_Core extends Core_Api_Abstract {

  public function truncate($string, $length = 80, $etc = '...', $break_words = false, $middle = false) {
	if ($length == 0)
		return '';

	if (strlen($string) > $length) {
		$length -= strlen($etc);
		if (!$break_words && !$middle) {
			$string = preg_replace('/\s+?(\S+)?$/', '', Engine_String::substr($string, 0, $length + 1));
		}
		if (!$middle) {
			return Engine_String::substr($string, 0, $length).$etc;
		} else {
			return Engine_String::substr($string, 0, $length/2) . $etc . Engine_String::substr($string, -$length/2);
		}
	} else {
		return $string;
	}
  }

}