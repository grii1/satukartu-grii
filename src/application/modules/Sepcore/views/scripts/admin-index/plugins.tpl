<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sepcore
 * @copyright  Copyright 2010-2012 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     altrego aka Vadim ( provadim@gmail.com )
 */
?>

<?php $this->headLink()->appendStylesheet($baseUrl . 'application/modules/Sepcore/external/styles/main.css'); ?>

<div class='tabs'><?php print $this->navigation()->menu()->setContainer($this->navigation)->render()?></div>

<?php if ( !empty($this->error) ) { print $this->sepMessage('Error: ' . $this->error, 'red'); } ?>

	<!-- /// HAVE PLUGINS \\\ -->
<?php if ( count($this->plugins) ) { ?>

	<ul class="sep_plugins_list">
	<?php foreach($this->plugins as $plugin) { ?>
		<?php if ( empty($plugin['name']) ) continue; ?>
		<li <?php if ( !$plugin['license'] && $plugin['installed'] ) { print ' class="plugin_nolicense"'; } ?>>

			<div class="sep_plugin_icon" style="background-image:url('<?php print $plugin['icon']; ?>');">
				<?php if ( !$plugin['license'] && $plugin['installed'] ) { print '<div class="sep_icon sep_icon_error"></div>'; } ?>
			</div>

			<div class="sep_plugin_info">
				<div class="sep_plugin_title"><a href="<?php print $plugin['link'] ?>"><?php print $plugin['title'] ?></a></div>

				<div class="sep_plugin_version">
					<span class="sep_standart_textstrong"><?php print $this->translate('Version'); ?>:</span>
						<!-- /// if plugin installed, check upgrade AND license -->
					<?php if ( $plugin['installed'] ) { ?>
						<?php print $plugin['installed_version']; ?>

						<?php if ( $plugin['upgrade'] ) { ?>
							&rarr;&nbsp;<span class="sep_standart_textgreen"><?php print $plugin['version'] ?></span>
						<?php } ?>

						<?php if ( $plugin['license'] && $plugin['upgrade'] ) { ?>
							<!-- /// if licensed and need upgrade -->
							<small class="sep_plugin_status sep_plugin_status_upgrade">(<?php print $this->translate('sep_You can upgrade this plugin'); ?>)</small>
						<?php } elseif ( $plugin['license'] && !$plugin['upgrade'] ) { ?>
							<!-- /// if licensed and actual -->
							<small class="sep_plugin_status sep_plugin_status_actual">(<?php print $this->translate('sep_You have actual version of this plugin'); ?>)</small>
						<?php } elseif ( !$plugin['license'] && $plugin['upgrade'] ) { ?>
							<!-- /// if not licensed and upgrade -->
							<small class="sep_plugin_status sep_plugin_status_illegal">(<?php print $this->translate('sep_You have to install the plugin licensed\'s copy, to upgrade.'); ?>)</small>
						<?php } elseif ( !$plugin['license'] && !$plugin['upgrade'] ) { ?>
							<!-- /// if not licensed and actual -->
							<small class="sep_plugin_status sep_plugin_status_illegal">(<?php print $this->translate('sep_You use not licensed copy of this plugin. Please, buy license, if you want use this plugin'); ?>)</small>
						<?php } ?>
					<?php } else { ?>
						<?php print $plugin['version']; ?>
					<?php } ?>
				</div>

				<div class="sep_plugin_description"><?php print $plugin['description'] ?></div>

				<div class="sep_plugin_buttons">
				<?php
				// if plugin installed
				if ( $plugin['installed'] ) {
						// if installed and not licensed show purchase button
					if ( !$plugin['license'] ) {
						print $this->SepButton('Purchase', $plugin['link_purchase'], 'red', 'purchase', array('new'=>true));
					}
						// if installed and need upgrade show upgrade button
					if ( $plugin['upgrade'] && $plugin['license'] ) {
						print $this->SepButton('Upgrade', array('action'=>'install', 'package'=>$plugin['name']), 'green', 'download', array('new'=>true));
					}
				} else {
						// if plugin licensed show install button
					if ( $plugin['license'] ) {
						print $this->SepButton('Install', array('action'=>'install', 'package'=>$plugin['name']), 'green', 'download', array('new'=>true));
					}
						// if not licensed show purchase button
					if ( !$plugin['license'] ) {
						print $this->SepButton('Purchase', $plugin['link_purchase'], 'blue', 'purchase', array('new'=>true));
					}
						// if official link exists
					if ( !empty($plugin['link']) ) {
						print $this->SepButton('Official page', $plugin['link'], 'black', 'sepicon', array('new'=>true));
					}
						// if socialengine.net link exists
					if ( !empty($plugin['link_se']) ) {
						print $this->SepButton('Check on Socialengine.net', $plugin['link_se'], 'black', 'seicon', array('new'=>true));
					}
				}
				?>
				</div>
			</div>

		</li>
	<?php } ?>
	</ul>

	<!-- /// HAVE NO PLUGINS \\\ -->
<?php }