<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sepcore
 * @copyright  Copyright 2010-2012 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     Vadim
 */
?>

<div class='tabs'><?php print $this->navigation()->menu()->setContainer($this->navigation)->render()?></div>

<div class="sep_admin_home_wrapper">
<?php
if ( !empty($this->error) ) {
	echo $this->sepMessage($this->error,'red','error');
}
?>
</div>