<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sepcore
 * @copyright  Copyright 2010-2012 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     Vadim
 */
?>

<?php $this->headLink()->appendStylesheet($baseUrl . 'application/modules/Sepcore/external/styles/main.css'); ?>

<div class='tabs'><?php print $this->navigation()->menu()->setContainer($this->navigation)->render()?></div>

<div class="sep_admin_contacts_left">
	<h3>Contact <span>Form</span></h3>
	<p>Feel free contact to our team using the contact form below. Also you may create support ticket in your client <a href="http://socialenginepro.com/client_home/">home</a>.</p>
	<?php
		if ( !empty($this->message) ) {
			print '<div style="text-align:center;">' . $this->sepMessage($this->message, 'green') . '</div>';
		} elseif ( !empty($this->error) ) {
			print $this->sepMessage($this->error, 'red');
		} else {
			print $this->form->render();
		}
	?>
</div>
<div class="sep_admin_contacts_right">
	<h3>Support <span>Timings</span></h3>
	Monday to Friday, 9 AM to 6 PM <br>
	Our Timezone: GMT + 06:00
	<h3 style="margin-top:30px;">Our  <span>Contacts</span></h3>
	You can contact us using the different online messengers below. We will answer them quickly, then email.
	<table cellspacing="0" cellpadding="0" style="margin-left:12px;">
		<tr>
			<td height="50" width="40"><img src="http://socialenginepro.com/images/gtalk.jpg"></td>
			<td><a href="mailto:support@socialenginepro.com">support@socialenginepro.com</a></td>
		</tr>
		<tr>
			<td height="50" width="40"><img src="http://socialenginepro.com/images/skype.jpg"></td>
			<td><a href="skype:SocialEnginePro">SocialEnginePro</a></td>
		</tr>
		<tr>
			<td height="50" width="40"><img src="http://socialenginepro.com/images/icq.png"></td>
			<td>406752106</td>
		</tr>
	</table>
</div>