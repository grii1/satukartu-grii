<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sepcore
 * @copyright  Copyright 2010-2012 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     Vadim
 */
?>

<div class='tabs'><?php print $this->navigation()->menu()->setContainer($this->navigation)->render()?></div>

<div class="sep_admin_home_wrapper">

<div class="settings">
<?php print $this->form->render(); ?>
</div>

</div>