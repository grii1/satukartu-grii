<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sepcore
 * @copyright  Copyright 2010-2012 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     altrego aka Vadim ( provadim@gmail.com )
 */

class Sepcore_Form_Admin_Global extends Engine_Form {

  public function init() {
	$this
		->setTitle('SocialEnginePro Core Module Global Settings')
		->setDescription('Below you can edit global settings of SocialEnginePro Core plugin. Note that you can upgrade and install SocialEnginePro Plugins from All Plugins section only of you enter your client email and password from SocialEnginePro client area.');

	$settings = Engine_Api::_()->getApi('settings', 'core');
	$client_email = $settings->getSetting('sepcore.client.email');

		// Element: client_email
	$this->addElement('Text', 'client_email', array(
		'label' => 'SocialEnginePro Account E-Mail',
		'value' => $settings->getSetting('sepcore.client.email', ''),
	));

		// Element: client_password
	$this->addElement('Password', 'client_password', array(
		'label' => 'SocialEnginePro Account Password',
		'autocomplete' => 'off',
		'value' => $settings->getSetting('sepcore.client.password', ''),
	));

		// Element: mainmenu_count
	$this->addElement('Text', 'mainmenu_count', array(
		'label' => 'SEP Main Menu widget visible menu items count',
		'value' => $settings->getSetting('sepcore.mainmenu.count', 10),
		'required' => true,
		'notEmpty' => true,
		'validators' => array(array('NotEmpty', true))
	));
	$this->mainmenu_count->getValidator('NotEmpty')->setMessage('Please enter a valid visible menu items count.', 'isEmpty');

		// Element: languages_count
	$this->addElement('Text', 'languages_count', array(
		'label' => 'SEP Language widget visible langs count',
		'value' => $settings->getSetting('sepcore.languages.count', 6),
		'required' => true,
		'notEmpty' => true,
		'validators' => array(array('NotEmpty', true))
	));
	$this->languages_count->getValidator('NotEmpty')->setMessage('Please enter a valid visible langs count.', 'isEmpty');


	$settings_addthis = unserialize($settings->getSetting('sepcore.addthis',''));
	if ( !$settings_addthis ) {
		$settings_addthis = array('style'=>0);
	}
	$this->addElement('Radio', 'addthis_style', array(
			'label'=>'Select style of AddThis widget:',
			'value'=>$settings_addthis['style'],
			'multiOptions'=>array_fill(0, 8, '')
		)
	);

	$this->addElement('Button', 'submit', array(
		'label' => 'Save Changes',
		'type' => 'submit'
	));
  }


  public function save(){

	$settings = Engine_Api::_()->getApi('settings', 'core');
	$client_email = $this->getValue('client_email');
	$client_password = $this->getValue('client_password');
	$languages_count = $this->getValue('languages_count');
	$mainmenu_count = $this->getValue('mainmenu_count');

	if ( !empty($client_password) ) {
		$settings->setSetting('sepcore.client.email', $client_email);
		$settings->setSetting('sepcore.client.password', md5($client_password));
	}

	$addthis_settings = array();
	$addthis_settings['style'] = $this->getValue('addthis_style');
	$addthis_settings = serialize($addthis_settings);
	$settings->setSetting('sepcore.addthis', $addthis_settings);

	$settings->setSetting('sepcore.mainmenu.count', $this->getValue('mainmenu_count'));
	$settings->setSetting('sepcore.languages.count', $this->getValue('languages_count'));

  }

}