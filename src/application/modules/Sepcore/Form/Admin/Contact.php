<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sepcore
 * @copyright  Copyright 2010-2012 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     altrego aka Vadim ( provadim@gmail.com )
 */

class Sepcore_Form_Admin_Contact extends Engine_Form {

  public function init() {
	$this->addElement('Select', 'subject', array(
		'label' => 'Subject',
		'multiOptions' => array(0=>'',
			'Question about a plugin/template'=>'Question about a plugin/template',
			'Custom development plugin/template'=>'Custom development plugin/template',
			'SocialEngine Customization'=>'SocialEngine Customization',
			'I have a suggestion about plugin/templates'=>'I have a suggestion about plugin/templates',
			'Report a Bug'=>'Report a Bug',
			'About us'=>'About us'
		)
	));

	$this->addElement('Textarea', 'message', array(
		'label' => 'Message',
		'required' => true,
		'notEmpty' => true,
		'validators' => array(array('NotEmpty', true))
	));
	$this->message->getValidator('NotEmpty')->setMessage('Please enter your message.', 'isEmpty');

	$this->addElement('Button', 'submit', array(
		'label' => 'Send',
		'type' => 'submit',
		'ignore' => true
	));
  }
}