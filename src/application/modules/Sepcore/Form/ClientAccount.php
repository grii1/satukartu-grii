<?php
/**
 * SocialEngine
 *
 * @category   Application_Extensions
 * @package    Sepcore
 * @copyright  Copyright 2010-2012 SocialEnginePro
 * @license    http://www.socialenginepro.com
 * @author     altrego aka Vadim ( provadim@gmail.com )
 */

class Sepcore_Form_ClientAccount extends Engine_Form {

  public function init() {

	$this
		->setMethod('post')
		->setOptions(array('style'=>'float:right; margin:10px 210px 12px 0;'));

	$this->addElement('Text', 'client_email', array(
		'label' => 'SocialEnginePro Account E-Mail',
		'allowEmpty' => false,
		'required' => true,
		'validators' => array(
			array('NotEmpty', true),
			array('StringLength', false, array(1, 128)),
		),
	));

	$this->addElement('Password', 'client_password', array(
		'label' => 'SocialEnginePro Account Password',
		'allowEmpty' => false,
		'required' => true,
		'autocomplete' => 'off',
		'validators' => array(
			array('NotEmpty', true),
		),
	));

	$this->addElement('Button', 'submit', array(
		'label' => 'Save and Continue installation',
		'type' => 'submit',
		'ignore' => true,
	));

  }


  public function save(){

	$db = Zend_Registry::get('Zend_Db');

	$email_select = $db->select()
		->from('engine4_core_settings')
		->where('name = ?', 'sepcore.client.email');
	$pass_select = $db->select()
		->from('engine4_core_settings')
		->where('name = ?', 'sepcore.client.password');

	$account_info = array();
	$account_info['email'] = $db->fetchRow($email_select);
	$account_info['password'] = $db->fetchRow($pass_select);

	$new_email = $this->getValue('client_email');
	$new_password = md5($this->getValue('client_password'));

	if ( empty($account_info['email']) ) {
		$db->insert( 'engine4_core_settings', array('name'=>'sepcore.client.email', 'value'=>$new_email));
	} else {
		$db->update( 'engine4_core_settings', array('value'=>$new_email), 'name="sepcore.client.email"');
	}

	if ( empty($account_info['password']) ) {
		$db->insert( 'engine4_core_settings', array('name'=>'sepcore.client.password', 'value'=>$new_password));
	} else {
		$db->update( 'engine4_core_settings', array('value'=>$new_password), 'name="sepcore.client.password"');
	}

  }

}