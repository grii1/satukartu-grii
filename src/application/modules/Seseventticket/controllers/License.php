<?php
//folder name or directory name. 
$module_name = 'seseventticket'; 
//product title and module title. 
$module_title = 'Advanced Events - Events Tickets Selling & Booking System'; 
if (!$this->getRequest()->isPost()) { return; } 
if (!$form->isValid($this->getRequest()->getPost())) { return; } 

if ($this->getRequest()->isPost()) { 
    // $postdata = array(); 
    // //domain name 
    // $postdata['domain_name'] = $_SERVER['HTTP_HOST']; 
    // //license key 
    // $postdata['licenseKey'] = @base64_encode($_POST['seseventticket_licensekey']); 
    // $postdata['module_title'] = @base64_encode($module_title); 
    // $ch = curl_init(); 
    // curl_setopt($ch, CURLOPT_URL, "https://socialnetworking.solutions/licensecheck.php"); 
    // curl_setopt($ch, CURLOPT_POST, 1); 
    // // in real life you should use something like: curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postdata)); 
    // // receive server response ... 
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
    // $server_output = curl_exec($ch); 
    // $error = 0; 
    // if (curl_error($ch)) { $error = 1; } 
    // curl_close($ch); 

    $server_output = "OK";
    $error = 0;
    //here we can set some variable for checking in plugin files. 
    if ($server_output == "OK" && $error != 1) { 
        if (!Engine_Api::_()->getApi('settings', 'core')->getSetting('seseventticket.pluginactivated')) { 
            $db = Zend_Db_Table_Abstract::getDefaultAdapter(); 
            
            $db->query('INSERT IGNORE INTO `engine4_sesevent_dashboards` (`type`, `title`, `enabled`, `main`) VALUES ("tickets", "Tickets", "1", "1"), ("manage_tickets", "Manage Tickets", "1", "0"), ("create_tickets", "Create New Tickets", "1", "0"), ("event_ticket_information", "Additional Details on Print", "1", "0"), ("search_ticket", "Search Tickets", "1", "0"), ("account_details", "Payment Gateway", "1", "0"), ("sales_statistics", "Sales Statistics", "1", "0"), ("manage_orders", "Manage Ticket Orders", "1", "0"), ("sales_orders", "Sales Reports", "1", "0"), ("payment_requests", "Payment Requests", "1", "0"), ("payment_transactions", "Payments Received", "1", "0");'); 
            
            $db->query('DROP TABLE IF EXISTS `engine4_sesevent_tickets`;'); 
            
            $db->query('CREATE TABLE `engine4_sesevent_tickets` ( `ticket_id` int(11) unsigned NOT NULL AUTO_INCREMENT, `event_id` int(11) unsigned NOT NULL, `type` enum("free","paid") NOT NULL DEFAULT "paid", `name` VARCHAR(255) NOT NULL, `description` VARCHAR(255) NOT NULL, `price` decimal(7,2) NOT NULL DEFAULT "0.00", `currency` VARCHAR(20) NOT NULL , `starttime` VARCHAR(255) NOT NULL, `endtime` VARCHAR(255) NOT NULL, `service_tax` FLOAT NOT NULL DEFAULT 0, `entertainment_tax` FLOAT NOT NULL DEFAULT 0, `total` smallint (5) NOT NULL DEFAULT 0, `min_quantity` tinyint (5) NOT NULL DEFAULT 0, `max_quantity` tinyint (5) NOT NULL DEFAULT 0, `order` TINYINT(11) NOT NULL DEFAULT 0, `is_delete` TINYINT(1) NOT NULL DEFAULT "0", `search` TINYINT(1) NOT NULL default "1", `photo_id` INT(11) unsigned NOT NULL default "0", `view_count` INT(11) unsigned NOT NULL default "0", `comment_count` INT(11) unsigned NOT NULL default "0", `rating` float NOT NULL DEFAULT 0, `creation_date` datetime NOT NULL, `modified_date` datetime NOT NULL, PRIMARY KEY (`ticket_id`), KEY (`event_id`), KEY (`search`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;'); 
            
            $db->query('DROP TABLE IF EXISTS `engine4_sesevent_ordertickets`;'); 
            
            $db->query('CREATE TABLE IF NOT EXISTS `engine4_sesevent_ordertickets` ( `orderticket_id` INT(11) unsigned NOT NULL auto_increment, `ticket_id` INT(11) unsigned NOT NULL, `event_id` INT(11) unsigned NOT NULL, `owner_id` INT(11) unsigned NOT NULL, `order_id` INT(11) unsigned NOT NULL, `quantity` TINYINT(11) NOT NULL DEFAULT "0", `title` VARCHAR(255) NOT NULL , `service_tax` FLOAT NULL, `price` float NOT NULL DEFAULT "0.00", `entertainment_tax` FLOAT NULL, `is_delete` TINYINT(1) NOT NULL DEFAULT "0", `creation_date` datetime NOT NULL, `state` ENUM("pending","cancelled","failed","incomplete","complete","refund") DEFAULT "incomplete", `modified_date` datetime NOT NULL, PRIMARY KEY (`orderticket_id`), KEY `order_id` (`order_id`), KEY `ticket_id` (`ticket_id`), KEY `event_id` (`event_id`), KEY `owner_id` (`owner_id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;'); 
            
            $db->query('DROP TABLE IF EXISTS `engine4_sesevent_orderticketdetails`;'); 
            
            $db->query('CREATE TABLE IF NOT EXISTS `engine4_sesevent_orderticketdetails` ( `orderticketdetails_id` int(11) unsigned NOT NULL auto_increment, `first_name` varchar(255) NOT NULL, `last_name` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `mobile` varchar(255) NOT NULL, `ticket_id` TINYINT(11) DEFAULT 0, `ticket_number` TINYINT(11) DEFAULT 0, `order_id` INT(11) DEFAULT 0, `registration_number` varchar(45) NOT NULL, PRIMARY KEY (`orderticketdetails_id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;'); 
            
            $db->query('DROP TABLE IF EXISTS `engine4_sesevent_orders`;'); 
            
            $db->query('CREATE TABLE IF NOT EXISTS `engine4_sesevent_orders` ( `order_id` int(11) unsigned NOT NULL auto_increment, `event_id` int(11) unsigned NOT NULL, `owner_id` int(11) unsigned NOT NULL, `gateway_id` varchar(128) DEFAULT NULL, `fname` varchar(128) DEFAULT NULL, `lname` varchar(128) DEFAULT NULL, `email` varchar(128) DEFAULT NULL, `company_title` varchar(128) DEFAULT NULL, `mobile` varchar(128) DEFAULT NULL, `ragistration_number` varchar(255) DEFAULT NULL, `order_no` varchar(255) DEFAULT NULL, `gateway_transaction_id` varchar(128) DEFAULT NULL, `total_tickets` tinyint(11) NOT NULL DEFAULT "1", `commission_amount` float DEFAULT 0, `total_service_tax` float DEFAULT 0, `total_entertainment_tax` float DEFAULT 0, `order_note` TEXT DEFAULT NULL, `private` TINYINT(1) DEFAULT "0", `state` ENUM("pending","cancelled","failed","incomplete","complete","refund") DEFAULT "incomplete", `change_rate` float DEFAULT "0.00", `total_amount` float DEFAULT "0.00", `currency_symbol` VARCHAR(45) NOT NULL, `gateway_type` VARCHAR(45) NOT NULL DEFAULT "Paypal", `is_delete` TINYINT(1) NOT NULL DEFAULT "0", `ip_address` varchar(55) NOT NULL DEFAULT "0.0.0.0", `creation_date` datetime NOT NULL, `modified_date` datetime NOT NULL, PRIMARY KEY (`order_id`), KEY `event_id` (`event_id`), KEY `owner_id` (`owner_id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;'); 
            
            $db->query('DROP TABLE IF EXISTS `engine4_sesevent_userpayrequests`;'); 
            
            $db->query('CREATE TABLE IF NOT EXISTS `engine4_sesevent_userpayrequests` ( `userpayrequest_id` INT(11) unsigned NOT NULL auto_increment, `event_id` INT(11) unsigned NOT NULL, `owner_id` INT(11) unsigned NOT NULL, `requested_amount` FLOAT DEFAULT "0", `release_amount` FLOAT DEFAULT "0", `user_message` TEXT, `admin_message` TEXT, `creation_date` datetime NOT NULL, `release_date` datetime NOT NULL, `is_delete` TINYINT(1) NOT NULL DEFAULT "0", `gateway_id` TINYINT (1) DEFAULT "2", `gateway_transaction_id` varchar(128) DEFAULT NULL, `state` ENUM("pending","cancelled","failed","incomplete","complete","refund") DEFAULT "pending", `currency_symbol` VARCHAR(45) NOT NULL, `gateway_type` VARCHAR(45) NOT NULL DEFAULT "Paypal", PRIMARY KEY (`userpayrequest_id`), KEY `event_id` (`event_id`), KEY `owner_id` (`owner_id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;'); 
            
            $db->query('INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`) VALUES ("sesevent_admin_main_managetickets", "sesevent", "Manage Tickets", "", \'{"route":"admin_default","module":"sesevent","controller":"tickets","action":"index"}\', "seseventticket_admin_main", "", 2), ("sesevent_admin_main_manageticketssub", "sesevent", "Manage Event Tickets", "", \'{"route":"admin_default","module":"sesevent","controller":"tickets","action":"index"}\', "sesevent_admin_main_managetickets", "", 1), ("sesevent_admin_main_manageticketorderssub", "sesevent", "Manage Ordered Tickets", "", \'{"route":"admin_default","module":"sesevent","controller":"tickets","action":"manage-ticket-orders"}\', "sesevent_admin_main_managetickets", "", 2), ("sesevent_admin_main_paymentrequest", "sesevent", "Manage Payments", "", \'{"route":"admin_default","module":"sesevent","controller":"payment"}\', "seseventticket_admin_main", "", 3), ("sesevent_admin_main_paymentrequestsub", "sesevent", "Payment Requests", "", \'{"route":"admin_default","module":"sesevent","controller":"payment"}\', "sesevent_admin_main_paymentrequest", "", 1), ("sesevent_admin_main_managepaymenteventownersub", "sesevent", "Manage Payments Made", "", \'{"route":"admin_default","module":"sesevent","controller":"tickets","action":"manage-payment-event-owner"}\', "sesevent_admin_main_paymentrequest", "", 2), ("sesevent_admin_main_currency", "sesevent", "Manage Currency", "", \'{"route":"admin_default","module":"sesbasic","controller":"settings","action":"currency"}\', "seseventticket_admin_main", "", 4), ("sesevent_admin_main_gateway", "sesevent", "Manage Gateways", "", \'{"route":"admin_default","module":"sesevent","controller":"gateway"}\', "seseventticket_admin_main", "", 5), ("sesevent_main_mytickets", "sesevent", "My Tickets", "Sesevent_Plugin_Menus::canViewEventsTicket", \'{"route":"sesevent_my_ticket"}\', "sesevent_main", "", 10);'); 
            
            include_once APPLICATION_PATH . "/application/modules/Seseventticket/controllers/defaultsettings.php"; 
            Engine_Api::_()->getApi('settings', 'core')->setSetting('seseventticket.pluginactivated', 1); 
            Engine_Api::_()->getApi('settings', 'core')->setSetting('seseventticket.licensekey', $_POST['seseventticket_licensekey']); 
        } 
        $domain_name = @base64_encode(str_replace(array('http://','https://','www.'),array('','',''),$_SERVER['HTTP_HOST'])); $licensekey = Engine_Api::_()->getApi('settings', 'core')->getSetting('seseventticket.licensekey'); 
        $licensekey = @base64_encode($licensekey); 
            
        Engine_Api::_()->getApi('settings', 'core')->setSetting('seseventticket.sesdomainauth', $domain_name);
        Engine_Api::_()->getApi('settings', 'core')->setSetting('seseventticket.seslkeyauth', $licensekey); 
        $error = 1; 
    } 
    else { 
        $error = $this->view->translate('Please enter correct License key for this product.'); 
        $error = Zend_Registry::get('Zend_Translate')->_($error); 
        $form->getDecorator('errors')->setOption('escape', false); 
        $form->addError($error); 
        $error = 0; 
                
        Engine_Api::_()->getApi('settings','core')->setSetting('seseventticket.licensekey', $_POST['seseventticket_licensekey']); 
        return; 
        $this->_helper->redirector->gotoRoute(array()); 
    } 
}