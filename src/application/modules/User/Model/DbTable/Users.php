<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Users.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    User
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 */
class User_Model_DbTable_Users extends Engine_Db_Table
{
  protected $_name = 'users';

  protected $_rowClass = 'User_Model_User';

  public function getUser($params = array()) {
    $tabeleName = $this->info('name');
    $select = $this->select()->from($tabeleName);
    if (isset($params['user_id']))
      $select->where('user_id =?', $params['user_id']);

    return $this->fetchRow($select);
  }

  public function setBalance($balance,$user_id){
    $data = array('balance' => $balance);
    $where = array('user_id' => $user_id);
    $this->update($data,$where);
  }

  public function approve($id){
    $this->update(
      array(
        'approved' =>  1 ,
        'enabled' => 1,
        'modified_date' => new Zend_Db_Expr('NOW()')
      ),
      array('user_id =?' => $id)
    );
  }
}
