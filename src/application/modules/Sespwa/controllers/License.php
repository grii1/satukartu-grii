<?php
//folder name or directory name. 
$module_name = 'sespwa'; 
//product title and module title. 
$module_title = 'Progressive Web App (PWA) Plugin - Interactive Mobile & Tablet Interface'; 
if (!$this->getRequest()->isPost()) { return; } 
if (!$form->isValid($this->getRequest()->getPost())) { return; } 
if ($this->getRequest()->isPost()) { 
    // $postdata = array(); 
    // //domain name 
    // $postdata['domain_name'] = $_SERVER['HTTP_HOST']; 
    // //license key 
    // $postdata['licenseKey'] = @base64_encode($_POST['sespwa_licensekey']); 
    // $postdata['module_title'] = @base64_encode($module_title); 
    // $ch = curl_init(); curl_setopt($ch, CURLOPT_URL, "https://socialnetworking.solutions/licensecheck.php"); 
    // curl_setopt($ch, CURLOPT_POST, 1); 
    // // in real life you should use something like: curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postdata)); 
    // // receive server response ... 
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); $server_output = curl_exec($ch); $error = 0; 
    // if (curl_error($ch)) { $error = 1; } 
    // curl_close($ch); 

    $server_output = "OK";
    $error = 0;
    //here we can set some variable for checking in plugin files. 
    if ($server_output == "OK" && $error != 1) { 
        if (!Engine_Api::_()->getApi('settings', 'core')->getSetting('sespwa.pluginactivated')) { 
            $db = Zend_Db_Table_Abstract::getDefaultAdapter(); 
            $db->query('INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`) VALUES ("sespwa_admin_main_managetemplates", "sespwa", "Manage Templates", "", \'{"route":"admin_default","module":"sespwa","controller":"content"}\', "sespwa_admin_main", "", 2), ("sespwa_admin_main_managethemes", "sespwa", "Manage Themes", "", \'{"route":"admin_default","module":"sespwa","controller":"themes"}\', "sespwa_admin_main", "", 3), ("sespwa_admin_main_manifest", "sespwa", "Manage Manifest", "", \'{"route":"admin_default","module":"sespwa","controller":"settings","action":"manifest"}\', "sespwa_admin_main", "", 5), ("sespwa_admin_main_menusettings", "sespwa", "Menus Settings", "", \'{"route":"admin_default","module":"sespwa","controller":"settings","action":"menu-settings"}\', "sespwa_admin_main", "", 4), ("sespwa_admin_main_managebanners", "sespwa", "Manage Banners", "", \'{"route":"admin_default","module":"sespwa","controller":"manage-banner","action":"index"}\', "sespwa_admin_main", "", 7);'); 
            
            $db->query('DROP TABLE IF EXISTS `engine4_sespwa_pages`;'); 
            $db->query('CREATE TABLE IF NOT EXISTS `engine4_sespwa_pages` ( `page_id` int(11) unsigned NOT NULL auto_increment, `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_general_ci NULL, `displayname` varchar(128) NOT NULL default "", `url` varchar(128) NULL, `title` varchar(255) NOT NULL, `description` text NOT NULL, `keywords` text NOT NULL, `custom` tinyint(1) NOT NULL default "1", `fragment` tinyint(1) NOT NULL default "0", `layout` varchar(32) NOT NULL default "", `levels` text default NULL, `provides` text default NULL, `view_count` int(11) unsigned NOT NULL default "0", `search` tinyint(1) NOT NULL default "0", PRIMARY KEY (`page_id`), UNIQUE KEY `name` (`name`), UNIQUE KEY `url` (`url`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci ;'); 
            $db->query('INSERT INTO engine4_sespwa_pages SELECT * FROM engine4_core_pages;'); 
            
            $db->query('DROP TABLE IF EXISTS `engine4_sespwa_content`;'); 
            $db->query('CREATE TABLE IF NOT EXISTS `engine4_sespwa_content` ( `content_id` int(11) unsigned NOT NULL auto_increment, `page_id` int(11) unsigned NOT NULL, `type` varchar(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL default "widget", `name` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL, `parent_content_id` int(11) unsigned NULL, `order` int(11) NOT NULL default "1", `params` text NULL, `attribs` text NULL, PRIMARY KEY (`content_id`), KEY (`page_id`, `order`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;'); 
            $db->query('INSERT INTO engine4_sespwa_content SELECT * FROM engine4_core_content;'); 
            
            $db->query('DROP TABLE IF EXISTS `engine4_sespwa_themes`;'); 
            $db->query('CREATE TABLE IF NOT EXISTS `engine4_sespwa_themes` ( `theme_id` int(11) unsigned NOT NULL auto_increment, `name` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL, `title` varchar(255) NOT NULL, `description` text NOT NULL, `active` tinyint(4) NOT NULL default "0", PRIMARY KEY (`theme_id`), UNIQUE KEY `name` (`name`), KEY `active` (`active`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci ;'); 
            $db->query('INSERT IGNORE INTO `engine4_sespwa_themes` (`theme_id`, `name`, `title`, `description`, `active`) VALUES (1, "blue", "Blue", "", 1);'); 
            
            $db->query('DROP TABLE IF EXISTS `engine4_sespwa_banners`;'); 
            $db->query('CREATE TABLE IF NOT EXISTS `engine4_sespwa_banners` ( `banner_id` int(11) unsigned NOT NULL auto_increment, `banner_name` VARCHAR(255) NULL , `creation_date` datetime NOT NULL, `modified_date` datetime NOT NULL, `enabled` TINYINT(1) NOT NULL DEFAULT "1", PRIMARY KEY (`banner_id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;'); 
            
            $db->query('DROP TABLE IF EXISTS `engine4_sespwa_manifests`;'); 
            $db->query('CREATE TABLE IF NOT EXISTS `engine4_sespwa_manifests` ( `manifest_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, `appname` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL, `shotname` varchar(255) COLLATE utf8_unicode_ci NOT NULL, `description` text COLLATE utf8_unicode_ci, `themecolor` varchar(244) COLLATE utf8_unicode_ci NOT NULL DEFAULT "#fffff", `backgroundcolor` varchar(244) COLLATE utf8_unicode_ci NOT NULL DEFAULT "#fffff", `app_icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT "0", `icon_72_72` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT "0", `icon_96_96` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT "0", `icon_128_128` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT "0", `icon_144_144` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT "0", `icon_152_152` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT "0", `icon_192_192` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT "0", `icon_384_384` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT "0", `icon_512_512` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT "0", PRIMARY KEY (`manifest_id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;'); 
            
            $db->query('DROP TABLE IF EXISTS `engine4_sespwa_slides`;'); 
            $db->query('CREATE TABLE IF NOT EXISTS `engine4_sespwa_slides` ( `slide_id` int(11) unsigned NOT NULL auto_increment, `banner_id` int(11) DEFAULT NULL, `title` varchar(255) DEFAULT NULL, `title_button_color` varchar(255) DEFAULT NULL, `description` text, `description_button_color` varchar(255) DEFAULT NULL, `file_type` varchar(255) DEFAULT NULL, `file_id` INT(11) DEFAULT "0", `status` ENUM("1","2","3") NOT NULL DEFAULT "1", `extra_button_linkopen` TINYINT(1) NOT NULL DEFAULT "0", `extra_button` tinyint(1) DEFAULT "0", `extra_button_text` varchar(255) DEFAULT NULL, `extra_button_link` varchar(255) DEFAULT NULL, `order` tinyint(10) NOT NULL DEFAULT "0", `creation_date` datetime NOT NULL, `modified_date` datetime NOT NULL, `enabled` TINYINT(1) NOT NULL DEFAULT "1", PRIMARY KEY (`slide_id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;'); 
            
            include_once APPLICATION_PATH . "/application/modules/Sespwa/controllers/defaultsettings.php"; 
            Engine_Api::_()->getApi('settings', 'core')->setSetting('sespwa.pluginactivated', 1); 
            Engine_Api::_()->getApi('settings', 'core')->setSetting('sespwa.licensekey', $_POST['sespwa_licensekey']); 
        } 
        
        $domain_name = @base64_encode(str_replace(array('http://','https://','www.'),array('','',''),$_SERVER['HTTP_HOST']));    
        $licensekey = Engine_Api::_()->getApi('settings', 'core')->getSetting('sespwa.licensekey'); 
        $licensekey = @base64_encode($licensekey); 
            
        Engine_Api::_()->getApi('settings', 'core')->setSetting('sespwa.sesdomainauth', $domain_name); 
        Engine_Api::_()->getApi('settings', 'core')->setSetting('sespwa.seslkeyauth', $licensekey); $error = 1; 
    } 
    else { 
        $error = $this->view->translate('Please enter correct License key for this product.'); 
        $error = Zend_Registry::get('Zend_Translate')->_($error); $form->getDecorator('errors')->setOption('escape', false); $form->addError($error); $error = 0; 
                
        Engine_Api::_()->getApi('settings', 'core')->setSetting('sespwa.licensekey', $_POST['sespwa_licensekey']); 
        return; 
        $this->_helper->redirector->gotoRoute(array()); 
    } 
}