<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Fields
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Country.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Fields
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     John
 */
class Fields_Form_Element_Bank extends Engine_Form_Element_Select
{
  public function init(){
    $multiOptions = array(
      'BCA' => 'BCA',
      'BNI' => 'BNI',
      'BNI Syariah' => 'BNI Syariah',
      'BNI Syariah' => 'BNI Syariah',
      'Mandiri' => 'Mandiri',
      'Mandiri Syariah' => 'Mandiri Syariah',
      'Bank DKI' => 'Bank DKI',
      'Btpn' => 'Btpn',
      'Bank bjb' => 'Bank bjb',
      'Bank Jateng' => 'Bank Jateng',
      'Maybank' => 'Maybank',
    );

    $this->setMultiOptions($multiOptions);
  }
}
