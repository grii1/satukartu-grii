<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Fields
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: Country.php 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */

/**
 * @category   Application_Core
 * @package    Fields
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @author     John
 */
class Fields_Form_Element_Group extends Engine_Form_Element_Select
{
  public function init()
  {
    $multiOptions = array();
    // $groups = Engine_Api::_()->getDbtable('groups', 'sesgroup')->getGroups(array('is_approved ' => 1 ));
    // foreach ($groups as $group) {
    //   $multiOptions["$group->group_id"] = $group->getTitle();
    // }
    $multiOptions["10"] = "GRII";

    $this->setMultiOptions($multiOptions);
    $this->setValue(10);
  }
}
