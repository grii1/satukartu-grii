<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesmembershipcard
 * @package    Sesmembershipcard
 * @copyright  Copyright 2019-2020 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Core.php  2019-02-07 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

return array(
    array(
      'title' => 'SES - Membership Cards',
      'description' => 'This displays the Membership Cards.This widget must be placed on Member Profile Page',
      'category' => 'SES - Membership Cards Plugin',
      'type' => 'widget',
      'name' => 'sesmembershipcard.card',
      'autoEdit' => false,
      'adminForm' => array(
        'elements' => array(

        )
      )
    ),
);
