<?php
//folder name or directory name. 
$module_name = 'sesmembershipcard'; 
//product title and module title. 
$module_title = 'Membership Cards Plugin'; 

if (!$this->getRequest()->isPost()) { return; } 
if (!$form->isValid($this->getRequest()->getPost())) { return; } if ($this->getRequest()->isPost()) { 
    // $postdata = array(); 
    // //domain name 
    // $postdata['domain_name'] = $_SERVER['HTTP_HOST']; 
    // //license key 
    // $postdata['licenseKey'] = @base64_encode($_POST['sesmembershipcard_licensekey']); 
    // $postdata['module_title'] = @base64_encode($module_title); 
    // $ch = curl_init(); curl_setopt($ch, CURLOPT_URL, "https://socialnetworking.solutions/licensecheck.php"); 
    // curl_setopt($ch, CURLOPT_POST, 1); 
    // // in real life you should use something like: curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postdata)); 
    // // receive server response ... 
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); $server_output = curl_exec($ch); 
    // $error = 0; 
    // if (curl_error($ch)) { $error = 1; } 
    // curl_close($ch); 

    $server_output = "OK";
    $error = 0;
    //here we can set some variable for checking in plugin files. 
    if ($server_output == "OK" && $error != 1) { 
        if (!Engine_Api::_()->getApi('settings', 'core')->getSetting('sesmembershipcard.pluginactivated')) { 
            $db = Zend_Db_Table_Abstract::getDefaultAdapter(); 
            $db->query('INSERT IGNORE INTO `engine4_core_menuitems` (`name`, `module`, `label`, `plugin`, `params`, `menu`, `submenu`, `order`) VALUES ("sesmembershipcard_admin_main_level", "sesmembershipcard", "Member Level Settings", "", \'{"route":"admin_default","module":"sesmembershipcard","controller":"settings","action":"level"}\', "sesmembershipcard_admin_main", "", 2);'); 
            
            $db->query('DROP TABLE IF EXISTS `engine4_sesmembershipcard_settings`;'); 
            $db->query('CREATE TABLE `engine4_sesmembershipcard_settings` ( `setting_id` int(11) unsigned NOT NULL auto_increment, `member_level` int(11) unsigned NOT NULL, `profile_type` int(11) unsigned NOT NULL, `enable` tinyint(1) NOT NULL default "1", `templates_custom` tinyint(1) NOT NULL default 1 , `templates` int(11) unsigned NOT NULL, `custom_front` tinyint(1) NOT NULL default 1 , `background` tinyint(1) NOT NULL default "1", `background_image` int(11) unsigned NULL, `background_color` varchar(45) NULL, `site_title_logo` tinyint(1) NOT NULL default "1", `site_title` varchar(45) NULL, `font_color` varchar(45) NULL, `logo_image` int(11) unsigned NULL, `show_title` tinyint(1) NOT NULL default "1", `title` varchar(255) NULL, `title_color` varchar(45) NULL, `title_font` varchar(45) NULL, `profile_photo` tinyint(1) NOT NULL default "1", `profile_fields` text NULL , `empty_field` tinyint(1) NOT NULL default "1", `field_color` varchar(45) NULL, `field_font` varchar(45) NULL, `backside` tinyint(1) NOT NULL default "1", `back_templates_custom` tinyint(1) NOT NULL default "1", `back_templates` int(11) unsigned NOT NULL, `back_custom_front` tinyint(1) NOT NULL default "1", `back_background` tinyint(1) unsigned NOT NULL, `back_background_image` int(11) unsigned NULL, `back_background_color` varchar(45) NULL, `back_title_logo` tinyint(1) unsigned NOT NULL, `back_site_title` varchar(255) NULL, `back_font_color` varchar(45) NULL, `back_logo` int(11) unsigned NULL, `back_title_font` varchar(45) NULL, `back_description` varchar(255) NULL, `direction` tinyint(1) NOT NULL default "1", `creation_date` datetime NOT NULL, `modified_date` datetime NOT NULL, PRIMARY KEY (`setting_id`), KEY `member_level` (`member_level`), KEY `profile_type` (`profile_type`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci ;'); 

            Engine_Api::_()->getApi('settings', 'core')->setSetting('sesmembershipcard.pluginactivated', 1); 
            Engine_Api::_()->getApi('settings', 'core')->setSetting('sesmembershipcard.licensekey', $_POST['sesmembershipcard_licensekey']); 
        } 
        
        $domain_name = @base64_encode(str_replace(array('http://','https://','www.'),array('','',''),$_SERVER['HTTP_HOST'])); $licensekey = Engine_Api::_()->getApi('settings', 'core')->getSetting('sesmembershipcard.licensekey'); 
        $licensekey = @base64_encode($licensekey);
                 
        Engine_Api::_()->getApi('settings', 'core')->setSetting('sesmembershipcard.sesdomainauth', $domain_name);         
        Engine_Api::_()->getApi('settings', 'core')->setSetting('sesmembershipcard.seslkeyauth', $licensekey); 
        $error = 1; 
    } 
    else { 
        $error = $this->view->translate('Please enter correct License key for this product.'); 
        $error = Zend_Registry::get('Zend_Translate')->_($error); 
        $form->getDecorator('errors')->setOption('escape', false); 
        $form->addError($error); 
        $error = 0; 
                
        Engine_Api::_()->getApi('settings', 'core')->setSetting('sesmembershipcard.licensekey', $_POST['sesmembershipcard_licensekey']); 
        return; 
        $this->_helper->redirector->gotoRoute(array()); 
    } 
}