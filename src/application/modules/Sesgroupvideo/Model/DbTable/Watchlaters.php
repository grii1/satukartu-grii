<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesgroupvideo
 * @package    Sesgroupvideo
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Watchlaters.php  2018-10-16 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

class Sesgroupvideo_Model_DbTable_Watchlaters extends Engine_Db_Table {

  protected $_rowClass = "Sesgroupvideo_Model_Watchlater";
  protected $_name = 'sesgroupvideo_watchlaters';

}
