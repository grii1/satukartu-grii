<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesgroupvideo
 * @package    Sesgroupvideo
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: index.tpl  2018-10-16 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
 
 ?>
<div class="quicklinks">
  <a href="<?php echo $this->url(array('action' => 'browse'), 'sesgroupvideo_general', true); ?>"><?php echo $this->translate("Browse Videos"); ?></a>
</div>
