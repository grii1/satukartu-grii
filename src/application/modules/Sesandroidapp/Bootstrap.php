<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesandroidapp
 * @package    Sesandroidapp
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Bootstrap.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

class Sesandroidapp_Bootstrap extends Engine_Application_Bootstrap_Abstract
{
   protected function _initFrontController() {
  }
  
  public function __construct($application) {
    parent::__construct($application);
    
  }
}