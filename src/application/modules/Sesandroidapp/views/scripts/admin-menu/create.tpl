<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesandroidapp
 * @package    Sesandroidapp
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: create.tpl  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

?>
<div class='clear'>
  <div class='settings sesbasic_popup_form'>
    <?php echo $this->form->render($this); ?>
  </div>
</div>
<script type="application/javascript">

function hideFun(value){
  var display = "block" ;
  if(value == 1){
    display = "block"  
  }else{
    display = "none" 
  }  
    document.getElementById('module-wrapper').style.display = display;
    document.getElementById('file-wrapper').style.display = display;
    if(document.getElementById('url-wrapper'))
    document.getElementById('url-wrapper').style.display = display; 
}
hideFun(document.getElementById('type').value);
</script>