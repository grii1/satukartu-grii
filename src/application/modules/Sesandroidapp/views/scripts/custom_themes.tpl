<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesandroidapp
 * @package    Sesandroidapp
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: custom_themes.tpl  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

?>
<div class="sesandroidapp_styling_buttons">
<?php echo $this->htmlLink(array('route' => 'default', 'module' => 'sesandroidapp', 'controller' => 'admin-theme', 'action' => 'add-custom-theme'), $this->translate("Add New Custom Theme"), array('class' => 'smoothbox theme_button sesandroidapp_icon sesandroidapp_icon_add', 'id' => 'custom_themes')); ?>
<?php //if($this->customtheme_id): ?>
	<?php echo $this->htmlLink(array('route' => 'default', 'module' => 'sesandroidapp', 'controller' => 'admin-theme', 'action' => 'add-custom-theme'), $this->translate("Edit Custom Theme Name"), array('class' => 'seschangeThemeName sesandroidapp_icon theme_button sesandroidapp_icon_edit', 'id' => 'edit_custom_themes')); ?>
	<?php echo $this->htmlLink(array('route' => 'default', 'module' => 'sesandroidapp', 'controller' => 'admin-theme', 'action' => 'delete-custom-theme'), $this->translate("Delete Custom Theme"), array('class' => 'theme_button sesandroidapp_icon sesandroidapp_icon_delete', 'id' => 'delete_custom_themes')); ?>
	<a href="javascript:void(0);" class="theme_button sesandroidapp_icon sesandroidapp_icon_delete disabled" id="deletedisabled_custom_themes" style="display: none;"><?php echo $this->translate("Delete Custom Theme"); ?></a>
<?php //endif; ?>
</div>