<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesandroidapp
 * @package    Sesandroidapp
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: index.tpl  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

?>
<?php $settings = Engine_Api::_()->getApi('settings', 'core');
$this->headScript()->appendFile($this->layout()->staticBaseUrl . 'application/modules/Sesbasic/externals/scripts/jscolor/jscolor.js');
$this->headScript()->appendFile($this->layout()->staticBaseUrl . 'application/modules/Sesbasic/externals/scripts/jquery.min.js');
?>

<script>
hashSign = '#';
isColorFieldRequired = false;
</script>
<?php include APPLICATION_PATH .  '/application/modules/Sesandroidapp/views/scripts/dismiss_message.tpl';?>
<h2>
  <?php echo $this->translate("Native Android Mobile App") ?>
</h2>
<div class="sesbasic_nav_btns">
  <a href="<?php echo $this->url(array('module' => 'sesandroidapp', 'controller' => 'settings', 'action' => 'support'),'admin_default',true); ?>" target = "_blank" class="request-btn">Help</a>
</div>
<?php if( count($this->navigation) ): ?>
  <div class='sesbasic-admin-navgation'>
    <?php echo $this->navigation()->menu()->setContainer($this->navigation)->render() ?>
  </div>
<?php endif; ?>
<div class='clear'>
  <div class='settings sesandroidapp_themes_form' style="position:relative;">
    <?php echo $this->form->render($this); ?>
    <div class="sesbasic_loading_cont_overlay" style="display:none"></div>
  </div>
</div>
<script>
  jqueryObjectOfSes(document).ready(function(){
    changeThemeColor(jqueryObjectOfSes("input[name='theme_color']:checked").val());  
  })
  jqueryObjectOfSes(document).on('click','.seschangeThemeName',function(e){
    e.preventDefault();
     var id = jqueryObjectOfSes('#custom_theme_color').val();
     var href = jqueryObjectOfSes(this).attr('href')+'/customtheme_id/'+id;
     Smoothbox.open(href);
      parent.Smoothbox.close;
      return false;
  });
  jqueryObjectOfSes(document).on('click','#delete_custom_themes',function(e){
    e.preventDefault();
     var id = jqueryObjectOfSes('#custom_theme_color').val();
     var href = jqueryObjectOfSes(this).attr('href')+'/customtheme_id/'+id;
     Smoothbox.open(href);
      parent.Smoothbox.close;
      return false;
  })
  
  function changeCustomThemeColor(value) {
      changeThemeColor(jqueryObjectOfSes("input[name='theme_color']:checked").val());
      if(jqueryObjectOfSes("input[name='theme_color']:checked").val() == 5)
        jqueryObjectOfSes('.sesbasic_loading_cont_overlay').show();
      var URL = en4.core.staticBaseUrl+'sesandroidapp/admin-theme/getcustomthemecolors/';
      (new Request.HTML({
          method: 'post',
          'url': URL ,
          'data': {
            format: 'html',
            customtheme_id: value,
          },
          onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript) {
          var customthevalyearray = jqueryObjectOfSes.parseJSON(responseHTML);
          
          for(i=0;i<customthevalyearray.length;i++){
            var splitValue = customthevalyearray[i].split('||');
            jqueryObjectOfSes('#'+splitValue[0]).val(splitValue[1]);
            if(jqueryObjectOfSes('#'+splitValue[0]).hasClass('SEScolor')){
              if(splitValue[1] == ""){
                splitValue[1] = "#FFFFFF";  
              }
             try{
              document.getElementById(splitValue[0]).color.fromString('#'+splitValue[1]);
             }catch(err) {
               document.getElementById(splitValue[0]).value = "#FFFFFF";
             }
            }
          }
          //generate string
          jqueryObjectOfSes('.sesbasic_loading_cont_overlay').hide();
          /*var index = 0;
          var string = "";
          jqueryObjectOfSes('.global_form input').each(function(){
            if(index > 6)
            {
              var value = jqueryObjectOfSes(this).val();
              string = string + "'"+value+"',";
              //console.log(jqueryObjectOfSes(this).val(),index);
            }
            index++;
          });
          console.log(string);*/
        }
      })).send();
  }
	function changeThemeColor(value) {
    var customthemeValue = jqueryObjectOfSes('#custom_theme_color').val();
    if(customthemeValue > 6){
      jqueryObjectOfSes('#edit_custom_themes').show();
      jqueryObjectOfSes('#delete_custom_themes').show();  
    }else{
      jqueryObjectOfSes('#edit_custom_themes').hide();
      jqueryObjectOfSes('#delete_custom_themes').hide();    
    }
     if(value != 5){
      jqueryObjectOfSes('.sesandroidapp_bundle').prev().hide();
      jqueryObjectOfSes('.sesandroidapp_bundle').hide();
      jqueryObjectOfSes('#custom_theme_color-wrapper, .sesandroidapp_styling_buttons').hide();
      jqueryObjectOfSes('#submit').css('display','none');
     }else{
      jqueryObjectOfSes('.sesandroidapp_bundle').prev().show();
      jqueryObjectOfSes('.sesandroidapp_bundle').show();
      jqueryObjectOfSes('#custom_theme_color-wrapper, .sesandroidapp_styling_buttons').show();
      jqueryObjectOfSes('#submit').css('display','inline-block');  
     }    
  }
  
</script>
