<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesandroidapp
 * @package    Sesandroidapp
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: activated-custom-theme.tpl  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

?>

<p>
  This is your currently active theme. Please change the current active theme and try again to delete it.
</p>

  <button type='button'  onclick='javascript:parent.Smoothbox.close()'><?php echo $this->translate("Cancel") ?></button>