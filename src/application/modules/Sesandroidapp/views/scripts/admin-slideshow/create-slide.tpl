<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesandroidapp
 * @package    Sesandroidapp
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: create-slide.tpl  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

?>
<?php
      $this->headScript()->appendFile($this->layout()->staticBaseUrl . 'application/modules/Sesbasic/externals/scripts/jscolor/jscolor.js');
      $this->headScript()->appendFile($this->layout()->staticBaseUrl . 'application/modules/Sesbasic/externals/scripts/jquery.min.js');
?>
<?php include APPLICATION_PATH .  '/application/modules/Sesandroidapp/views/scripts/dismiss_message.tpl';?>
<h2>
  <?php echo $this->translate("Native Android Mobile App") ?>
</h2>
<div class="sesbasic_nav_btns">
  <a href="<?php echo $this->url(array('module' => 'sesandroidapp', 'controller' => 'settings', 'action' => 'support'),'admin_default',true); ?>" target = "_blank" class="request-btn">Help</a>
</div>
<?php if( count($this->navigation) ): ?>
  <div class='sesbasic-admin-navgation'>
    <?php echo $this->navigation()->menu()->setContainer($this->navigation)->render() ?>
  </div>
<?php endif; ?>
<div class="sesbasic_search_reasult">
	<?php echo $this->htmlLink(array('route' => 'admin_default', 'module' => 'sesandroidapp', 'controller' => 'slideshow', 'action' => 'index'), $this->translate("Back to Manage Photo Slides") , array('class'=>'sesbasic_icon_back buttonlink')); ?>
</div>
<div class='clear'>
  <div class='settings sesbasic_admin_form'>
    <?php echo $this->form->render($this); ?>
  </div>
</div>
<script type="application/javascript">
jqueryObjectOfSes('#type').change(function(){
  if(jqueryObjectOfSes(this).val() == 1){
    jqueryObjectOfSes('#video-wrapper').show(); 
    jqueryObjectOfSes('#title-wrapper').hide(); 
    jqueryObjectOfSes('#title_color-wrapper').hide(); 
    jqueryObjectOfSes('#description-wrapper').hide();  
    jqueryObjectOfSes('#description_color-wrapper').hide();
  }else{
    jqueryObjectOfSes('#video-wrapper').hide(); 
    jqueryObjectOfSes('#title-wrapper').show(); 
    jqueryObjectOfSes('#title_color-wrapper').show(); 
    jqueryObjectOfSes('#description-wrapper').show();  
    jqueryObjectOfSes('#description_color-wrapper').show(); 
  }
});
jqueryObjectOfSes('#type').trigger('change');
</script>
<style type="text/css">
.settings div.form-label label.required:after{
	content:" *";
	color:#f00;
}
</style>
