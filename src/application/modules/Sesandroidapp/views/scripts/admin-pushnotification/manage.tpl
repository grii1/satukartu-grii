<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesandroidapp
 * @package    Sesandroidapp
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: manage.tpl  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

?>
<?php include APPLICATION_PATH .  '/application/modules/Sesapi/views/scripts/dismiss_message.tpl';?>
<h2>
  <?php echo $this->translate('Native Android Mobile App'); ?>
</h2>
<div class="sesbasic_nav_btns">
  <a href="<?php echo $this->url(array('module' => 'sesandroidapp', 'controller' => 'settings', 'action' => 'support'),'admin_default',true); ?>" target = "_blank" class="request-btn">Help</a>
</div>
<?php if( count($this->navigation)): ?>
  <div class='sesbasic-admin-navgation'> <?php echo $this->navigation()->menu()->setContainer($this->navigation)->render(); ?> </div>
<?php endif; ?>
<?php if( count($this->subnavigation)): ?>
  <div class='sesbasic-admin-navgation'> <?php echo $this->navigation()->menu()->setContainer($this->subnavigation)->render(); ?> </div>
<?php endif; ?>
<h3><?php echo $this->translate('Manage & Send Push Notifications'); ?></h3>
<p><?php echo $this->translate('Here you can configure the push notification message and send to all subscribers of your choice. You can send new push notification by using the “Send Push Notification” link below.'); ?></p><br>
<div class="sesbasic_search_reasult">
  <?php echo $this->htmlLink(array('route' => 'admin_default', 'module' => 'sesandroidapp', 'controller' => 'pushnotification', 'action' => 'create'), $this->translate("Send Push Notifications"), array('class'=>'buttonlink sesbasic_icon_add smoothbox')); ?>
</div>
<div class="sesbasic_manage_table">
  <div class="sesbasic_manage_table_head" style="width:100%;">
    
    <div style="width:5%" class="admin_table_centered">
      <?php echo "Id";?>
    </div>
    <div style="width:20%" class="admin_table_centered">
     <?php echo $this->translate("Title") ?>
    </div>
    <div style="width:25%" class="admin_table_centered">
     <?php echo $this->translate("Description") ?>
    </div>
    <div style="width:20%" class="admin_table_centered">
     <?php echo $this->translate("Send To") ?>
    </div>
    <div style="width:10%" class="admin_table_centered">
     <?php echo $this->translate("Date") ?>
    </div>
    <div style="width:20%" class="admin_table_centered">
     <?php echo $this->translate("Options"); ?>
    </div>  
  </div>
  <ul class="sesbasic_manage_table_list" id='menu_list' style="width:100%;">
  <?php foreach ($this->noti as $item) : ?>
    <li class="item_label" id="slide_<?php echo $item->pushnotification_id; ?>" style="cursor:pointer;">
      <div style="width:5%;" class="admin_table_centered">
        <?php echo $item->pushnotification_id; ?>
      </div>
      <div style="width:20%;" class="admin_table_centered">
        <?php echo $this->string()->truncate($item->title,30); ?>
      </div>
      <div style="width:25%;" class="admin_table_centered">
        <?php echo $this->string()->truncate($item->description,30); ?>
      </div>                
      <div style="width:20%;" class="admin_table_centered">
        <?php echo $item->criteria; ?>
      </div>
      <div style="width:10%;" class="admin_table_centered">
        <?php echo date('Y-m-d H:i:s',strtotime($item->creation_date)); ?>
      </div>                                   
      <div style="width:20%;" class="admin_table_centered">          
        <?php echo $this->htmlLink(array('route' => 'admin_default', 'module' => 'sesandroidapp', 'controller' => 'pushnotification', 'action' => 'resend', 'id' => $item->pushnotification_id), $this->translate("Resend"), array('class'=>'smoothbox')) ?>
        |
        <?php echo $this->htmlLink(
          array('route' => 'admin_default', 'module' => 'sesandroidapp', 'controller' => 'pushnotification', 'action' => 'delete', 'id' => $item->pushnotification_id),
          $this->translate("Delete"),
          array('class' => 'smoothbox'));
        ?>
      </div>
    </li>
  <?php endforeach; ?>
</ul>
</div>
