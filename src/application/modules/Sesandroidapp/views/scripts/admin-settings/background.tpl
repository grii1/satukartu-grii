<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesandroidapp
 * @package    Sesandroidapp
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: background.tpl  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

?>

<?php $this->headScript()->appendFile($this->layout()->staticBaseUrl . 'application/modules/Sesbasic/externals/scripts/sesJquery.js');?>
<?php include APPLICATION_PATH .  '/application/modules/Sesandroidapp/views/scripts/dismiss_message.tpl';?>
<h2>
  <?php echo $this->translate("iOS Native Mobile App") ?>
</h2>
<div class="sesbasic_nav_btns">
  <a href="<?php echo $this->url(array('module' => 'sesandroidapp', 'controller' => 'settings', 'action' => 'support'),'admin_default',true); ?>" target = "_blank" class="request-btn">Help</a>
</div>
<?php if( count($this->navigation) ): ?>
  <div class='sesbasic-admin-navgation'>
    <?php echo $this->navigation()->menu()->setContainer($this->navigation)->render() ?>
  </div>
<?php endif; ?>
<div class="settings sesbasic_admin_form">
  <div class='settings'>
    <?php echo $this->form->render($this); ?>
  </div>
</div>
<script type="application/javascript">
sesJqueryObject('#sesandroidapp_image_video').change(function(e){
  var value = sesJqueryObject(this).val();
  if(value == 1){
    sesJqueryObject('#sesandroidapp_video_slide-wrapper').show();
    sesJqueryObject('#sesandroidapp_login_background_image-wrapper').hide();
    sesJqueryObject('#sesandroidapp_forgot_background_image-wrapper').hide();
  }else{
    sesJqueryObject('#sesandroidapp_video_slide').val('');
    sesJqueryObject('#sesandroidapp_video_slide-wrapper').hide();
    sesJqueryObject('#sesandroidapp_login_background_image-wrapper').show();
    sesJqueryObject('#sesandroidapp_forgot_background_image-wrapper').show();  
  }  
})
sesJqueryObject('#sesandroidapp_image_video').trigger('change');
</script>