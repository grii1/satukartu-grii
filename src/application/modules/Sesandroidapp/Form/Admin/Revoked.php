<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesandroidapp
 * @package    Sesandroidapp
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Revoked.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

class Sesandroidapp_Form_Admin_Revoked extends Engine_Form {
  public function init() {
    
    $this
            ->setTitle('Revoke Token')
            ->setDescription("Are you sure want to revoke token of this user. After revoking token this token user will not be able to access the app.")
            ->setMethod('POST');;
		
		$this->addElement('Hidden', 'id', array());
    $this->addElement('Button', 'submit', array(
      'label' => 'Revoke',
      'type' => 'submit',
      'ignore' => true,
      'decorators' => array('ViewHelper')
    ));
    $this->addElement('Cancel', 'cancel', array(
        'label' => 'cancel',
        'link' => true,
        'prependText' => ' or ',
        'href' => '',
        'onClick' => 'parent.Smoothbox.close()',
        'decorators' => array(
            'ViewHelper'
        )
    ));
    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
  }
}
