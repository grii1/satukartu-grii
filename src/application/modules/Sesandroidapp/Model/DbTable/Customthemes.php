<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesandroidapp
 * @package    Sesandroidapp
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Customthemes.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */


class Sesandroidapp_Model_DbTable_Customthemes extends Engine_Db_Table {
	protected $_rowClass = "Sesandroidapp_Model_Customtheme";
  
  public function getThemeKey($params = array()){
      $select = $this->select();
      if(!empty($params['theme_id']))
        $select->where('theme_id =?',$params['theme_id']);
      if(!empty($params['column_key']))
        $select->where('column_key =?',$params['column_key']);
      if(!empty($params['customtheme_id']))
        $select->where('customtheme_id =?',$params['customtheme_id']);
      if(!empty($params['is_custom']))
        $select->where('is_custom =?',$params['is_custom']);
      return $this->fetchAll($select);
  }
      
}
