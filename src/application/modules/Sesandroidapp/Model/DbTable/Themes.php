<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesandroidapp
 * @package    Sesandroidapp
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Themes.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

class Sesandroidapp_Model_DbTable_Themes extends Engine_Db_Table {  
  public function getTheme($params = array()){
      $select = $this->select();
      if(!empty($params['theme_id']))
        $select->where('theme_id =?',$params['theme_id']);      
      return $this->fetchAll($select);
  }
      
}
