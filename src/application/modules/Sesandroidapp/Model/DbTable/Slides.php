<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesandroidapp
 * @package    Sesandroidapp
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Slides.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */


class Sesandroidapp_Model_DbTable_Slides extends Engine_Db_Table {
	protected $_rowClass = "Sesandroidapp_Model_Slide";

  public function getSlides($status = false,$params=array()) {
    $tableName = $this->info('name');
    $select = $this->select();
    $select->from($tableName);
	  $select ->order('order ASC');
	  if($status)
			$select = $select->where('status = 1');
      $select->where('type !=?',1);
    if(!empty($params['fetchAll'])){
      return $this->fetchAll($select);  
    }
    return Zend_Paginator::factory($select);
  }
}
