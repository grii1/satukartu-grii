<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesandroidapp
 * @package    Sesandroidapp
 * @copyright  Copyright 2018-2019 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Pushnotifications.php  2018-08-14 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

class Sesandroidapp_Model_DbTable_Pushnotifications extends Engine_Db_Table {
	
  public function getNotifications($param = array()) {
    $select = $this->select()
                   ->from($this->info('name'));    
    $select->order("pushnotification_id DESC");
    return $this->fetchAll($select);
  }
}