<?php
/**
 * SocialEngine
 *
 * @category   Application_Core
 * @package    Core
 * @copyright  Copyright 2006-2010 Webligo Developments
 * @license    http://www.socialengine.com/license/
 * @version    $Id: index.tpl 9747 2012-07-26 02:08:08Z john $
 * @author     John
 */
?>

<?php
  $viewer = Engine_Api::_()->user()->getViewer();

  if($viewer->level_id == 1){
    echo $this->navigation()
      ->menu()
      ->setContainer($this->navigation)
      ->setUlClass('navigation')
      ->render();
  }
?>
