<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesfeedgif
 * @package    Sesfeedgif
 * @copyright  Copyright 2017-2018 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Zipupload.php  2017-12-06 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
class Sesfeedgif_Form_Admin_Image_Zipupload extends Engine_Form {

  public function init() {
  
    $this->setTitle('Upload Zipped Folder')
            ->setDescription('');
            
    //Search options
    $this->addElement('Text', 'tags',array(
      'label'=>'GIF Image Categories',
      'autocomplete' => 'off',
      'allowEmpty' => false,
      'required' => true,
      'description' => 'Enter the categories for this GIF image and separate them with commas.',
      'filters' => array(
        'StripTags',
        new Engine_Filter_Censor(),
      ),
    ));
    $this->tags->getDecorator("Description")->setOption("placement", "append");

    $this->addElement('File', 'file', array(
        'allowEmpty' => false,
        'required' => true,
        'label' => 'Zipped Folder',
        'description' => 'Choose the zipped folder of the GIF images which you want to upload on your website. [Note: folder with extension: ".zip" only.]',
    ));
    $this->file->addValidator('Extension', false, 'zip');
    
    $this->addElement('Button', 'submit', array(
      'label' => 'Save Changes',
      'type' => 'submit',
      'ignore' => true,
      'decorators' => array('ViewHelper')
    ));
    $this->addElement('Cancel', 'cancel', array(
        'label' => 'Cancel',
        'link' => true,
        'prependText' => ' or ',
        'onclick' => 'javascript:parent.Smoothbox.close()',
        'decorators' => array(
            'ViewHelper',
        ),
    ));
    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
  }
}