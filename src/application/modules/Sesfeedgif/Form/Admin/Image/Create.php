<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesfeedgif
 * @package    Sesfeedgif
 * @copyright  Copyright 2017-2018 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: Create.php  2017-12-06 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

class Sesfeedgif_Form_Admin_Image_Create extends Engine_Form {

  public function init() {
  
    $this->setTitle('Upload New GIF Image')
            ->setDescription('');
            
    $id = Zend_Controller_Front::getInstance()->getRequest()->getParam('id', 0);

    if(!$id){
      $re = true;
      $all = false;  
    }else{
      $re = false;
      $all = true;
    }
    
    //Search options
    $this->addElement('Text', 'tags',array(
      'label'=>'GIF Image Categories',
      'autocomplete' => 'off',
      'allowEmpty' => $all,
      'required' => $re,
      'description' => 'Enter the categories for this GIF image and separate them with commas.',
      'filters' => array(
        'StripTags',
        new Engine_Filter_Censor(),
      ),
    ));
    $this->tags->getDecorator("Description")->setOption("placement", "append");
    
    if(empty($id)) {
      $this->addElement('File', 'file', array(
          'allowEmpty' => $all,
          'required' => $re,
          'label' => 'GIF Image',
          'description' => 'Choose the GIF image you want to upload on your website.',
      ));
      $this->file->addValidator('Extension', false, 'gif, GIF');
    }
    $this->addElement('Button', 'submit', array(
      'label' => 'Upload',
      'type' => 'submit',
      'ignore' => true,
      'decorators' => array('ViewHelper')
    ));
    $this->addElement('Cancel', 'cancel', array(
        'label' => 'Cancel',
        'link' => true,
        'prependText' => ' or ',
        'onclick' => 'javascript:parent.Smoothbox.close()',
        'decorators' => array(
            'ViewHelper',
        ),
    ));
    $this->addDisplayGroup(array('submit', 'cancel'), 'buttons');
  }
}