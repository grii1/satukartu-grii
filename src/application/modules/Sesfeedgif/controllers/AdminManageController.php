<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesfeedgif
 * @package    Sesfeedgif
 * @copyright  Copyright 2017-2018 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: AdminManageController.php  2017-12-06 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */

class Sesfeedgif_AdminManageController extends Core_Controller_Action_Admin {

  public function indexAction() {

    if ($this->getRequest()->isPost()) {
      $values = $this->getRequest()->getPost();
      foreach ($values as $key => $value) {
        if ($key == 'delete_' . $value) {
          $image = Engine_Api::_()->getItem('sesfeedgif_image', $value);
          if($image)
          $image->delete();
        }
      }
    }

    $this->view->formFilter = $formFilter = new Sesfeedgif_Form_Admin_Filter();

    $this->view->navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('sesadvancedactivity_admin_main', array(), 'sesfeedgif_admin_main_fegifsettings');

    $this->view->subnavigation = $navigation = Engine_Api::_()->getApi('menus', 'core')
      ->getNavigation('sesfeedgif_admin_main', array(), 'sesfeedgif_admin_main_feedgif');

    $values = array();
    if ($formFilter->isValid($this->_getAllParams()))
      $values = $formFilter->getValues();

    $this->view->assign($values);

    $imagesTable = Engine_Api::_()->getDbTable('images','sesfeedgif');
    $rName = $imagesTable->info('name');
    $tmTable = Engine_Api::_()->getDbtable('TagMaps', 'core');
    $tmName = $tmTable->info('name');
    $tagName = Engine_Api::_()->getDbtable('Tags', 'core')->info('name');

    $select = $imagesTable->select()
            ->from($rName)
            ->setIntegrityCheck(false)
            ->joinLeft( $tmName, "$tmName.resource_id = $rName.image_id and " . $tmName . ".resource_type = 'sesfeedgif_image'", null )
            ->joinLeft( $tagName, "$tagName.tag_id = $tmName.tag_id", array($tagName.".text") );
            if(isset($_GET['name']))
              $select->where( $tagName . ".text LIKE ? ",'%'.$_GET['name'].'%' );
            $select->where( $rName . ".file_id <> ? ", 0 )
            ->group($rName . '.image_id');

    if (isset($_GET['user_count']) && $_GET['user_count'] != '')
      $select->order($rName . '.user_count DESC');
    else
      $select->order($rName. '.image_id DESC' );

    $this->view->paginator = Zend_Paginator::factory($select);
    $this->view->paginator->setItemCountPerPage(16);
    $this->view->paginator->setCurrentPageNumber($this->_getParam('page',1));
  }

  public function enabledAction() {

    $id = $this->_getParam('id');
    if (!empty($id)) {
      $item = Engine_Api::_()->getItem('sesfeedgif_image', $id);
      $item->enabled = !$item->enabled;
      $item->save();
    }
    $this->_redirect('admin/sesfeedgif/manage');
  }

  public function orderAction() {

    if (!$this->getRequest()->isPost())
      return;

    $imagesTable = Engine_Api::_()->getDbtable('images', 'sesfeedgif');
    $images = $imagesTable->fetchAll($imagesTable->select());
    foreach ($images as $image) {
      $order = $this->getRequest()->getParam('manageimages_' . $image->image_id);
      if (!$order)
        $order = 999;
      $image->order = $order;
      $image->save();
    }
    return;
  }

  public function uploadGiphyGifAction() {

    $this->view->navigation = Engine_Api::_()->getApi('menus', 'core')->getNavigation('sesfeedgif_admin_main', array(), 'sesfeedgif_admin_main_feedgif');

    $this->view->form = $form = new Sesfeedgif_Form_Admin_Image_Uploadgiphy();

    // Check if post
    if( !$this->getRequest()->isPost() ) {
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_('Not post');
      return;
    }

    if(!$form->isValid($this->getRequest()->getPost()) && !$id) {
      $this->view->status = false;
      $this->view->error =  Zend_Registry::get('Zend_Translate')->_('Invalid data');
      return;
    }

    $values = $form->getValues();
    $viewer = Engine_Api::_()->user()->getViewer();
    $db = Engine_Db_Table::getDefaultAdapter();
    $db->beginTransaction();
    // If we're here, we're done
    $this->view->status = true;

    $getValue = Engine_Api::_()->getDbTable('texts', 'sesfeedgif')->getValue($values['searchtext']);
    if(empty($getValue)) {
      $offset = 1;
    } else {
      $offset = $getValue;
    }
    $dbGetInsert = Engine_Db_Table::getDefaultAdapter();
    $imagesTable = Engine_Api::_()->getDbtable('images', 'sesfeedgif');
    try {

      $responses = $this->giphyApi($values['searchtext'], $offset);
      ini_set('memory_limit', '1024M');
      $data = array();

//       if($getValue) {
//         $getValue = $getValue + 15;
//         $db->query('UPDATE `engine4_sesfeedgif_texts` SET `limit` = "'.$getValue.'" WHERE `engine4_sesfeedgif_texts`.`text` = "'.$values['searchtext'].'";');

      if(empty($getValue)) {
        $dbGetInsert->query('INSERT IGNORE INTO `engine4_sesfeedgif_texts` (`limit`, `text`) VALUES ("1", "'.$values['searchtext'].'");');
      }

      foreach($responses as $response) {
        if(empty($response->images->original)) continue;
        $code = $response->id;
        $getCode = Engine_Api::_()->getDbTable('images', 'sesfeedgif')->getCode($code);
        if($getCode) {
          continue;
        } else {
          $url = 'https://media.giphy.com/media/'.$code.'/giphy.gif';
          $item = $imagesTable->createRow();
          $item->setFromArray($values);
          $item->save();
          $item->order = $item->image_id;
          $item->save();
          //Add tags work
          $tags = preg_split('/[,]+/', $values['tags']);
          $item->tags()->addTagMaps($viewer, $tags);
          $item->gifimage_code = $code;
          $item->save();
          if($url) {
            $file_id = $this->setPhoto($url, $item->image_id);
            $item->file_id = $file_id;
            $item->save();
          }
          $db->commit();

          $dbGetInsert->query('UPDATE `engine4_sesfeedgif_texts` SET `limit` = "'.$offset.'" WHERE `engine4_sesfeedgif_texts`.`text` = "'.$values['searchtext'].'";');
          $offset++;
        }
      }
      $this->_forward('success', 'utility', 'core', array(
        'smoothboxClose' => 10,
        'parentRefresh'=> 10,
        'messages' => array('Feed GIF Image Uploaded Successfully.')
      ));
    } catch(Exception $e) {
//       $db->rollBack();
//       throw $e;
      $this->_forward('success', 'utility', 'core', array(
        'smoothboxClose' => 10,
        'parentRefresh'=> 10,
        'messages' => array('Feed GIF Image Uploaded Successfully.')
      ));
    }
  }

  public function giphyApi($searchtext, $offset) {

    $giphyApi = Engine_Api::_()->getApi('settings', 'core')->getSetting('sesfeedgif.giphyapi', '');
    $url = "http://api.giphy.com/v1/gifs/search?q=".urlencode($searchtext)."&api_key=".$giphyApi."&limit=15&offset=".$offset;
    $videoResponse = json_decode(file_get_contents($url));
    return $videoResponse->data;
  }

  public function createonegiphyAction() {

    $this->view->form = $form = new Sesfeedgif_Form_Admin_Image_Createonegiphy();

    // Check if post
    if( !$this->getRequest()->isPost() ) {
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_('Not post');
      return;
    }

    if(!$form->isValid($this->getRequest()->getPost()) && !$id) {
      $this->view->status = false;
      $this->view->error =  Zend_Registry::get('Zend_Translate')->_('Invalid data');
      return;
    }

    $values = $form->getValues();
    if($values['url']) {
      preg_match('~https?://(?|media\.giphy\.com/media/([^ /]+)/giphy\.gif|i\.giphy\.com/([^ /]+)\.gif|giphy\.com/gifs/(?:.*-)?([^ /]+))~i', $values['url'], $matches);
      if($matches[1]) {
        $getCode = Engine_Api::_()->getDbTable('images', 'sesfeedgif')->getCode($matches[1]);
        if($getCode) {
          $error = Zend_Registry::get('Zend_Translate')->_("This image is already exist. So, you can enter different url.");
          $form->getDecorator('errors')->setOption('escape', false);
          $form->addError($error);
          return;
        }
      }
    }

    $viewer = Engine_Api::_()->user()->getViewer();
    $db = Engine_Db_Table::getDefaultAdapter();
    $db->beginTransaction();
    // If we're here, we're done
    $this->view->status = true;

    $giphyapi = Engine_Api::_()->getApi('settings', 'core')->getSetting('sesfeedgif.giphyapi', '');
    try {

      $imagesTable = Engine_Api::_()->getDbtable('images', 'sesfeedgif');

      $item = $imagesTable->createRow();
      $item->setFromArray($values);
      $item->save();
      $item->order = $item->image_id;
      $item->save();

      //Add tags work
      $tags = preg_split('/[,]+/', $values['tags']);
      $item->tags()->addTagMaps($viewer, $tags);

      if($values['url']) {

        preg_match('~https?://(?|media\.giphy\.com/media/([^ /]+)/giphy\.gif|i\.giphy\.com/([^ /]+)\.gif|giphy\.com/gifs/(?:.*-)?([^ /]+))~i', $values['url'], $matches);
        if($matches[1]) {
          $item->gifimage_code = $matches[1];
          $item->save();
        }

        $file_id = $this->setPhoto($values['url'], $item->image_id);
        $item->file_id = $file_id;
        $item->save();
      }
      $db->commit();
    } catch(Exception $e) {
      $db->rollBack();
      throw $e;
    }

    $this->_forward('success', 'utility', 'core', array(
      'smoothboxClose' => 10,
      'parentRefresh'=> 10,
      'messages' => array('Feed Image Image Uploaded Successfully.')
    ));
  }

  protected function setPhoto($photo,$id = 0) {

    if (is_string($photo)) {
      $file = $photo;
      $fileName = $photo;
    } else {
      throw new User_Model_Exception('invalid argument passed to setPhoto');
    }
    if (!$fileName) {
      $fileName = $file;
    }

    $name = basename($file);
    $extension = ltrim(strrchr($fileName, '.'), '.');
    $base = rtrim(substr(basename($fileName), 0, strrpos(basename($fileName), '.')), '.');
    $path = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'temporary';
    $params = array(
        'parent_type' => 'video',
        'parent_id' => $id,
        'user_id' => Engine_Api::_()->user()->getViewer()->getIdentity(),
        'name' => $fileName,
    );
    // Save
    $filesTable = Engine_Api::_()->getDbtable('files', 'storage');
    $mainPath = $path . DIRECTORY_SEPARATOR . $base . '_main.' . $extension;
    $image = Engine_Image::factory();
    $image->open($file)
            ->resize(500, 500)
            ->write($mainPath)
            ->destroy();
    // Store
    try {
      $iMain = $filesTable->createFile($mainPath, $params);
    } catch (Exception $e) {
      // Remove temp files
      @unlink($mainPath);
      // Throw
      if ($e->getCode() == Storage_Model_DbTable_Files::SPACE_LIMIT_REACHED_CODE) {
        throw new Sesvideoimporter_Model_Exception($e->getMessage(), $e->getCode());
      } else {
        throw $e;
      }
    }
    // Remove temp files
    @unlink($mainPath);
    // Update row
    return $iMain->file_id;
  }

  public function createAction() {

    $id = $this->_getParam('id',false);

    $this->view->form = $form = new Sesfeedgif_Form_Admin_Image_Create();
    if($id){
      $item = Engine_Api::_()->getItem('sesfeedgif_image',$id);
      $form->populate($item->toArray());

      //Tags Work
      $tagStr = '';
      foreach( $item->tags()->getTagMaps() as $tagMap ) {
        $tag = $tagMap->getTag();
        if( !isset($tag->text) ) continue;
        if( '' !== $tagStr ) $tagStr .= ', ';
        $tagStr .= $tag->text;
      }
      $form->populate(array(
        'tags' => $tagStr,
      ));
      $this->view->tagNamePrepared = $tagStr;

      $form->setTitle('Edit this GIF Image');
      $form->submit->setLabel('Save Changes');
    }

    // Check if post
    if( !$this->getRequest()->isPost() ) {
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_('Not post');
      return;
    }

    if(!$form->isValid($this->getRequest()->getPost()) && !$id) {
      $this->view->status = false;
      $this->view->error =  Zend_Registry::get('Zend_Translate')->_('Invalid data');
      return;
    }
    $viewer = Engine_Api::_()->user()->getViewer();
    $values = $form->getValues();
    $db = Engine_Db_Table::getDefaultAdapter();
    $db->beginTransaction();
    // If we're here, we're done
    $this->view->status = true;
    try {

      $imagesTable = Engine_Api::_()->getDbtable('images', 'sesfeedgif');

      unset($values['file']);
      if(empty($id))
        $item = $imagesTable->createRow();
      $item->setFromArray($values);
      $item->save();
      $item->order = $item->image_id;
      $item->save();
      if(empty($id)) {
        //Add tags work
        $tags = preg_split('/[,]+/', $values['tags']);
        $item->tags()->addTagMaps($viewer, $tags);
      } else {
        // handle tags
        $tags = preg_split('/[,]+/', $values['tags']);
        $item->tags()->setTagMaps($viewer, $tags);
      }

      if(!empty($_FILES['file']['name'])) {
        $file_ext = pathinfo($_FILES['file']['name']);
        $file_ext = $file_ext['extension'];
        $storage = Engine_Api::_()->getItemTable('storage_file');
        $storageObject = $storage->createFile($form->file, array(
          'parent_id' => $item->getIdentity(),
          'parent_type' => $item->getType(),
          'user_id' => Engine_Api::_()->user()->getViewer()->getIdentity(),
        ));
        // Remove temporary file
        @unlink($file['tmp_name']);
        $item->file_id = $storageObject->file_id;
        $item->save();
      }

      $db->commit();
    } catch(Exception $e) {
      $db->rollBack();
      throw $e;
    }

    $this->_forward('success', 'utility', 'core', array(
      'smoothboxClose' => 10,
      'parentRefresh'=> 10,
      'messages' => array('Feed Image Image Uploaded Successfully.')
    ));
  }

  public function deleteAction() {

    // In smoothbox
    $this->_helper->layout->setLayout('admin-simple');
    $this->view->form = $form = new Sesbasic_Form_Admin_Delete();

    $form->setTitle('Delete This GIF Image');
    $form->setDescription('Are you sure that you want to delete this gif image? It will not be recoverable after being deleted.');

    $form->submit->setLabel('Delete');
    $id = $this->_getParam('id');
    $this->view->item_id = $id;
    // Check post
    if ($this->getRequest()->isPost()) {
      $image = Engine_Api::_()->getItem('sesfeedgif_image', $id);
      $image->delete();
      $this->_forward('success', 'utility', 'core', array(
          'smoothboxClose' => 10,
          'parentRefresh' => 10,
          'messages' => array('Feed Image Image Deleteed Successfully.')
      ));
    }
  }

  public function uploadZipFileAction(){

    $this->view->form = $form = new Sesfeedgif_Form_Admin_Image_Zipupload();

    // Check if post
    if( !$this->getRequest()->isPost() ) {
      $this->view->status = false;
      $this->view->error = Zend_Registry::get('Zend_Translate')->_('Not post');
      return;
    }

    if( !$form->isValid($this->getRequest()->getPost()) ) {
      $this->view->status = false;
      $this->view->error =  Zend_Registry::get('Zend_Translate')->_('Invalid data');
      return;
    }

    if(!empty($_FILES["file"]["name"])) {

      $file = $_FILES["file"];
      $filename = $file["name"];
      $tmp_name = $file["tmp_name"];
      $type = $file["type"];

      $name = explode(".", $filename);
      $accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');

      if(in_array($type,$accepted_types)) { //If it is Zipped/compressed File
        $okay = true;
      }

      $continue = strtolower($name[1]) == 'zip' ? true : false; //Checking the file Extension

      if(!$continue) {
        $form->addError("The file you are trying to upload is not a .zip file. Please try again.");
        return;
      }


      /* here it is really happening */
      $ran = $name[0]."-".time()."-".rand(1,time());
      $dir = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'temporary/';
      $targetdir = $dir.$ran;
      $targetzip = $dir.$ran.".zip";

      if(move_uploaded_file($tmp_name, $targetzip)) { //Uploading the Zip File
        /* Extracting Zip File */
        $zip = new ZipArchive();
        $x = $zip->open($targetzip);  // open the zip file to extract
        if ($x === true) {
            $zip->extractTo($targetdir); // place in the directory with same name
            $zip->close();

            @unlink($targetzip); //Deleting the Zipped file
            // Get subdirectories
            chmod($targetdir, 0777) ;
            $directories = glob($targetdir.'*', GLOB_ONLYDIR);
            if ($directories !== FALSE) {
              $db = Engine_Db_Table::getDefaultAdapter();
              $db->beginTransaction();
              // If we're here, we're done
              $this->view->status = true;
              try {
                foreach($directories as $directory) {
                  $path = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory));
                    foreach ($path as $file) {
                      if (!$file->isFile())
                        continue;
                      $base_name = basename($file->getFilename());
                      if (!($pos = strrpos($base_name, '.')))
                        continue;
                      $extension = strtolower(ltrim(substr($base_name, $pos), '.'));
                      if (!in_array($extension, array('gif', 'GIF')))
                        continue;
                      $this->uploadZipFile($file->getPathname(), $_POST['tags']);
                  }
                }
                $db->commit();
                $this->rrmdir($targetdir);
               } catch(Exception $e) {
                  $db->rollBack();
                  throw $e;
               }
            }
        }
      } else {
        $form->addError("There was a problem with the upload. Please try again.");
        return;
      }
    }
    $this->_forward('success', 'utility', 'core', array(
      'smoothboxClose' => 10,
      'parentRefresh'=> 10,
      'messages' => array('Zip images uploaded Successfully.')
    ));
  }

  private function uploadZipFile($file = '', $tags = '') {

    $imagesTable = Engine_Api::_()->getDbtable('images', 'sesfeedgif');
    $viewer = Engine_Api::_()->user()->getViewer();
    $item = $imagesTable->createRow();
    $values['enabled'] = 1;
    $item->setFromArray($values);
    $item->save();
    $item->order = $item->image_id;
    $item->save();

    if($tags) {
      //Add tags work
      $tags = preg_split('/[,]+/', $tags);
      $item->tags()->addTagMaps($viewer, $tags);
    }

    if(!empty($file)) {
      $file_ext = pathinfo($file);
      $file_ext = $file_ext['extension'];
      $storage = Engine_Api::_()->getItemTable('storage_file');
      $fileUpload = array('name'=>basename($file),'tmp_name' => $file,'size'=>filesize($file),'error'=>0);
      $storageObject = $storage->createFile($file, array(
        'parent_id' => $item->getIdentity(),
        'parent_type' => $item->getType(),
        'user_id' => Engine_Api::_()->user()->getViewer()->getIdentity(),
      ));
      // Remove temporary file
      @unlink($file['tmp_name']);
      $item->file_id = $storageObject->file_id;
      $item->save();
    }
  }

  private function rrmdir($src) {
    $dir = opendir($src);
    while(false !== ( $file = readdir($dir)) ) {
        if (( $file != '.' ) && ( $file != '..' )) {
            $full = $src . '/' . $file;
            if ( is_dir($full) ) {
                $this->rrmdir($full);
            }
            else {
                unlink($full);
            }
        }
    }
    closedir($dir);
    rmdir($src);
  }
}
