<?php
/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesalbum
 * @package    Sesalbum
 * @copyright  Copyright 2015-2016 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: index.tpl 2015-06-16 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
?>
<?php include APPLICATION_PATH .  '/application/modules/Sesalbum/views/scripts/dismiss_message.tpl';
$this->headScript()->prependFile($this->layout()->staticBaseUrl . 'application/modules/Sesbasic/externals/scripts/sesJquery.js');
?>
<h2>
  <?php echo $this->translate("Advanced Photos & Albums Plugin") ?>
</h2>
<div class="sesbasic_nav_btns">
  <a href="<?php echo $this->url(array('module' => 'sesalbum', 'controller' => 'settings', 'action' => 'help'),'admin_default',true); ?>" class="request-btn">Help</a>
</div>
<?php if( count($this->navigation) ): ?>
  <div class='sesbasic-admin-navgation'>
    <?php echo $this->navigation()->menu()->setContainer($this->navigation)->render() ?>
  </div>
<?php endif; ?>
<div class="settings sesbasic_admin_form">
  <div class='settings'>
    <?php echo $this->form->render($this); ?>
  </div>
</div>
<script type="application/javascript">
$('dummy-label').remove();
document.getElementById('dummy-element').style.fontSize = '14px';
document.getElementById('dummy-element').style.fontWeight = 'bold';
</script>

<script>
window.addEvent('domready',function() {
enablesessocialshare(<?php echo Engine_Api::_()->getApi('settings', 'core')->getSetting('sesalbum.enablesessocialshare', 1); ?>);
});
function enableShare(value) {

  if(value == 1) {
    var enableShareval = sesJqueryObject('input[name=sesalbum_enablesocialshare]:checked').val();
    var enablesessocialshareval = sesJqueryObject('input[name=sesalbum_enablesessocialshare]:checked').val();
    sesJqueryObject('input[name="sesalbum_enablesessocialshare"]').prop('checked',true);
  }
}

function enablesessocialshare(value) {

if(value == 1) {
  var enableShareval = sesJqueryObject('input[name=sesalbum_enablesocialshare]:checked').val();
  var enablesessocialshareval = sesJqueryObject('input[name=sesalbum_enablesessocialshare]:checked').val();
  sesJqueryObject('input[name="sesalbum_enablesocialshare"]').prop('checked',true);
  $('sesalbum_enableplusicon-wrapper').style.display = 'block';
  $('sesalbum_iconlimit-wrapper').style.display = 'block';
} else {
  $('sesalbum_enableplusicon-wrapper').style.display = 'none';
  $('sesalbum_iconlimit-wrapper').style.display = 'none';
}

}

</script>