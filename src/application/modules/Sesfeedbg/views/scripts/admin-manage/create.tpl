<?php

/**
 * SocialEngineSolutions
 *
 * @category   Application_Sesfeedbg
 * @package    Sesfeedbg
 * @copyright  Copyright 2017-2018 SocialEngineSolutions
 * @license    http://www.socialenginesolutions.com/license/
 * @version    $Id: create.tpl  2017-08-28 00:00:00 SocialEngineSolutions $
 * @author     SocialEngineSolutions
 */
?>
<script type="text/javascript">
  var cal_starttime_onHideStart = function(){
    // check end date and make it the same date if it's too
    cal_endtime.calendars[0].start = new Date( $('starttime-date').value );
    // redraw calendar
    cal_endtime.navigate(cal_endtime.calendars[0], 'm', 1);
    cal_endtime.navigate(cal_endtime.calendars[0], 'm', -1);
  }
  var cal_endtime_onHideStart = function(){
    // check start date and make it the same date if it's too
    cal_starttime.calendars[0].end = new Date( $('endtime-date').value );
    // redraw calendar
    cal_starttime.navigate(cal_starttime.calendars[0], 'm', 1);
    cal_starttime.navigate(cal_starttime.calendars[0], 'm', -1);
  }

</script>
<div class='settings sesbasic_popup_form'>
  <?php echo $this->form->render($this); ?>
</div>

<script type="text/javascript">
  
  <?php if(empty($this->enableenddate)): ?>
    window.addEvent('domready',function() {
      $('endtime-wrapper').style.display = 'none';
    });
  <?php endif; ?>

  var enableenddatse = function(value){
    if(value == 1) {
      $('endtime-wrapper').style.display = 'block';
    } else {
      $('endtime-wrapper').style.display = 'none';
    }
  }


  $('starttime-hour').hide();
  $('starttime-minute').hide();
  $('starttime-ampm').hide();
  $('endtime-hour').hide();
  $('endtime-minute').hide();
  $('endtime-ampm').hide();  
</script>