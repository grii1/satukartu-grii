<?php
/**
 * SocialEnginePro
 *
 * @category   Application_Extensions
 * @package    money
 * @author     Azim
 */

/**
 * @category   Application_Extensions
 * @package    money
 */
class Engine_Service_LiqPay extends Zend_Service_Abstract
{
 
  protected $_idMobile;

  protected $_idCard;

  protected $_signature;

  protected $_certificate;

  protected $_testMode;

  protected $_version = '1.2';
  
  
  
  

  protected $_log;
  
  protected $_payWay;
 
 
  public function __construct(array $options)
  {
    $this->setOptions($options);
    
    // Force the curl adapter if it's available
    if( extension_loaded('curl') ) {
      $adapter = new Zend_Http_Client_Adapter_Curl();
      $adapter->setCurlOption(CURLOPT_SSL_VERIFYPEER, false);
      $adapter->setCurlOption(CURLOPT_SSL_VERIFYHOST, false);
      $adapter->setCurlOption(CURLOPT_SSLVERSION, 3);
      //$adapter->setCurlOption(CURLOPT_VERBOSE, false);
      $this->getHttpClient()->setAdapter($adapter);
    }
    $this->getHttpClient()->setConfig(array('timeout' => 15));
  }

  public function setOptions(array $options)
  {
    foreach( $options as $key => $value ) {
      $property = '_' . $key;
      if( property_exists($this, $property) ) {
        $this->$property = $value;
      }
    }
    // Check options
    if( empty($this->_idMobile) || empty($this->_idCard) ||
        (empty($this->_signature)  ) ) {
      throw new Engine_Service_LiqPay_Exception('Not all connection ' .
          'options were specified.', 'MISSING_LOGIN');
      throw new Zend_Service_Exception('Not all connection options were specified.');
    }
  }

  public function createXml($params = array()){
      $xml='<request>' .
            '<version>'.$this->_version.'</version>' .
            '<merchant_id>' . $this->_idCard . '</merchant_id>' .
            '<result_url>' . $params['user_back_url'] . '</result_url>' .
            '<server_url>' . $params['RETURNURL'] . '</server_url>' .
            '<order_id>' . $params['order_id'] . '</order_id>' .
            '<amount>' . $params['amount'] . '</amount>' .
            '<currency>' . $params['CURRENCYCODE'] . '</currency>' .
            '<description>' . $params['title'] . '</description>' .
            '<default_phone></default_phone>' .
            '<pay_way>' . $this->_payWay . '</pay_way>' . 
            '<exp_time></exp_time>' .
            '</request>';
      
      return $xml;
  }
  
  
  
  public function getOperationXml($params = array()){
      $xml = $this->createXml($params);
       $operation_xml = base64_encode($xml);
       return $operation_xml;
  }

  public function getSignature($params = array()){
      $xml = $this->createXml($params);
      
      $signature = base64_encode(sha1($this->_signature . $xml . $this->_signature, 1));
      
      return $signature;
  }
  
  public function validateSignature($params = array()){
       $signature = base64_encode(sha1($this->_signature. base64_decode($params['operation_xml']) .$this->_signature,1)); 
       
       if($signature == $params['signature']){
           return true;
       }
       else{
           return false;
       }
  }




 
  /**
   * Check params
   *
   * @param array $params
   * @param array $requiredParams
   * @param array $supportedParams
   * @return array
   */
  protected function _checkParams(array $params, $requiredParams = null,
      $supportedParams = null)
  {
    // Check params
    if( !is_array($params) ) {
      if( !empty($params) ) {
        throw new Engine_Service_PayPal_Exception('Invalid data type',
            'UNKNOWN_PARAM');
      } else {
        $params = array();
      }
    }

    // Check required params
    if( is_string($requiredParams) ) {
      $requiredParams = array($requiredParams);
    } else if( null === $requiredParams ) {
      $requiredParams = array();
    }

    // Check supported params
    if( is_string($supportedParams) ) {
      $supportedParams = array($supportedParams);
    } else if( null === $supportedParams ) {
      $supportedParams = array();
    }

    // Nothing to do
    if( empty($requiredParams) && empty($supportedParams) &&
        is_array($requiredParams) && is_array($supportedParams) ) {
      return array();
    }

    // Build full supported
    if( is_array($supportedParams) && is_array($requiredParams) ) {
      $supportedParams = array_unique(array_merge($supportedParams, $requiredParams));
    }
    
    // Run strtoupper on all keys?
    $params = array_combine(array_map('strtoupper', array_keys($params)), array_values($params));

    // Init
    $processedParams = array();
    $foundKeys = array();

    // Process out simple params
    $processedParams = array_merge($processedParams,
        array_intersect_key($params, array_flip($supportedParams)));
    $params = array_diff_key($params, array_flip($supportedParams));
    $foundKeys = array_merge($foundKeys, array_keys($processedParams));

    // Process out complex params
    foreach( $supportedParams as $supportedFormat ) {
      foreach( $params as $key => $value ) {
        if( count($parts = sscanf($key, $supportedFormat)) > 0 ) {
          $foundKeys[] = $supportedFormat;
          $processedParams[$key] = $value;
        }
      }
    }

    // Remove complex params
    $params = array_diff_key($params, $processedParams);

    // Anything left is an unsupported param
    if( !empty($params) ) {
      $paramStr = '';
      foreach( $params as $unsupportedParam ) {
        if( $paramStr != '' ) $paramStr .= ', ';
        $paramStr .= $unsupportedParam;
      }
      //trigger_error(sprintf('Unknown param(s): %1$s', $paramStr), E_USER_NOTICE);
      throw new Engine_Service_PayPal_Exception(sprintf('Unknown param(s): ' .
          '%1$s', $paramStr), 'UNKNOWN_PARAM');
    }

    // Let's check required against foundKeys
    if( count($missingRequiredParams = array_diff_key($requiredParams, $foundKeys)) > 0 ) {
      $paramStr = '';
      foreach( $missingRequiredParams as $missingRequiredParam ) {
        if( $paramStr != '' ) $paramStr .= ', ';
        $paramStr .= $missingRequiredParam;
      }
      throw new Engine_Service_PayPal_Exception(sprintf('Missing required ' .
          'param(s): %1$s', $paramStr), 'MISSING_REQUIRED');
    }
    
    return $processedParams;
  }

  
  
}
