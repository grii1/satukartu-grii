<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<!--<![endif]-->
<!--<![endif]-->
<html lang="en">
<head>
<title>GRII - Gereja Reformed Injili Indonesia</title>
<meta charset="utf-8">
<!-- Meta -->
<meta name="keywords" content="" />
<meta name="author" content="">
<meta name="robots" content="" />
<meta name="description" content="" />

<!-- this styles only adds some repairs on idevices  -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon.ico">

<!-- Google fonts - witch you want to use - (rest you can just remove) -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Stylesheets -->
<link rel="stylesheet" media="screen" href="js/bootstrap/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="js/mainmenu/menu.css" type="text/css" />
<link rel="stylesheet" href="css/default.css" type="text/css" />
<link rel="stylesheet" href="css/layouts.css" type="text/css" />
<link rel="stylesheet" href="css/shortcodes.css" type="text/css" />
<link rel="stylesheet" href="css/timeline.css" type="text/css" />
<link rel="stylesheet" href="css/calendar.css" type="text/css" />
<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />
<link rel="stylesheet" href="js/masterslider/style/masterslider.css" />
<link rel="stylesheet" type="text/css" href="js/cubeportfolio/cubeportfolio.min.css">
<link rel="stylesheet" type="text/css" href="css/Simple-Line-Icons-Webfont/simple-line-icons.css" media="screen" />
<link rel="stylesheet" href="css/et-line-font/et-line-font.css">
<link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
<link rel="stylesheet" href="js/jFlickrFeed/style.css" />



<!-- Remove the below comments to use your color option -->
<!--<link rel="stylesheet" href="css/colors/lightblue.css" />-->
<!--<link rel="stylesheet" href="css/colors/orange.css" />-->
<!--<link rel="stylesheet" href="css/colors/green.css" />-->
<!--<link rel="stylesheet" href="css/colors/pink.css" />-->
<!--<link rel="stylesheet" href="css/colors/red.css" />-->
<!--<link rel="stylesheet" href="css/colors/purple.css" />-->
<!--<link rel="stylesheet" href="css/colors/bridge.css" />-->
<!--<link rel="stylesheet" href="css/colors/yellow.css" />-->
<!--<link rel="stylesheet" href="css/colors/violet.css" />-->
<link rel="stylesheet" href="css/colors/brown.css" />
<!--<link rel="stylesheet" href="css/colors/mossgreen.css" />-->

</head>

<body>
<div class="site_wrapper">
  <!--
  <div class="topbar light topbar-padding">
    <div class="container">
      <div class="topbar-left-items">
        <ul class="toplist toppadding pull-left paddtop1">
          <li class="rightl">Call us</li>
          <li>(888) 123-4567</li>
        </ul>
      </div>


      <div class="topbar-right-items pull-right">
        <ul class="toplist toppadding">
          <li><a href="https://www.facebook.com/codelayers"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/codelayers"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
          <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
          <li><a href="#" class=" btn btn-red-4 less-top-bottom-padding btn-round btn-small-2">Donate</a></li>
        </ul>
      </div>
    </div>
  </div>-->
  <div class="clearfix"></div>

  <div id="header">
    <div class="container">
      <div class="navbar brown navbar-default yamm">
        <div class="navbar-header">
          <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid" class="navbar-toggle two three"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          <a href="index.php" class="navbar-brand less-top-padding"><img src="images/logo.png" alt=""/></a> </div>
        <div id="navbar-collapse-grid" class="navbar-collapse collapse pull-right">
          <ul class="nav brown navbar-nav">
            <li><a href="index.php" class="dropdown-toggle">Event</a></li>
            <li><a href="news.php" class="dropdown-toggle">Past Event</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!--end menu-->
  <div class="clearfix"></div>
  <section>
    <div class="container-fluid">
      <div class="row">
        <div class="demo-full-width">
          <div id="grid-container" class="cbp">
            <div class="cbp-item identity logos"> <a href="images/image_1.jpg" target="_blank" class="cbp-caption cbp-lightbox" data-title="KU Mand Livestream Minggu 7:30 WIB">
              <div class="cbp-caption-defaultWrap"> <img src="images/image_1.jpg" alt=""> </div>
              <div class="cbp-caption-activeWrap">
                <div class="cbp-l-caption-alignLeft">
                  <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title">KU Mand Livestream Minggu 7:30 WIB</div>
                    <div class="cbp-l-caption-desc">GRII Live Streaming</div>
                  </div>
                </div>
              </div>
              </a> </div>
            <div class="cbp-item web-design"> <a href="images/image_4.jpg" class="cbp-caption cbp-lightbox" data-title="Magna Tempus Urna<br>by Codelayers">
              <div class="cbp-caption-defaultWrap"> <img src="images/image_4.jpg" alt=""> </div>
              <div class="cbp-caption-activeWrap">
                <div class="cbp-l-caption-alignLeft">
                  <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title">PD Pagi Livestream Sabtu 6:30 WIB</div>
                    <div class="cbp-l-caption-desc">GRII Live Streaming</div>
                  </div>
                </div>
              </div>
              </a> </div>
            <div class="cbp-item motion identity"> <a href="images/image_2.jpg" class="cbp-caption cbp-lightbox" data-title="World Clock Widget<br>by Codelayers">
              <div class="cbp-caption-defaultWrap"> <img src="images/image_2.jpg" alt=""> </div>
              <div class="cbp-caption-activeWrap">
                <div class="cbp-l-caption-alignLeft">
                  <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title">KU Indo Livestream  Minggu 9:30 WIB</div>
                    <div class="cbp-l-caption-desc">GRII Live Streaming</div>
                  </div>
                </div>
              </div>
              </a> </div>
            <div class="cbp-item identity graphic"> <a href="images/image_3.jpg" class="cbp-caption cbp-lightbox" data-title="Quisque Ornare <br>by Codelayers">
              <div class="cbp-caption-defaultWrap"> <img src="images/image_3.jpg" alt=""> </div>
              <div class="cbp-caption-activeWrap">
                <div class="cbp-l-caption-alignLeft">
                  <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title">English Worship Service Sun 10:30 WIB </div>
                    <div class="cbp-l-caption-desc">GRII Live Streaming</div>
                  </div>
                </div>
              </div>
              </a> </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="clearfix"></div>

  <section>
    <div class="pagenation-holder">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h3>Events</h3>
          </div>
          <div class="col-md-6 text-right">
            <div class="pagenation_links"><a href="index.php">Home</a><i> / </i> Events</div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--end section-->
  <div class="clearfix"></div>

  <section class="sec-padding">
  <!--end right column-->
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <h4 class="uppercase oswald">Filter Event</h4>
          <form method="post" action="php/smartprocess.php" id="smart-form">
          <div class="form-group">
                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Filter by Pendeta">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Filter by Tema / Seri">
                </div>
                <button type="submit" class="btn btn-primary">Search Event</button>
          </form>
        <!--end item-->
        <div class="clearfix"></div>
        </div>

        <div class="col-md-9">
          <div class="col-xs-12">
            <h2 class="uppercase oswald">Past Event</h2>
            <div class="title-line-4 less-margin"></div>
            <div class="clearfix"></div>
            <div class="container">
              <div class="row">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Date Time</th>
                          <th scope="col">Pendeta</th>
                          <th scope="col">Tema</th>
                          <th scope="col">Link</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td align="center">26 Maret 2020 18:30</td>
                          <td align="center">Pdt. Aiter</td>
                          <td><strong>Plagues of Egypt</strong><br>
                              Ten Plagues of Egypt - 1 Air Menjadi Darah<br>
                              GRII Citra Raya<br>
                              <code>Mulai Menit Ke-31</code></td>
                              <td align="center"><a href="#" class="col-form-label-sm" data-toggle="tooltip" title="Reformed TV"><img src="images/icon/social-media.png"></a> |
                                      <a href="#" class="col-form-label-sm"><i class="fa fa-youtube" data-toggle="tooltip" title="Youtube"></i> </a> |
                                      <a href="#" class="col-form-label-sm"><img src="images/icon/electronics.png" data-toggle="tooltip" title="Live Streaming"> </a> |
                                      <a href="#" class="col-form-label-sm"><img src="images/icon/zoom.png" data-toggle="tooltip" title="Zoom Meeting"> </a></td>
                        </tr>
                        <tr>
                          <th scope="row">2</th>
                          <td align="center">26 Maret 2020 18:30</td>
                          <td align="center">Pdt. Aiter</td>
                          <td><strong>Plagues of Egypt</strong><br>
                              Ten Plagues of Egypt - 2 Katak<br>
                              GRII Citra Raya<br>
                              <code>Mulai Menit Ke-26</code></td>
                              <td align="center"><a href="#" class="col-form-label-sm" data-toggle="tooltip" title="Reformed TV"><img src="images/icon/social-media.png"></a> |
                                      <a href="#" class="col-form-label-sm"><i class="fa fa-youtube" data-toggle="tooltip" title="Youtube"></i> </a> |
                                      <a href="#" class="col-form-label-sm"><img src="images/icon/electronics.png" data-toggle="tooltip" title="Live Streaming"> </a> |
                                      <a href="#" class="col-form-label-sm"><img src="images/icon/zoom.png" data-toggle="tooltip" title="Zoom Meeting"> </a></td>
                        </tr>
                        <tr>
                          <th scope="row">3</th>
                          <td align="center">26 Maret 2020 18:30</td>
                          <td align="center">Pdt. Aiter</td>
                          <td><strong>Plagues of Egypt</strong><br>
                              Ten Plagues of Egypt - 3 Nyamuk<br>
                              GRII Citra Raya<br>
                              <code>Mulai Menit Ke-17</code></td>
                              <td align="center"><a href="#" class="col-form-label-sm" data-toggle="tooltip" title="Reformed TV"><img src="images/icon/social-media.png"></a> |
                                      <a href="#" class="col-form-label-sm"><i class="fa fa-youtube" data-toggle="tooltip" title="Youtube"></i> </a> |
                                      <a href="#" class="col-form-label-sm"><img src="images/icon/electronics.png" data-toggle="tooltip" title="Live Streaming"> </a> |
                                      <a href="#" class="col-form-label-sm"><img src="images/icon/zoom.png" data-toggle="tooltip" title="Zoom Meeting"> </a></td>
                        </tr>
                        <tr>
                          <th scope="row">4</th>
                          <td align="center">26 Mei 2020 18:30</td>
                          <td align="center">Pdt. Aiter</td>
                          <td><strong>Plagues of Egypt</strong><br>
                              Ten Plagues of Egypt - 4 Lalat Pikat<br>
                              GRII Pusat<br>
                              <code>Mulai Menit Ke-17</code></td>
                              <td align="center"><a href="#" class="col-form-label-sm" data-toggle="tooltip" title="Reformed TV"><img src="images/icon/social-media.png"></a> |
                                      <a href="#" class="col-form-label-sm"><i class="fa fa-youtube" data-toggle="tooltip" title="Youtube"></i> </a> |
                                      <a href="#" class="col-form-label-sm"><img src="images/icon/electronics.png" data-toggle="tooltip" title="Live Streaming"> </a> |
                                      <a href="#" class="col-form-label-sm"><img src="images/icon/zoom.png" data-toggle="tooltip" title="Zoom Meeting"> </a></td>
                        </tr>
                        <tr>
                          <th scope="row">5</th>
                          <td align="center">26 Mei 2020 18:30</td>
                          <td align="center">Pdt. Aiter</td>
                          <td><strong>Plagues of Egypt</strong><br>
                              Ten Plagues of Egypt - 5 Penyakit Sampar<br>
                              GRII Pusat<br>
                              <code>Mulai Menit Ke-17</code></td>
                              <td align="center"><a href="#" class="col-form-label-sm" data-toggle="tooltip" title="Reformed TV"><img src="images/icon/social-media.png"></a> |
                                      <a href="#" class="col-form-label-sm"><i class="fa fa-youtube" data-toggle="tooltip" title="Youtube"></i> </a> |
                                      <a href="#" class="col-form-label-sm"><img src="images/icon/electronics.png" data-toggle="tooltip" title="Live Streaming"> </a> |
                                      <a href="#" class="col-form-label-sm"><img src="images/icon/zoom.png" data-toggle="tooltip" title="Zoom Meeting"> </a></td>
                        </tr>
                        <tr>
                          <th scope="row">6</th>
                          <td align="center">26 Mei 2020 18:30</td>
                          <td align="center">Pdt. Aiter</td>
                          <td><strong>Plagues of Egypt</strong><br>
                              Ten Plagues of Egypt - 6 Barah<br>
                              GRII Pusat<br>
                              <code>Mulai Menit Ke-17</code></td>
                              <td align="center"><a href="#" class="col-form-label-sm" data-toggle="tooltip" title="Reformed TV"><img src="images/icon/social-media.png"></a> |
                                      <a href="#" class="col-form-label-sm"><i class="fa fa-youtube" data-toggle="tooltip" title="Youtube"></i> </a> |
                                      <a href="#" class="col-form-label-sm"><img src="images/icon/electronics.png" data-toggle="tooltip" title="Live Streaming"> </a> |
                                      <a href="#" class="col-form-label-sm"><img src="images/icon/zoom.png" data-toggle="tooltip" title="Zoom Meeting"> </a></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
              </div>
          </div>
          </div>
          <div class="clearfix"></div>

        </div>
        <!--end left column-->


      </div>
    </div>
  </section>
  <!-- end section -->
  <div class="clearfix"></div>

  <!--end section-->
  <div class="clearfix"></div>
  <section class="section-copyrights sec-moreless-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12"> <span>Copyright © 2020 GRII Indonesia</div>
        </div>
    </div>
  </section>
  <!--end section-->
  <div class="clearfix"></div>

  <a href="#" class="scrollup brown"></a><!-- end scroll to top of the page-->
</div>
<!-- end site wraper -->

<!-- ============ JS FILES ============ -->

<script type="text/javascript" src="js/universal/jquery.js"></script>
<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jFlickrFeed/jflickrfeed.min.js"></script>
<script src="js/jFlickrFeed/jquery.cycle.all.min.js"></script>
<script>
$('#basicuse').jflickrfeed({
limit: 6,
qstrings: {
id: '133294431@N08'
},
itemTemplate:
'<li>' +
'<a href="{{image_b}}"><img src="{{image_s}}" alt="{{title}}" /></a>' +
'</li>'
});
$(document).ready(function () {
    function c(passed_month, passed_year, calNum) {
        var calendar = calNum == 0 ? calendars.cal1 : calendars.cal2;
        makeWeek(calendar.weekline);
        calendar.datesBody.empty();
        var calMonthArray = makeMonthArray(passed_month, passed_year);
        var r = 0;
        var u = false;
        while (!u) {
            if (daysArray[r] == calMonthArray[0].weekday) {
                u = true
            } else {
                calendar.datesBody.append('<div class="blank"></div>');
                r++;
            }
        }
        for (var cell = 0; cell < 42 - r; cell++) { // 42 date-cells in calendar
            if (cell >= calMonthArray.length) {
                calendar.datesBody.append('<div class="blank"></div>');
            } else {
                var shownDate = calMonthArray[cell].day;
                var iter_date = new Date(passed_year, passed_month, shownDate);
                if (
                (
                (shownDate != today.getDate() && passed_month == today.getMonth()) || passed_month != today.getMonth()) && iter_date < today) {
                    var m = '<div class="past-date">';
                } else {
                    var m = checkToday(iter_date) ? '<div class="today">' : "<div>";
                }
                calendar.datesBody.append(m + shownDate + "</div>");
            }
        }

        var color = "#444444";
        calendar.calHeader.find("h2").text(i[passed_month] + " " + passed_year);
        calendar.weekline.find("div").css("color", color);
        calendar.datesBody.find(".today").css("color", "#87b633");

        // find elements (dates) to be clicked on each time
        // the calendar is generated
        var clicked = false;
        selectDates(selected);

        clickedElement = calendar.datesBody.find('div');
        clickedElement.on("click", function () {
            clicked = $(this);
            var whichCalendar = calendar.name;

            if (firstClick && secondClick) {
                thirdClicked = getClickedInfo(clicked, calendar);
                var firstClickDateObj = new Date(firstClicked.year,
                firstClicked.month,
                firstClicked.date);
                var secondClickDateObj = new Date(secondClicked.year,
                secondClicked.month,
                secondClicked.date);
                var thirdClickDateObj = new Date(thirdClicked.year,
                thirdClicked.month,
                thirdClicked.date);
                if (secondClickDateObj > thirdClickDateObj && thirdClickDateObj > firstClickDateObj) {
                    secondClicked = thirdClicked;
                    // then choose dates again from the start :)
                    bothCals.find(".calendar_content").find("div").each(function () {
                        $(this).removeClass("selected");
                    });
                    selected = {};
                    selected[firstClicked.year] = {};
                    selected[firstClicked.year][firstClicked.month] = [firstClicked.date];
                    selected = addChosenDates(firstClicked, secondClicked, selected);
                } else { // reset clicks
                    selected = {};
                    firstClicked = [];
                    secondClicked = [];
                    firstClick = false;
                    secondClick = false;
                    bothCals.find(".calendar_content").find("div").each(function () {
                        $(this).removeClass("selected");
                    });
                }
            }
            if (!firstClick) {
                firstClick = true;
                firstClicked = getClickedInfo(clicked, calendar);
                selected[firstClicked.year] = {};
                selected[firstClicked.year][firstClicked.month] = [firstClicked.date];
            } else {
                secondClick = true;
                secondClicked = getClickedInfo(clicked, calendar);

                // what if second clicked date is before the first clicked?
                var firstClickDateObj = new Date(firstClicked.year,
                firstClicked.month,
                firstClicked.date);
                var secondClickDateObj = new Date(secondClicked.year,
                secondClicked.month,
                secondClicked.date);

                if (firstClickDateObj > secondClickDateObj) {

                    var cachedClickedInfo = secondClicked;
                    secondClicked = firstClicked;
                    firstClicked = cachedClickedInfo;
                    selected = {};
                    selected[firstClicked.year] = {};
                    selected[firstClicked.year][firstClicked.month] = [firstClicked.date];

                } else if (firstClickDateObj.getTime() == secondClickDateObj.getTime()) {
                    selected = {};
                    firstClicked = [];
                    secondClicked = [];
                    firstClick = false;
                    secondClick = false;
                    $(this).removeClass("selected");
                }


                // add between dates to [selected]
                selected = addChosenDates(firstClicked, secondClicked, selected);
            }
            selectDates(selected);
        });

    }

    function selectDates(selected) {
        if (!$.isEmptyObject(selected)) {
            var dateElements1 = datesBody1.find('div');
            var dateElements2 = datesBody2.find('div');

            function highlightDates(passed_year, passed_month, dateElements) {
                if (passed_year in selected && passed_month in selected[passed_year]) {
                    var daysToCompare = selected[passed_year][passed_month];
                    for (var d in daysToCompare) {
                        dateElements.each(function (index) {
                            if (parseInt($(this).text()) == daysToCompare[d]) {
                                $(this).addClass('selected');
                            }
                        });
                    }

                }
            }

            highlightDates(year, month, dateElements1);
            highlightDates(nextYear, nextMonth, dateElements2);
        }
    }

    function makeMonthArray(passed_month, passed_year) { // creates Array specifying dates and weekdays
        var e = [];
        for (var r = 1; r < getDaysInMonth(passed_year, passed_month) + 1; r++) {
            e.push({
                day: r,
                // Later refactor -- weekday needed only for first week
                weekday: daysArray[getWeekdayNum(passed_year, passed_month, r)]
            });
        }
        return e;
    }

    function makeWeek(week) {
        week.empty();
        for (var e = 0; e < 7; e++) {
            week.append("<div>" + daysArray[e].substring(0, 3) + "</div>")
        }
    }

    function getDaysInMonth(currentYear, currentMon) {
        return (new Date(currentYear, currentMon + 1, 0)).getDate();
    }

    function getWeekdayNum(e, t, n) {
        return (new Date(e, t, n)).getDay();
    }

    function checkToday(e) {
        var todayDate = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var checkingDate = e.getFullYear() + '/' + (e.getMonth() + 1) + '/' + e.getDate();
        return todayDate == checkingDate;

    }

    function getAdjacentMonth(curr_month, curr_year, direction) {
        var theNextMonth;
        var theNextYear;
        if (direction == "next") {
            theNextMonth = (curr_month + 1) % 12;
            theNextYear = (curr_month == 11) ? curr_year + 1 : curr_year;
        } else {
            theNextMonth = (curr_month == 0) ? 11 : curr_month - 1;
            theNextYear = (curr_month == 0) ? curr_year - 1 : curr_year;
        }
        return [theNextMonth, theNextYear];
    }

    function b() {
        today = new Date;
        year = today.getFullYear();
        month = today.getMonth();
        var nextDates = getAdjacentMonth(month, year, "next");
        nextMonth = nextDates[0];
        nextYear = nextDates[1];
    }

    var e = 480;

    var today;
    var year,
    month,
    nextMonth,
    nextYear;

    var r = [];
    var i = [
        "JANUARY",
        "FEBRUARY",
        "MARCH",
        "APRIL",
        "MAY",
        "JUNE",
        "JULY",
        "AUGUST",
        "SEPTEMBER",
        "OCTOBER",
        "NOVEMBER",
        "DECEMBER"];
    var daysArray = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"];

    var cal1 = $("#calendar_first");
    var calHeader1 = cal1.find(".calendar_header");
    var weekline1 = cal1.find(".calendar_weekdays");
    var datesBody1 = cal1.find(".calendar_content");

    var cal2 = $("#calendar_second");
    var calHeader2 = cal2.find(".calendar_header");
    var weekline2 = cal2.find(".calendar_weekdays");
    var datesBody2 = cal2.find(".calendar_content");

    var bothCals = $(".calendar");

    var switchButton = bothCals.find(".calendar_header").find('.switch-month');

    var calendars = {
        "cal1": {
            "name": "first",
                "calHeader": calHeader1,
                "weekline": weekline1,
                "datesBody": datesBody1
        },
            "cal2": {
            "name": "second",
                "calHeader": calHeader2,
                "weekline": weekline2,
                "datesBody": datesBody2
        }
    }


    var clickedElement;
    var firstClicked,
    secondClicked,
    thirdClicked;
    var firstClick = false;
    var secondClick = false;
    var selected = {};

    b();
    c(month, year, 0);
    c(nextMonth, nextYear, 1);
    switchButton.on("click", function () {
        var clicked = $(this);
        var generateCalendars = function (e) {
            var nextDatesFirst = getAdjacentMonth(month, year, e);
            var nextDatesSecond = getAdjacentMonth(nextMonth, nextYear, e);
            month = nextDatesFirst[0];
            year = nextDatesFirst[1];
            nextMonth = nextDatesSecond[0];
            nextYear = nextDatesSecond[1];

            c(month, year, 0);
            c(nextMonth, nextYear, 1);
        };
        if (clicked.attr("class").indexOf("left") != -1) {
            generateCalendars("previous");
        } else {
            generateCalendars("next");
        }
        clickedElement = bothCals.find(".calendar_content").find("div");
    });


    //  Click picking stuff
    function getClickedInfo(element, calendar) {
        var clickedInfo = {};
        var clickedCalendar,
        clickedMonth,
        clickedYear;
        clickedCalendar = calendar.name;
        clickedMonth = clickedCalendar == "first" ? month : nextMonth;
        clickedYear = clickedCalendar == "first" ? year : nextYear;
        clickedInfo = {
            "calNum": clickedCalendar,
                "date": parseInt(element.text()),
                "month": clickedMonth,
                "year": clickedYear
        }
        return clickedInfo;
    }


    // Finding between dates MADNESS. Needs refactoring and smartening up :)
    function addChosenDates(firstClicked, secondClicked, selected) {
        if (secondClicked.date > firstClicked.date || secondClicked.month > firstClicked.month || secondClicked.year > firstClicked.year) {

            var added_year = secondClicked.year;
            var added_month = secondClicked.month;
            var added_date = secondClicked.date;

            if (added_year > firstClicked.year) {
                // first add all dates from all months of Second-Clicked-Year
                selected[added_year] = {};
                selected[added_year][added_month] = [];
                for (var i = 1;
                i <= secondClicked.date;
                i++) {
                    selected[added_year][added_month].push(i);
                }

                added_month = added_month - 1;
                while (added_month >= 0) {
                    selected[added_year][added_month] = [];
                    for (var i = 1;
                    i <= getDaysInMonth(added_year, added_month);
                    i++) {
                        selected[added_year][added_month].push(i);
                    }
                    added_month = added_month - 1;
                }

                added_year = added_year - 1;
                added_month = 11; // reset month to Dec because we decreased year
                added_date = getDaysInMonth(added_year, added_month); // reset date as well

                // Now add all dates from all months of inbetween years
                while (added_year > firstClicked.year) {
                    selected[added_year] = {};
                    for (var i = 0; i < 12; i++) {
                        selected[added_year][i] = [];
                        for (var d = 1; d <= getDaysInMonth(added_year, i); d++) {
                            selected[added_year][i].push(d);
                        }
                    }
                    added_year = added_year - 1;
                }
            }

            if (added_month > firstClicked.month) {
                if (firstClicked.year == secondClicked.year) {
                    selected[added_year][added_month] = [];
                    for (var i = 1;
                    i <= secondClicked.date;
                    i++) {
                        selected[added_year][added_month].push(i);
                    }
                    added_month = added_month - 1;
                }
                while (added_month > firstClicked.month) {
                    selected[added_year][added_month] = [];
                    for (var i = 1;
                    i <= getDaysInMonth(added_year, added_month);
                    i++) {
                        selected[added_year][added_month].push(i);
                    }
                    added_month = added_month - 1;
                }
                added_date = getDaysInMonth(added_year, added_month);
            }

            for (var i = firstClicked.date + 1;
            i <= added_date;
            i++) {
                selected[added_year][added_month].push(i);
            }
        }
        return selected;
    }
});
</script>
<script src="js/mainmenu/customeUI.js"></script>
<script type="text/javascript" src="js/cubeportfolio/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="js/cubeportfolio/main.js"></script>
<script src="js/scrolltotop/totop.js"></script>
<script src="js/mainmenu/jquery.sticky.js"></script>

<script src="js/scripts/functions.js" type="text/javascript"></script>
</body>
</html>
