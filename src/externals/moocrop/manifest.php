<?php return array(
  'package' => array(
    'type' => 'external',
    'name' => 'moocrop',
    'version' => '4.10.5',
    'revision' => '$Revision: 9747 $',
    'path' => 'externals/moocrop',
    'repository' => 'socialengine.com',
    'title' => 'Moocrop',
    'author' => 'Webligo Developments',
    'directories' => array(
      'externals/moocrop',
    )
  )
) ?>