<?php return array(
  'package' => array(
    'type' => 'external',
    'name' => 'flowplayer',
    'version' => '4.10.5',
    'revision' => '$Revision: 9747 $',
    'path' => 'externals/flowplayer',
    'repository' => 'socialengine.com',
    'title' => 'Flow Player',
    'author' => 'Webligo Developments',
    'directories' => array(
      'externals/flowplayer',
    )
  )
) ?>
