<?php return array(
  'package' => array(
    'type' => 'external',
    'name' => 'Selectize',
    'version' => '4.10.5',
    'revision' => '$Revision: 9747 $',
    'path' => 'externals/selectize',
    'repository' => 'socialengine.com',
    'title' => 'external',
    'author' => 'Webligo Developments',
    'license' => 'http://www.socialengine.com/license/',
    'directories' => array(
      'externals/selectize',
    )
  )
) ?>
