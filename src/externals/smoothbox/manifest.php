<?php return array(
  'package' => array(
    'type' => 'external',
    'name' => 'smoothbox',
    'version' => '4.10.5',
    'revision' => '$Revision: 9747 $',
    'path' => 'externals/smoothbox',
    'repository' => 'socialengine.com',
    'title' => 'Smoothbox',
    'author' => 'Webligo Developments',
    'directories' => array(
      'externals/smoothbox',
    )
  )
) ?>
